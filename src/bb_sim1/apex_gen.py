import sys
import os
import math
import random

class Job:
	def __init__(self, jobType, jobID, pipeID, jobSize, jobLen, cpRatio):
		self.jobType = jobType
		self.jobID = jobID
		self.jobSize = jobSize
		self.jobLen = jobLen
		self.cpRatio = cpRatio
		self.pipeID = pipeID


###CIELO RATIO###
CIELO_CORES = 131072
TRINITY_NODES = 2528
TRINITY_CORES_PER_NODE = 176
TRINITY_CORES = TRINITY_CORES_PER_NODE * TRINITY_NODES # 19000 - total nodes; 32 - cores per node
EAP_CIELO_AVG_CORES = 16384
EAP_CIELO_HRO_CORES = 65536
LAP_CIELO_AVG_CORES = 4096
LAP_CIELO_HRO_CORES = 32768
STON_CIELO_AVG_CORES = 32768
STON_CIELO_HRO_CORES = 131072
VPIC_CIELO_AVG_CORES = 30000
VPIC_CIELO_HRO_CORES = 70000

EAP_AVG_CORE_RATIO = EAP_CIELO_AVG_CORES / CIELO_CORES
EAP_HRO_CORE_RATIO = EAP_CIELO_HRO_CORES / CIELO_CORES
LAP_AVG_CORE_RATIO = LAP_CIELO_AVG_CORES / CIELO_CORES
LAP_HRO_CORE_RATIO = LAP_CIELO_HRO_CORES / CIELO_CORES
STON_AVG_CORE_RATIO = STON_CIELO_AVG_CORES / CIELO_CORES
STON_HRO_CORE_RATIO = STON_CIELO_HRO_CORES / CIELO_CORES
VPIC_AVG_CORE_RATIO = VPIC_CIELO_AVG_CORES / CIELO_CORES
VPIC_HRO_CORE_RATIO = VPIC_CIELO_HRO_CORES / CIELO_CORES

EAP_AVG_JOB_NODES_TRINITY = EAP_AVG_CORE_RATIO * TRINITY_CORES / TRINITY_CORES_PER_NODE
EAP_HRO_JOB_NODES_TRINITY = EAP_HRO_CORE_RATIO * TRINITY_CORES / TRINITY_CORES_PER_NODE
LAP_AVG_JOB_NODES_TRINITY = LAP_AVG_CORE_RATIO * TRINITY_CORES / TRINITY_CORES_PER_NODE
LAP_HRO_JOB_NODES_TRINITY = LAP_HRO_CORE_RATIO * TRINITY_CORES / TRINITY_CORES_PER_NODE
STON_AVG_JOB_NODES_TRINITY = STON_AVG_CORE_RATIO * TRINITY_CORES / TRINITY_CORES_PER_NODE
STON_HRO_JOB_NODES_TRINITY = STON_HRO_CORE_RATIO * TRINITY_CORES / TRINITY_CORES_PER_NODE
VPIC_AVG_JOB_NODES_TRINITY = VPIC_AVG_CORE_RATIO * TRINITY_CORES / TRINITY_CORES_PER_NODE
VPIC_HRO_JOB_NODES_TRINITY = VPIC_HRO_CORE_RATIO * TRINITY_CORES / TRINITY_CORES_PER_NODE

print('eap avg %d eap hero %d' % (EAP_AVG_JOB_NODES_TRINITY, EAP_HRO_JOB_NODES_TRINITY))
print('lap avg %d lap hero %d' % (LAP_AVG_JOB_NODES_TRINITY, LAP_HRO_JOB_NODES_TRINITY))
print('ston avg %d ston hero %d' % (STON_AVG_JOB_NODES_TRINITY, STON_HRO_JOB_NODES_TRINITY))
print('vpic avg %d vpic hero %d' % (VPIC_AVG_JOB_NODES_TRINITY, VPIC_HRO_JOB_NODES_TRINITY))

### PIPELINE SIZE ###
EAP_DEFAULT_CNT = 30
LAP_DEFAULT_CNT = 10
STON_CNT = 6
VPIC_CNT = 4
TOTAL_CNT = EAP_DEFAULT_CNT + LAP_DEFAULT_CNT + STON_CNT + VPIC_CNT
EAP_RATIO = EAP_DEFAULT_CNT / TOTAL_CNT
LAP_RATIO = LAP_DEFAULT_CNT / TOTAL_CNT
STON_RATIO = STON_CNT / TOTAL_CNT
VPIC_RATIO = VPIC_CNT / TOTAL_CNT

### JOBS ###
PERJOB_HOUR = 24
EAP_WALLTIME_AVG = 262
LAP_WALLTIME_AVG = 64
STON_WALLTIME_AVG = 128
VPIC_WALLTIME_AVG = 96
#EAP_JOBS_PER_PIPE = round(EAP_WALLTIME / PERJOB_HOUR)
#LAP_JOBS_PER_PIPE = round(LAP_WALLTIME / PERJOB_HOUR)
#STON_JOBS_PER_PIPE = round(STON_WALLTIME / PERJOB_HOUR)
#VPIC_JOBS_PER_PIPE = round(VPIC_WALLTIME / PERJOB_HOUR)
#print('eap job count %d lap job count %d ston job count %d vpic job count %d' % (EAP_JOBS_PER_PIPE, LAP_JOBS_PER_PIPE, STON_JOBS_PER_PIPE, VPIC_JOBS_PER_PIPE))

### CP RATIOS ###
EAP_CP_RATIO = 0.1
LAP_CP_RATIO = 0.15
STON_CP_RATIO = 0.7
VPIC_CP_RATIO = 0.0625

### ABSOLUTE MIN ###
jobSizeMin = 32

####START####
num_pipes = int(sys.argv[1])
print('user entered pipelien number is %d' % num_pipes)

fd_out = open("fake_config", 'w')
fd_out1 = open("o2.1_config", 'w')

eap_pipes = int(num_pipes * EAP_RATIO)
lap_pipes = int(num_pipes * LAP_RATIO)
ston_pipes = int(num_pipes * STON_RATIO)
vpic_pipes = int(num_pipes * VPIC_RATIO)

print('eap %d lap %d ston %d vpic %d' % (eap_pipes, lap_pipes, ston_pipes, vpic_pipes))


WALLTIME_RANGE_FACTOR = 1.2
jobQueue = []
jobID = 0
jobLen = 24
### Generate eap jobs ###
jobType = 'EAP'
sigma = (EAP_AVG_JOB_NODES_TRINITY - jobSizeMin) / 4
print('#### EAP JOBS ####')
for i in range(0, eap_pipes - 1):
	#first get a random pipeline hour based on average
	sigma_pipe_hour = (EAP_WALLTIME_AVG * WALLTIME_RANGE_FACTOR - EAP_WALLTIME_AVG) / 3
	this_hour = random.gauss(EAP_WALLTIME_AVG, sigma_pipe_hour)
	num_jobs = round(this_hour / jobLen)
	print('eap pipe id %d hour %f' % (i, this_hour))
	for j in range(0, num_jobs):
		jobSize = random.gauss(EAP_AVG_JOB_NODES_TRINITY, sigma)
		job = Job('EAP', jobID, i, jobSize, jobLen, EAP_CP_RATIO)
		print('job id %d josbSize %d' % (jobID, jobSize))
		jobQueue.append(job)
		jobID = jobID + 1

### Generate lap jobs ###
jobType = 'LAP'
sigma = (LAP_AVG_JOB_NODES_TRINITY - jobSizeMin) / 4
print('### LAP JOBS ###')
for i in range(0, lap_pipes - 1):
	sigma_pipe_hour = (LAP_WALLTIME_AVG * WALLTIME_RANGE_FACTOR - LAP_WALLTIME_AVG) / 3
	this_hour = random.gauss(LAP_WALLTIME_AVG, sigma_pipe_hour)
	num_jobs = round(this_hour / jobLen)
	for j in range(0, num_jobs):
		jobSize = random.gauss(LAP_AVG_JOB_NODES_TRINITY, sigma)
		job = Job('LAP', jobID, i, jobSize, jobLen, LAP_CP_RATIO)
		#print('job id %d jobSize %d' % (jobID, jobSize))
		jobQueue.append(job)
		jobID = jobID + 1

### Generate silverton jobs ###
jobType = 'STON'
sigma = (STON_AVG_JOB_NODES_TRINITY - jobSizeMin) / 4
print('### STON JOBS ###')
for i in range(0, ston_pipes - 1):
	sigma_pipe_hour = (STON_WALLTIME_AVG * WALLTIME_RANGE_FACTOR - STON_WALLTIME_AVG) / 3
	this_hour = random.gauss(STON_WALLTIME_AVG, sigma_pipe_hour)
	num_jobs = round(this_hour / jobLen)
	for j in range(0, num_jobs):
		jobSize = random.gauss(STON_AVG_JOB_NODES_TRINITY, sigma)
		job = Job('STON', jobID, i, jobSize, jobLen, STON_CP_RATIO)
		#print('job id %d jobSize %d' % (jobID, jobSize))
		jobQueue.append(job)
		jobID = jobID + 1

### Generate vpic jobs ###
jobType = 'VPIC'
sigma = (VPIC_AVG_JOB_NODES_TRINITY - jobSizeMin) / 4
print('### VPIC JOBS ###')
for i in range(0, vpic_pipes - 1):
	sigma_pipe_hour = (VPIC_WALLTIME_AVG * WALLTIME_RANGE_FACTOR - VPIC_WALLTIME_AVG) / 3
	this_hour = random.gauss(VPIC_WALLTIME_AVG, sigma_pipe_hour)
	num_jobs = round(this_hour / jobLen)
	for j in range(0, num_jobs):
		jobSize = random.gauss(VPIC_AVG_JOB_NODES_TRINITY, sigma)
		job = Job('VPIC', jobID, i, jobSize, jobLen, VPIC_CP_RATIO)
		#print('HERO job id %d jobSize %d' % (jobID, jobSize))
		jobQueue.append(job)
		jobID = jobID + 1

### Generate EAP HERO pipeline ###
jobType = "EAP"
sigma_pipe_hour = (EAP_WALLTIME_AVG * WALLTIME_RANGE_FACTOR - EAP_WALLTIME_AVG) / 3
this_hour = random.gauss(EAP_WALLTIME_AVG, sigma_pipe_hour)
num_jobs = round(this_hour / jobLen)
for i in range(0, num_jobs):
		jobSize = EAP_HRO_JOB_NODES_TRINITY
		job = Job('EAP', jobID, eap_pipes - 1, jobSize, jobLen, EAP_CP_RATIO)
		#print('EAP HERO job id %d jobSize %d' % (jobID, jobSize))
		jobQueue.append(job)
		jobID = jobID + 1

### Generate LAP HERO pipeline ###
sigma_pipe_hour = (LAP_WALLTIME_AVG * WALLTIME_RANGE_FACTOR - LAP_WALLTIME_AVG) / 3
this_hour = random.gauss(LAP_WALLTIME_AVG, sigma_pipe_hour)
num_jobs = round(this_hour / jobLen)
for i in range(0, num_jobs):
		jobSize = LAP_HRO_JOB_NODES_TRINITY
		job = Job('LAP', jobID, lap_pipes - 1, jobSize, jobLen, LAP_CP_RATIO)
		#print('LAP HERO job id %d jobSize %d' % (jobID, jobSize))
		jobQueue.append(job)
		jobID = jobID + 1

### Generate STON HERO pipeline ###
sigma_pipe_hour = (STON_WALLTIME_AVG * WALLTIME_RANGE_FACTOR - STON_WALLTIME_AVG) / 3
this_hour = random.gauss(STON_WALLTIME_AVG, sigma_pipe_hour)
num_jobs = round(this_hour / jobLen)
for i in range(0, num_jobs):
		jobSize = STON_HRO_JOB_NODES_TRINITY
		job = Job('STON', jobID, ston_pipes - 1, jobSize, jobLen, STON_CP_RATIO)
		#print('STON HERO job id %d jobSize %d' % (jobID, jobSize))
		jobQueue.append(job)
		jobID = jobID + 1

### Generate VPIC HERO pipeline
sigma_pipe_hour = (VPIC_WALLTIME_AVG * WALLTIME_RANGE_FACTOR - VPIC_WALLTIME_AVG) / 3
this_hour = random.gauss(VPIC_WALLTIME_AVG, sigma_pipe_hour)
num_jobs = round(this_hour / jobLen)
for i in range(0, num_jobs):
		jobSize = VPIC_HRO_JOB_NODES_TRINITY
		job = Job('VPIC', jobID, vpic_pipes - 1, jobSize, jobLen, VPIC_CP_RATIO)
		#print('VPIC HERO job id %d jobSize %d' % (jobID, jobSize))
		jobQueue.append(job)
		jobID = jobID + 1

#### SHUFFLE JOBQUEUE ####
random.shuffle(jobQueue)

### first make concentrated configuration ###
faultRate = 28
recoveryRate = 1
parity = 0
bbAssignment = 0
scheduling = 1
pfsBw = 984
bbNodeNum = 288
bbBw = 5.87
bbCapacity = 6735.6444
bbTotalWrite = 1424.795
bbTotalRead = 1659.9424
computeNodeNum = 9500
computeNodeMem = 128
computeNodeBw = 15.3

fd_out.write("concentrated\n")
fd_out.write("faultRate(Hours):%d\n" % faultRate)
fd_out.write("nodeRecoveryTime(hour):%d\n" % recoveryRate)
fd_out.write("parity:%d\n" % parity)
fd_out.write("bbAssignment:%d\n" % bbAssignment)
fd_out.write("scheduling:%d\n" % scheduling)
fd_out.write("*********\n")
fd_out.write("PFSBandwidth(GB/s):%f\n" % pfsBw)
fd_out.write("bbNodeNumber:%d\n" % bbNodeNum)
fd_out.write("bbNodeBw(GB/s):%f\n" % bbBw)
fd_out.write("bbNodeCapacity(GB):%f\n" % bbCapacity)
fd_out.write("bbTotalWrite:%f\n" % bbTotalWrite)
fd_out.write("bbTotalRead:%f\n" % bbTotalRead)
fd_out.write("totaComputeNode:%d\n" % computeNodeNum)
fd_out.write("computeNodeMem(GB):%f\n" % computeNodeMem)
fd_out.write("computeNodeBandwidth(GB/s):%f\n" % computeNodeBw)
fd_out.write("************\n")

length = len(jobQueue)
for i in range(0, length):
	fd_out.write("job %d\n" % i)
	fd_out.write("%s\n" % jobQueue[i].jobType)
	fd_out.write("pipeID:%s\n" % jobQueue[i].pipeID)
	fd_out.write("jobID:%d\n" % jobQueue[i].jobID)
	fd_out.write("jobLen:%f\n" % jobQueue[i].jobLen)
	fd_out.write("jobNodeCount:%d\n" % jobQueue[i].jobSize)
	fd_out.write("chkpRatio:%f\n" % jobQueue[i].cpRatio)
	fd_out.write("bbNodeUseNumber:%d\n" % 2)
	#if jobQueue[i].jobSize > computeNodeNum / 4:
	#	fd_out.write("bbNodeUseNumber:%d\n" % 2)
	#else:
	#	fd_out.write("bbNodeUseNumber:%d\n" % 4)
fd_out.write('END')
fd_out.close()

### Now make Local config file ###
faultRate = 34
recoveryRate = 1
parity = 0
scheduling = 1
bbTotalBW = 3381.12
bbTotalWrite = 14400
bbTotalRead = 38400
pfsBw = 984
computeNodeNum = 2528
computeNodeMem = 1280
computeNodeBw = 46.566
fd_out1.write("local\n")
fd_out1.write("faultRate(Hours):%d\n" % faultRate)
fd_out1.write("nodeRecoveryTime(hour):%d\n" % recoveryRate)
fd_out1.write("parity:%d\n" % parity)
fd_out1.write("********\n")
fd_out1.write("bbTotalBw(GB/s):%f\n" % bbTotalBW)
fd_out1.write("bbTOtalWrite:%f\n" % bbTotalWrite)
fd_out1.write("bbTotalRead:%f\n" % bbTotalRead)
fd_out1.write("pfsBandwidth(GB/s):%f\n" % pfsBw)
fd_out1.write("totaComputeNode:%d\n" % computeNodeNum)
fd_out1.write("computeNodeMem(GB):%f\n" % computeNodeMem)
fd_out1.write("computeNodeBandwidth(GB/s):%f\n" % computeNodeBw)
fd_out1.write("************\n")

length = len(jobQueue)
for i in range(0, length):
        fd_out1.write("job %d\n" % i)
        fd_out1.write("%s\n" % jobQueue[i].jobType)
        fd_out1.write("pipeID:%s\n" % jobQueue[i].pipeID)
        fd_out1.write("jobID:%d\n" % jobQueue[i].jobID)
        fd_out1.write("jobLen:%f\n" % jobQueue[i].jobLen)
        fd_out1.write("jobNodeCount:%d\n" % jobQueue[i].jobSize)
        fd_out1.write("chkpRatio:%f\n" % jobQueue[i].cpRatio)
fd_out1.write('END\n')
fd_out1.close()

### verification ###
### Goal - 50 % EAP, 
num_eap_pipes = 0
num_lap_pipes = 0
num_ston_pipes = 0
num_vpic_pipes = 0
eap_pipe_dict = dict()
lap_pipe_dict = dict()
ston_pipe_dict = dict()
vpic_pipe_dict = dict()
for i in jobQueue:
	#print('job name %s pipeID %d' % (i.jobType, i.pipeID))
	if i.jobType == 'EAP':
		if i.pipeID in eap_pipe_dict:
			eap_pipe_dict[i.pipeID] = eap_pipe_dict[i.pipeID] + 1
		else:
			#print('eap pipe id %d' % i.pipeID)
			num_eap_pipes = num_eap_pipes + 1
			eap_pipe_dict[i.pipeID] = 1
	elif i.jobType == 'LAP':
		if i.pipeID in lap_pipe_dict:
			lap_pipe_dict[i.pipeID] = lap_pipe_dict[i.pipeID] + 1
		else:
			#print('lap pipe id %d' % i.pipeID)
			num_lap_pipes = num_lap_pipes + 1
			lap_pipe_dict[i.pipeID] = 1
	elif i.jobType == 'STON':
		if i.pipeID in ston_pipe_dict:
			ston_pipe_dict[i.pipeID] = ston_pipe_dict[i.pipeID] + 1
		else:
			#print('ston pipe id %d' % i.pipeID)
			num_ston_pipes = num_ston_pipes + 1
			ston_pipe_dict[i.pipeID] = 1
	elif i.jobType == 'VPIC':
		if i.pipeID in vpic_pipe_dict:
			vpic_pipe_dict[i.pipeID] = vpic_pipe_dict[i.pipeID] + 1
		else:	
			#print('vpic pipe id %d' % i.pipeID)
			num_vpic_pipes = num_vpic_pipes + 1
			vpic_pipe_dict[i.pipeID] = 1

keys = eap_pipe_dict.keys()
num_hours = 0
for key in keys:
	num_hours = eap_pipe_dict[key] * 24 + num_hours

avg_hours = num_hours / len(keys)
print('eap pipeline avg wall time %f' % avg_hours)

keys = lap_pipe_dict.keys()
num_hours = 0
for key in keys:
	num_hours = lap_pipe_dict[key] * 24 + num_hours

avg_hours = num_hours / len(keys)
print('lap pipeline avg wall time %f' % avg_hours)

keys = ston_pipe_dict.keys()
num_hours = 0
for key in keys:
	num_hours = ston_pipe_dict[key] * 24 + num_hours
avg_hours = num_hours / len(keys)
print('ston pipeline avg wall time %f' % avg_hours)

keys = vpic_pipe_dict.keys()
num_hours = 0
for key in keys:
	num_hours = vpic_pipe_dict[key] * 24 + num_hours
avg_hours = num_hours / len(keys)
print('vpic pipeline avg wall time %f' % avg_hours)


total_pipes = num_eap_pipes + num_lap_pipes + num_ston_pipes + num_vpic_pipes
#print('num eap pipe %d' % num_eap_pipes)
#print('num lap pipe %d' % num_lap_pipes)
#print('num ston pipe %d' % num_ston_pipes)
#print('num vpic pipe %d' % num_vpic_pipes)
print('eap pipe ratio %f; lap pipe ratio %f; ston pipe ratio %f; vpic pipe ratio %f' % ((num_eap_pipes / total_pipes), (num_lap_pipes / total_pipes), (num_ston_pipes / total_pipes), (num_vpic_pipes / total_pipes)))

print('total job count %d' % len(jobQueue))
