import sys
import os
import math

###start###
argc = len(sys.argv)
fd = []
for i in range(1, argc - 1):
	fd = open(sys.argv[i], 'r')

fd_out = open("compute_nodes_stats.dat", "w")
fd_out.write("#bb_settings    aggregate_idle_hours     aggregate_non_idle_hours")
fd_out1 = open("compute_nodes_eff_stats.dat", "w")
count = len(fd)
total_job_stats = []
#first read in all the simulation settings
for i in range(0, count):
	line = fd[i].readline().rstrip()
	sim_setting = line
	
	line = fd[i].readline().rstrip()
	splits = line.split(' ')
	total_idle_time = float(splits[len(splits) - 1]) / 3600
	
	line = fd[i].readline().rstrip()
	splits = line.split(' ')
	total_non_idle_time = float(splits[len(splits) - 1]) / 3600

	line = fd[i].readline().rstrip()
	splits = line.split(' ')
	fd_out.write("%s %f %f\n" % (sim_setting, total_idle_time, total_non_idle_time))

	efficiency = float(splits[len(splits) - 1])
	fd_out1.write("%s %f\n" % (sim_setting, efficiency))

fd_out.close()
fd_out1.close()

