from compute_node import *
from stats import *
from simpy import *
import simpy
class Job:
	def __init__(self, env, totalBBnum, computeNodeMemSize, reqQueues, jobDone, fd_finish, currentAvailable, parity, pfsReqQ, bbNodes):
		self.env = env
		self.finish = jobDone
		#self.computeNodeStats = nodestats
		self.totalBBNum = totalBBnum
		self.reqQs = reqQueues
		self.pfsBandwidth = -1
		self.timeToSchedule = 0
		self.jobLen = 0
		self.computeNodeNumber = 0
		self.computeNodeMemSize = computeNodeMemSize
		self.chkpRatio = 0
		self.cpFreq = 0
		self.nodeBandwidth = 0
		self.numBurstBuffer = 0
		self.stripePolicy = ''
		self.pfsReqQ = pfsReqQ
		self.computeNodeProcs = []
		self.computeNodeIndices = []
		self.cpRecs = dict()
		self.stats = Stats()
		self.freshStart = 0
		self.fd_finish = fd_finish
		self.compBBNodes = []
		self.bbNodes = bbNodes
		self.run = None
		self.schedulerProc = None
		self.currentTime = [0] #use list to force pass by reference
		#self.run = env.process(self.start_job())
		self.currentAvailable = currentAvailable
		self.parity = parity
		self.reStageIn = 0
		self.latestFaultTime = -1

	def begin_job(self, restartFlag):
		self.run = self.env.process(self.start_jobb(restartFlag))

	def getRestartTime(self, mode):
		if mode == 1:
			print('get restart time mode %d' % mode)
			return 0
		#this gets the time when the latest available CP completed
		totalOutSize = -1
		if self.parity == 0:
			totalOutSize = self.computeNodeMemSize * self.chkpRatio * self.computeNodeNumber
		elif self.parity == 1:
			totalOutSize = self.computeNodeMemSize * self.chkpRatio * self.computeNodeNumber * 1.2

		pfsOutputTime = totalOutSize / self.pfsBandwidth
		cycle = Job.findRestartPoint(self.cpRecs, self.computeNodeNumber, self.env.now, pfsOutputTime)	
		print('get restart time start cycle is %d' % cycle)
		time = -1
		if cycle >= 0:
			time = 0
			#compute the total valid time for all valid cycles
			for i in range(0, cycle + 1):
				recs = self.cpRecs[i]
				nodes = recs.keys()
				count = 0
				for j in nodes:
					count = count + recs[0][3]
				if count == len(nodes):
					print('%d th cycle is valid' % i)
					time = time + recs[0][2]
				#time = recs[0][2] + time
		return time

	def start_jobb(self, restartFlag):
		stageOut = 0
		#print('job %d in start  job')
		done = 0
		barriers = [] #used to sync between compute nodes
		completeQ = [] #used to notify job that all compute nodes are done
		for i in range(self.computeNodeNumber):
			barriers.append(None)

		counter = [0]
		#check if it is a restart
		#find pfs output time for this job
		totalOutSize = -1
		if self.parity == 0:
			totalOutSize = self.computeNodeMemSize * self.chkpRatio * self.computeNodeNumber
		elif self.parity == 1:
			totalOutSize = self.computeNodeMemSize * self.chkpRatio * self.computeNodeNumber * 1.2

		pfsOutputTime = totalOutSize / self.pfsBandwidth
		print('job parity %d output size is %f' % (self.parity, totalOutSize))
		cycle = Job.findRestartPoint(self.cpRecs, self.computeNodeNumber, self.env.now, pfsOutputTime)
		#print('job %d tries to find recovery point %d' % (self.jobID, cycle))
		if restartFlag == 1:
			#there is a valid checkpoint
			#first check if BBs are still valid
			status = self.checkBBStatus()
			if status == 1:
				#since some bb node has failed, have to restage in
				self.reStageIn = 1
				self.compBBNodes = []
			if cycle > -1:
				cycleRecs = self.cpRecs[cycle] #get the bbs used for this cycle
			self.setComputeNodesWithRestart(cycle, completeQ, barriers, counter, status)
		else:
			#there is no valid chcekopint, just start nodes like normal
			#print('starting without restart')
			self.setComputeNodesWithoutRestart(completeQ, barriers, counter)
		
		bbReqSize = (totalOutSize / self.computeNodeNumber) / self.numBurstBuffer
		#if this is a fresh start, issue stagein
		if restartFlag != 1 or self.reStageIn == 1:
			retval = 0
			time1 = self.env.now
			while retval == 0:
				print('job %d started stage in at %f' % (self.jobID, self.env.now))
				stageInEvent = self.env.event()
				msg = (0, self.jobID, self.compBBNodes, bbReqSize, stageInEvent)
				try:
					yield self.pfsReqQ.put(msg)
					yield stageInEvent
					if stageInEvent.value == 0:
						retval = 1
						#job only stages in when first started or some bb node failed
						#during restart ,which means all progress is lost, need to start from beginning
						cycle = -1
						time3 = self.env.now
						self.stats.recordStageInBandwidth((totalOutSize / (time3 - time1)))
					elif stageInEvent.value == 1:
						print('job %d need to re-stage in due to bb failure' % self.jobID)
						self.repickBBNodes()
						#repick a set of BB nodes or each compute nodes
				except simpy.Interrupt as i:
					#either running out of time, or there is a fault at compute node
					msg = i.cause
					pfsMsg = (-1, self.jobID)
					yield self.pfsReqQ.put(pfsMsg)
					print('job %d notified pfs at %f' % (self.jobID, self.env.now))
					if msg == -1:
						self.shutDown()
						yield AllOf(self.env, completeQ)
						print('all compute nodes shut down')
					else:
						#send cancel message to pfs
						self.latestFaultTime = self.env.now
						self.reStageIn = 1 #interrupted during stage in
						self.stats.recordFault()
						self.handleFault(msg, completeQ)
						self.freshStart = 1
						#for x in self.handleFault(msg, completeQ):
						#	yield x
					time2 = self.env.now
					self.stats.recordStageInTime(time2 - time1)
					self.updateComputeNodeIdleNonIdleFinishTime()
					self.releaseNodes(msg)
					self.stats.recordFinishTime(self.env.now)
					self.finish.succeed()
					print('job %d terminated at %f due to stage in due to comp node fail' % (self.jobID, self.env.now))
					return
			time2 = self.env.now
			self.stats.recordStageInTime(time2 - time1)
		self.reStageIn = 0
		
		#start compute nodes
		self.startComputeNodeProcs(restartFlag, cycle)

		job_done = 0
		msg = None
		while job_done == 0:
			try:
				print('job id %d waiting to finish' % self.jobID)
				yield AllOf(self.env, completeQ)
				job_done = 1
				print('job %d is done at %f' % (self.jobID, self.env.now))
				stageOut = 1
			except simpy.Interrupt as i:
				msg = i.cause
				#print('job %d interrupted at %f cause node %d is wrong' % (self.jobID, self.env.now, msg))
				if msg == -1:
					print('job %d running out of time, force to terminate' % self.jobID)
					self.shutDown()
					yield AllOf(self.env, completeQ)
					print('all compute nodes shut down')
					stageOut = 1
				elif msg == -2:
					#we know restart failed due to some bb failed, need to restart and re stage-in
					print('job %d interrupted during recovery due to bad bb at %f' % (self.jobID, self.env.now))
					#need to 
					self.reStageIn = 1
					self.stats.recordFault()
					#self.handleFault(msg, completeQ)
					self.handleFault(msg, completeQ)
					
					#tell scheduler to restart the job
					print('job %d interrupting scheduler to restart' % self.jobID)
					self.schedulerProc.interrupt((self.jobID, 1))
				else:
					self.latestFaultTime = self.env.now
					self.stats.recordFault()
					self.handleFault(msg, completeQ)
				job_done = 2
		#release the nodes
		#self.releaseNodes(msg)
		if stageOut == 1:
			print('job %d started stageout at %f' % (self.jobID, self.env.now))
			time1 = self.env.now
			try:
				stageOutEvent = self.env.event()
				stageOutMsg = (1, self.jobID, self.compBBNodes, bbReqSize, stageOutEvent)
				yield self.pfsReqQ.put(stageOutMsg)
				yield stageOutEvent
				if stageOutEvent.value == 1:
					#stageOut Failed due to some bb node failed, need to restart from beginning
					print('job %d stage out failed at %f' % (self.jobID, self.env.now))
					self.latestFaultTime = self.env.now
					self.reStageIn = 1
					self.freshStart = 1
					self.stats.recordFault()
					#signal the scheduler to restart the job
					self.schedulerProc.interrupt((self.jobID, 1))
				elif stageOutEvent.value == 0:
					print('job %d stage out done good at %f' % (self.jobID, self.env.now))
					time3 = self.env.now
					self.stats.recordStageOutBandwidth((totalOutSize / (time3 - time1)))
			except simpy.Interrupt as i:
				msg = i.cause
				pfsMsg = (-1, self.jobID)
				yield self.pfsReqQ.put(pfsMsg)
				print('job %d interrupted during stageout at %f' % (self.jobID, self.env.now))
				if msg == -1:
					self.shutDown()
					yield AllOf(self.env, completeQ)
				else:
					#compute node fault
					print('compute node fault, cancel stage out')
					#self.freshStart = 1
					self.latestFaultTime = self.env.now
					self.stats.recordFault()
					self.handleFault(msg, completeQ)
			time2 = self.env.now
			self.stats.recordStageOutTime(time2 - time1)
		self.updateComputeNodeIdleNonIdleFinishTime()
		self.releaseNodes(msg)
		self.stats.recordFinishTime(self.env.now)
		self.finish.succeed()
		#print('total compute time %f' % self.stats.totalComputeTime)
		print("job %d terminated at %f" % (self.jobID, self.env.now))

	def checkBBStatus(self):
		status = 0
		for i in self.compBBNodes:
			for j in i:
				if self.bbNodes[j].status != 0:
					status = 1
					print('job %d bb status check found a bad bb' % self.jobID)
					return status
		print('job %d bb check pass' % self.jobID)
		return status

	def shutDown(self):
		length = len(self.computeNodeProcs)
		for i in range(length):
			print('job %d shut down stopping comp %d' % (self.jobID, self.computeNodeProcs[i].nodeID))
			self.computeNodeProcs[i].run.interrupt(-1)

	def setComputeBBNodes(self, completeQ, barriers):
		for i in range(self.computeNodeNumber):
			complete = self.env.event()

	def startComputeNodeProcs(self, restartFlag, cycle):
		if restartFlag == 1 and cycle > -1:
			cycleRecs = self.cpRecs[cycle]
			for i in range(self.computeNodeNumber):
				compNode = self.computeNodeProcs[i]
				compNode.createProcess(cycle, self.stats, self.currentTime)
		else:
			for i in range(self.computeNodeNumber):
				compNode = self.computeNodeProcs[i]
				compNode.createProcess(-1, self.stats, self.currentTime)

	def repickBBNodes(self):
		self.compBBNodes = []
		for i in range(self.computeNodeNumber):
			compNode = self.computeNodeProcs[i]
			compNode.assignBBNodes()
			self.compBBNodes.append(compNode.bbToUse)

	def updateComputeNodeIdleNonIdleFinishTime(self):
		for i in range(self.computeNodeNumber):
			compNode = self.computeNodeProcs[i]
			compNode.updateEndIdleNonIdleTime()

	def setComputeNodesWithoutRestart(self, completeQ, barriers, counter):
		for i in range(self.computeNodeNumber):
			complete = self.env.event()
			completeQ.append(complete)
			compNode = self.computeNodeProcs[i]
			compNode.updateStartIdleNonIdleTime()
			compNode.setNodeLocalID(i)
			compNode.setJobComputeNodeCount(self.computeNodeNumber)
			compNode.setJobID(self.jobID)
			compNode.setJobLength(self.jobLen)
			compNode.setTotalBurstBufferNumber(self.totalBBNum)
			compNode.setBurstBufferUsageNumber(self.numBurstBuffer)
			compNode.setOutputRatio(self.chkpRatio)
			compNode.setCheckpointFreq(self.cpFreq)
			compNode.setCompleteEvent(complete)
			compNode.setBarriers(barriers)
			compNode.setChkpRecord(self.cpRecs)
			compNode.assignBBNodes()
			self.compBBNodes.append(compNode.bbToUse)
			compNode.jobProc = self.run
			#compNode.syncCounter = counter
			#compNode.createProcess(-1, None, self.stats, self.currentTime)
		print('job bb nodes')
		print(self.compBBNodes)

	def setComputeNodesWithRestart(self, cycle, completeQ, barriers, counter, status):
		for i in range(self.computeNodeNumber):
			complete = self.env.event()
			completeQ.append(complete)
			compNode = self.computeNodeProcs[i]
			#print('%dth compute node id %d' % (i, compNode.nodeID))
			compNode.updateStartIdleNonIdleTime()
			compNode.setNodeLocalID(i)
			compNode.setJobID(self.jobID)
			compNode.setJobComputeNodeCount(self.computeNodeNumber)
			compNode.setJobLength(self.jobLen)
			compNode.setTotalBurstBufferNumber(self.totalBBNum)
			compNode.setBurstBufferUsageNumber(self.numBurstBuffer)
			compNode.setOutputRatio(self.chkpRatio)
			compNode.setCheckpointFreq(self.cpFreq)
			compNode.setBarriers(barriers)
			compNode.setCompleteEvent(complete)
			compNode.setChkpRecord(self.cpRecs)
			if status == 0:
				compNode.bbToUse = self.compBBNodes[i]
			elif status == 1:
				compNode.assignBBNodes()
				self.compBBNodes.append(compNode.bbToUse)
			compNode.jobProc = self.run
			#compNode.syncCounter = counter
			#compNode.createProcess(cycle, cycleRecs[i], self.stats, self.currentTime)
		print('job restart bb nodes check')
		print(self.compBBNodes)

	@staticmethod
	def findRestartPoint(cpRecs, numComputeNodes, timeNow, pfsOutputTime):
	#this function will find a valid restart cycle to begin with
		keys = cpRecs.keys()
		length = len(keys)
		cycle = -1
		if length > 0:
			#in this case, there is more than 1 cycle recorded
			for i in range(length - 1, -1, -1):
				cycleRecs = cpRecs[i]
				nodes = list(cycleRecs.keys())
				if len(nodes) == numComputeNodes:
					#this cycle has a complete checkpoint, we can use this one
					print('find restart point cycle %d' % i)
					count = 0
					for j in nodes:
						statFlag = cycleRecs[j][3]
						count = count + statFlag
					if count == numComputeNodes:
						cycle = i
						break
					print('count is %d' % count)
					#nodeID = nodes[0]
					#statFlag = cycleRecs[nodeID][3]
					#if statFlag == 0:
						#time = cycleRecs[nodeID][0]
						#duration = cycleRecs[nodeID][1]
						#print('cycle %d cp time passed %f' %(i, timeNow - time))
						#if ((timeNow - time) >= pfsOutputTime):
					#	cycle = i
					#	break
		else:
			print('job no valid cp')
		return cycle

	def handleFault(self, msg, completeQ):
		print('in handle fault')	
		length = len(self.computeNodeProcs)
		self.computeNodeIndices = []
		#interrupt all compute nodes cuz there is a fault in some compute node
		#print('job %d sending fault interrupt to compute node %d' % (self.jobID, self.computeNodeProcs[i].nodeID))
		for i in range(length):
			#print('job %d sending fault interrupt to compute node %d' % (self.jobID, self.computeNodeProcs[i].nodeID))
			if self.computeNodeProcs[i].run != None and self.computeNodeProcs[i].run.is_alive:
				self.computeNodeProcs[i].run.interrupt(msg)

		print('job %d finished sending fault interrupt to all compute node' % self.jobID)
		print('all compute nodes donen')

	def releaseNodes(self, wrongNode):
		length = len(self.computeNodeProcs)
		if wrongNode == None or wrongNode < 0:
			print('job %d relaseing all nodes no bad dnoes' % self.jobID)
			count = 0
			for i in range(length):
				self.computeNodeProcs[i].nodeStat = -1
				self.computeNodeProcs[i].updateEndIdleNonIdleTime()
				count = count + 1
			self.currentAvailable[0] = self.currentAvailable[0] + count
		else:
			print('job %d releasing all nodes with bad node %d' % (self.jobID, wrongNode))
			#only need to change node status for good nodes
			count = 0
			for i in range(length):
				if self.computeNodeProcs[i].nodeID != wrongNode:
					#this is a working node
					self.computeNodeProcs[i].nodeStat = -1
					count = count + 1
				self.computeNodeProcs[i].updateEndIdleNonIdleTime()
			self.currentAvailable[0] = self.currentAvailable[0] + count

		self.computeNodeProcs = []

	def setJobType(self, jobType):
		self.jobType = jobType

	def setPipeID(self, pipeID):
		self.pipeID = pipeID

	def setJobID(self, jobID):
		self.jobID = jobID

	def setScheduleTime(self, time):
		self.timeToSchedule = time
	
	def setJobLen(self, duration):
		self.jobLen = duration

	def setNodeNumber(self, number):
		self.computeNodeNumber = number
		
	def setNodeMemSize(self, size):
		self.computeNodeMemSize = size

	def setCheckpointMemoryRatio(self, ratio):
		self.chkpRatio = ratio
	
	def setNodeBandwidth(self, bw):
		self.nodeBandwidth = bw

	def setBurstBufferUseNumber(self, count):
		self.numBurstBuffer = count

	def setStripePolicy(self, policy):
		self.stripePolicy = policy
	
	def setChkpFreq(self, chkptFreq):
		self.cpFreq = chkptFreq
