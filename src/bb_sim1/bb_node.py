import simpy
import pprint
class BBNode:
	
	def __init__(self, env, myid, bandwidth, requestQ, log_fd, pfsReqQ, writeBw, readBw):
		self.env = env
		self.reqid = 0
		self.reqCount = 0
		self.status = 0 #0 - working; -1 - unavailable
		self.bbid = myid
		self.reqQ = requestQ
		self.pfsReqQ = pfsReqQ
		self.bw = bandwidth
		self.readBw = readBw
		self.writeBw = writeBw
		self.sharecnt = []
		self.log = log_fd
		self.sharecnt.append(0)
		self.sharecnt.append(0)
		self.start = env.process(self.run())

	#polls request from reqQ, then creates an executor to process request, bandwidth is affected when there are concurrent requests
	def run(self):
		self.log.write("%d %d %f\n" % (self.bbid, self.reqCount, self.env.now))
		ioreqs = dict()#map request id to its execute process
		nodeToExecutor = dict()
		pfsIOReqs = dict()
		msg = None
		nodeID = None
		event = None
		outputsize = None
		jobID = None
		jobReqID = None
		state = 0
		req = None
		stageType = None
		pfsBandwidth = None
		reqType = None
		while True:
			if state == 0:
					currentid = self.reqid
				#try:
					#print('bb node %d ready to get request at %f' %(self.bbid, self.env.now))
					#req = self.reqQ.get()
					msg = yield self.reqQ.get()
					#print('bb node %d got a reqeust at %f' % (self.bbid, self.env.now))
					#yield self.env.timeout(0.0000000001)
					nodeID = msg[0]
					event = msg[1] # event used to notify compute node that the request for this bb node is done
					outputsize = msg[2]
					compNodeBW = msg[3]
					#print('bb %d got request %d from compute node %d with outsize %f nodebw %f at time %f' %(self.bbid, currentid, nodeID, outputsize, compNodeBW, self.env.now))
					#when nodeID == -1, this is bb node fail message
					if nodeID == -1:
						#print('bbnode %d gets a fault at %f' % (self.bbid, self.env.now))
						self.status = -1
					#	#terminates all on going executors
						self.endAllExecutors(ioreqs, pfsIOReqs)
					#when event == none and outputsize == none, this is a compute node bandwidth change message
					elif nodeID == -3:
						#this is a cancel message from pfs, cancle all on-going requests for this pfs
						jobID = msg[4]
						#print('bb %d got cancle stage in req from job %d' % (self.bbid, jobID))
						if jobID in pfsIOReqs:
							reqList = pfsIOReqs[jobID]
							for i in reqList:
								if i[0].is_alive:
									#print('bb %d interrupting stage in executor' % self.bbid)
									i[0].interrupt('finish')
									#self.sharecnt[0] = self.sharecnt[0] - 1
					elif nodeID == -2:
						#this is a stage in request from pfs
						state = 2
						jobID = msg[4]
						jobReqID = msg[5]
						stageType = msg[6]
						pfsBandwidth = msg[7]
					elif event == None and outputsize == None:
						#print('bb %d got a change of bw message from node %d with new bw %f' % (self.bbid, nodeID, compNodeBW))
						#need to notify executor for nodeID on this BB that there is a change in compute nodeID bandwidth
						if nodeID in nodeToExecutor:
							exe = nodeToExecutor[nodeID]
							if exe.is_alive:
								exe.interrupt(compNodeBW)
					#when outputsize == -1, this is a compute node fail message
					elif outputsize == -1:
						#print('bbnode %d interrupted by compute node %d at %f' % (self.bbid, nodeID, self.env.now))
						if nodeID in nodeToExecutor:
							exe = nodeToExecutor[nodeID]
							if exe.is_alive:
								#print('current executor for node %d is still running, interrupting it to finish' % nodeID)
								exe.interrupt('computeNodeFault')
						#else:
							#print('there is no executor for node %d' % nodeID)
						state = 0
					else:
						#print('bb %d got request %d from compute node %d with outsize %f at time %f' %(self.bbid, currentid, nodeID, outputsize, self.env.now))
						state = 1
						reqType = msg[4]

			elif state == 1:
				if self.status == -1:
					#print('bbnode %d currently unavailable' % self.bbid)
					event.succeed(value = 1)
					state = 0
				else:
					#msg = None
					#req = None
					if reqType == 0:
						#print('bb %d got write request from compte %d' % (self.bbid, nodeID))
						self.sharecnt[0] = self.sharecnt[0] + 1 #increase share count
					elif reqType == 1:
						#print('bb %d got read request from compute %d' % (self.bbid, nodeID))
						self.sharecnt[1] = self.sharecnt[1] + 1
					done = self.env.event() #complete event for notifying other executors that there might be a change in sharing
					self.reqCount = self.reqCount + 1
					self.log.write("%d %d %f\n" % (self.bbid, self.reqCount, self.env.now))
					executor = self.env.process(self.execute(event, done, currentid, outputsize, compNodeBW, reqType))
					nodeToExecutor[nodeID] = executor
					ioreqs[currentid] = executor
	
					#uses interrupt to inform other on-going requests that sharing is increased
					keys = ioreqs.keys()
					for k in keys:
						if k != currentid:
							proc = ioreqs[k]
							if proc.is_alive:
								proc.interrupt('up')
					keys = pfsIOReqs.keys()
					for k in keys:
						reqList = pfsIOReqs[k]
						for i in reqList:
							if i[0].is_alive:
								i[0].interrupt('up')

					#start a complete process to asynchronously wait for req to complete
					finish = self.env.process(self.complete(currentid, done, ioreqs, pfsIOReqs, reqType, nodeID, nodeToExecutor))
					self.reqid = self.reqid + 1
					state = 0

			#in stage in/out request
			elif state == 2:
				if self.status == -1:
					#print('bbnode %d unavailable' % self.bbid)
					event.succeed(value = 1)
				else:
					if stageType == 0:
					#	print('bb %d got stage in req for job %d with jobreqid %d' % (self.bbid, jobID, jobReqID))
						self.sharecnt[0] = self.sharecnt[0] + 1
					elif stageType == 1:
					#	print('bb %d got stage out req for job %d with jobreqid %d' % (self.bbid, jobID, jobReqID))
						self.sharecnt[1] = self.sharecnt[1] + 1
					self.reqCount = self.reqCount + 1
					done = self.env.event()
					self.log.write("%d %d %f\n" % (self.bbid, self.reqCount, self.env.now))
					executor = self.env.process(self.stageInExecute(currentid, outputsize, event, done, jobID, jobReqID, stageType, pfsBandwidth))
					if jobID in pfsIOReqs:
						reqList = pfsIOReqs[jobID]
						reqList.append((executor, jobReqID))
					else:
						reqList = []
						reqList.append((executor, jobReqID))
						pfsIOReqs[jobID] = reqList

					#notice other exectors increase in sharing
					keys = ioreqs.keys()
					for k in keys:
						proc = ioreqs[k]
						if proc.is_alive:
							proc.interrupt('up')
					keys = pfsIOReqs.keys()
					for k in keys:
						reqList = pfsIOReqs[k]
						for i in reqList:
							if k != jobID or i[1] != jobReqID:
								if i[0].is_alive:
									i[0].interrupt('up')
					finish = self.env.process(self.pfsIOComplete(jobID, jobReqID, done, pfsIOReqs, ioreqs, stageType))
					self.reqid = self.reqid + 1
				state = 0

	def pfsIOComplete(self, jobID, jobReqID, done, pfsIOReqs, ioReqs, stageType):
		yield done
		self.reqCount = self.reqCount - 1
		self.log.write("%d %d %f\n" % (self.bbid, self.reqCount, self.env.now))
		try:
			if done.value == 1 or done.value == 3:
				#print('complete exe cancelling job %d bb %d' % (jobID, self.bbid))
				#cleanup records for this job
				if jobID in pfsIOReqs:
					reqList = pfsIOReqs[jobID]
					if len(reqList) > 0:
						del reqList[:]
					del pfsIOReqs[jobID]
					#print('deleting job %d record on bb %d' % (jobID, self.bbid))
				if stageType == 0:
					#print('stage in req job %d job reqid %d done at %f' % (jobID, jobReqID, self.env.now))
					self.sharecnt[0] = self.sharecnt[0] - 1
				elif stageType == 1:
					#print('stage out req job %d job reqid %d done at %f' % (jobID, jobReqID, self.env.now))
					self.sharecnt[1] = self.sharecnt[1] - 1
				return
		except AttributeError:
			pass

		if stageType == 0:
			#print('stage in req job %d job reqid %d done at %f' % (jobID, jobReqID, self.env.now))
			self.sharecnt[0] = self.sharecnt[0] - 1
		elif stageType == 1:
			#print('stage out req job %d job reqid %d done at %f' % (jobID, jobReqID, self.env.now))
			self.sharecnt[1] = self.sharecnt[1] - 1
		keys = pfsIOReqs.keys()
		for k in keys:
			reqList = pfsIOReqs[k]
			for i in reqList:
				if k != jobID or i != jobReqID:
					if i[0].is_alive:
						i[0].interrupt('down')
		keys = ioReqs.keys()
		for k in keys:
			proc = ioReqs[k]
			if proc.is_alive:
				proc.interrupt('down')
		#remove	
		#if stageType == 0:
		#	print('stage in req job %d job reqid %d done at %f' % (jobID, jobReqID, self.env.now))
		#elif stageType == 1:
		#	print('stage out req job %d job reqid %d done at %f' % (jobID, jobReqID, self.env.now))

		reqList = pfsIOReqs[jobID]
		length = len(reqList)
		target = None
		for i in range(length):
			if reqList[i][1] == jobReqID:
				target = i
				break

		del reqList[target]
		if len(reqList) == 0:
			#remove this item from pfs book keeping dictionary
			#if stageType == 0:
			#	print('stage in reqs for job %d all done on bb %d' % (jobID, self.bbid))
			#elif stageType == 1:
			#	print('stage in reqs for job %d all done on bb %d' % (jobID, self.bbid))
			del pfsIOReqs[jobID]

	def complete(self, reqid, done, ioreqs, pfsIOReqs, reqType, nodeID, nodeToExecutor):
		yield done
		self.reqCount = self.reqCount - 1
		self.log.write("%d %d %f\n" % (self.bbid, self.reqCount, self.env.now))
		try:
			if done.value == 1:
				del ioreqs[reqid]
				del nodeToExecutor[nodeID]
				if reqType == 0:
					self.sharecnt[0] = self.sharecnt[0] - 1
					#print('bb %d write req %d from comp node %d interrupted at %f' % (self.bbid, reqid, nodeID, self.env.now))
				elif reqType == 1:
					self.sharecnt[1] = self.sharecnt[1] - 1
					#print('bb %d read req %d from comp node %d interrupted at %f' % (self.bbid, reqid, nodeID, self.env.now))
				#self.sharecnt[0] = self.sharecnt[0] - 1
				return
		except AttributeError:
			pass
		#print('bbnode %d reqid %d finished at %f' %(self.bbid, reqid, self.env.now))
		if reqType == 0:
			self.sharecnt[0] = self.sharecnt[0] - 1
			#print('bb %d write req %d from comp node %d finished at %f' % (self.bbid, reqid, nodeID, self.env.now))
		elif reqType == 1:
			self.sharecnt[1] = self.sharecnt[1] - 1
			#print('bb %d read req %d from comp node %d finished at %f' % (self.bbid, reqid, nodeID, self.env.now))
		#uses interrupt to inform other on-going requests that sharing is decreased
		keys = ioreqs.keys()
		for k in keys:
			if k != reqid:
				proc = ioreqs[k]
				if proc.is_alive:
					proc.interrupt('down')
		#now take this finished request out of dictionary
		del ioreqs[reqid]
		del nodeToExecutor[nodeID]
		#tell pfs io reqs there is a change in count
		keys = pfsIOReqs.keys()
		for k in keys:
			reqList = pfsIOReqs[k]
			for i in reqList:
				if i[0].is_alive:
					i[0].interrupt('down')
					
	def stageInExecute(self, currentid, outputsize, event, done, jobID, jobReqID, stageType, pfsBandwidth):
		complete = 0
		startT = float(self.env.now)
		remainsize = float(outputsize)
		if stageType == 0:
			bandwidth = float(self.writeBw)/ float(self.sharecnt[0])
			print('stage in executor got pfs bw %f' % pfsBandwidth)
			#print('stage in req executor job %d jobreqid %d shared degree %d remainsize %f starting bandwidth %f' % (jobID, jobReqID, self.sharecnt[0], remainsize, bandwidth))
		elif stageType == 1:
			bandwidth = float(self.readBw)/ float(self.sharecnt[1])
			#print('stage out req executor job %d jobreqid %d shared degree %d remainsize %f starting bandwidth %f' % (jobID, jobReqID, self.sharecnt[1], remainsize, bandwidth))
		#bandwidth = float(self.bw)/ float(self.sharecnt[0])
		if bandwidth > pfsBandwidth:
			bandwidth = pfsBandwidth
			print('bb %d executor using pfs bandwidth %f' % (self.bbid, pfsBandwidth))
		remainT = float(outputsize) / bandwidth#float((self.bw/float(self.sharecnt[0])))

		bbNodeEnd = 0
		while complete == 0:
			try:
				yield self.env.timeout(remainT)
				complete = 1
			except simpy.Interrupt as i:
				msg = i.cause
				currentT = float(self.env.now)
				if msg == 'down' and complete == 0:
					remainsize = BBNode.calculate_remainsize(remainsize, currentT, startT, bandwidth)
					if stageType == 0:
						retval = BBNode.stageInCalculateRemainT(remainsize, self.writeBw, self.sharecnt, stageType)
					elif stageType == 1:
						retval = BBNode.stageInCalculateRemainT(remainsize, self.readBw, self.sharecnt, stageType)
					remainT = retval[0]
					bandwidth = retval[1]
					#print("bb %d job %d jobreqid %d decrease shared on stae in at %f, remain size %f remainT %f" % (self.bbid, jobID, jobReqID, self.env.now, remainsize, remainT))
				elif msg == 'up' and complete == 0:
					remainsize = BBNode.calculate_remainsize(remainsize, currentT, startT, bandwidth)
					if stageType == 0:
						retval = BBNode.stageInCalculateRemainT(remainsize, self.writeBw, self.sharecnt, stageType)
					elif stageType == 1:
						retval = BBNode.stageInCalculateRemainT(remainsize, self.readBw, self.sharecnt, stageType)
					remainT = retval[0]
					bandwidth = retval[1]
					#print('bb %d job %d jobreqid %d increase shared on on stage in % f, remain size %f remainT %f' % (self.bbid, jobID, jobReqID, self.env.now, remainsize, remainT))
				elif msg == 'finish':
					#print('bb %d executor canceling stagein/out' % self.bbid)
					complete = 1
					bbNodeEnd = 2
				elif msg == 'end' and complete == 0:
					complete = 1
					bbNodeEnd = 1
				if bbNodeEnd != 1:
					startT = currentT

		if bbNodeEnd == 1:
			event.succeed(value = 1)
			done.succeed(value = 1)
		elif bbNodeEnd == 2:
			event.succeed(value = 1)
			done.succeed(value = 3)
		else:
			event.succeed(value = 2)
			done.succeed(value = 2)
	
	def execute(self, event, done, currentid, outputsize, compNodeBW, reqType):	
		complete = 0
		#if python could specify types, that would be great
		startT = float(self.env.now)
		remainsize = float(outputsize)
		bandwidth = None
		if reqType == 0: #write request
			bandwidth = float(self.writeBw)/ float(self.sharecnt[0])
			#print('bb %d start write bw %f with sharing %d at %f' % (self.bbid, bandwidth, self.sharecnt[0], self.env.now))
		elif reqType == 1: #read request
			bandwidth = float(self.readBw)/ float(self.sharecnt[1])
			#print('bb %d start read bw %f with sharing %d at %f' % (self.bbid, bandwidth, self.sharecnt[1], self.env.now))
		#print('bb %d req %d starting bw %f' % (self.bbid, currentid, thisBandwidth))
		if bandwidth > compNodeBW:
			#print('bb %d req %d limited by compute node bw %f' % (self.bbid, currentid, compNodeBW))
			bandwidth = compNodeBW
			#print('bb %d req %d is limited by its own shared bandwidth %f' % (self.bbid, currentid, bandwidth))

		remainT = float(outputsize) / bandwidth#float((self.bw/float(self.sharecnt[0])))
		#print('bb %d req %d start time %f with sharing %d initial waittime %f'%(self.bbid, currentid, self.env.now, self.sharecnt[0], remainT))
		bbNodeEnd = 0
		while complete == 0:
			try:
				yield self.env.timeout(remainT)
				complete = 1
			except simpy.Interrupt as i:
				msg = i.cause
				currentT = float(self.env.now)
				if msg == 'down' and complete == 0:
					#print('bb %d exeproc %d got to know some procs finished at time %f with new sharing degree %d' %(self.bbid, currentid, currentT, self.sharecnt[0]))
					remainsize = BBNode.calculate_remainsize(remainsize, currentT, startT, bandwidth)
					#print('bb %d exeproc %d remain job size %f' %(self.bbid, currentid, remainsize))
					if reqType == 0:
						retval = BBNode.calculate_remainT(remainsize, self.writeBw, self.sharecnt, compNodeBW, reqType)
						#print('bb %d write decrease sharing to %d new bw %f remainT %f' % (self.bbid, self.sharecnt[0], retval[1], retval[0]))
					elif reqType == 1:
						retval = BBNode.calculate_remainT(remainsize, self.readBw, self.sharecnt, compNodeBW, reqType)
						#print('bb %d read decrease sharing to %d new bw %f remainT %f' % (self.bbid, self.sharecnt[1], retval[1], retval[0]))
					#print('bb %d req %d new bw %f new waittime %f after decrease of sharing' % (self.bbid, currentid, self.bw/self.sharecnt[0], remainT))
					remainT = retval[0]
					bandwidth = retval[1]
				elif msg == 'up' and complete == 0:
					#print('bb %d exeproc %d got to know increased sharing at time %f with new sharing degree %d' % (self.bbid, currentid, self.env.now, self.sharecnt[0]))
					remainsize = BBNode.calculate_remainsize(remainsize, currentT, startT, bandwidth)
					#print('bb %d exeproc %d remain job size %f' %(self.bbid, currentid, remainsize))
					if reqType == 0:
						retval = BBNode.calculate_remainT(remainsize, self.writeBw, self.sharecnt, compNodeBW, reqType)
						#print('bb %d write increase sharing to %d new bw %f remainT %f' % (self.bbid, self.sharecnt[0], retval[1], retval[0]))
					elif reqType == 1:
						retval = BBNode.calculate_remainT(remainsize, self.readBw, self.sharecnt, compNodeBW, reqType)
						#print('bb %d read decrease sharing to %d new bw %f remainT %f' % (self.bbid, self.sharecnt[0], retval[1], retval[0]))
					remainT = retval[0]
					bandwidth = retval[1]
					#print('bb %d req %d new bw %f new waittime %f after increase of sharing' % (self.bbid, currentid, float(self.bw/(self.sharecnt[0])), remainT))
				elif msg == 'computeNodeFault' and complete == 0:
					#print('bb node %d executor req %d interrupted due to node fault' %(self.bbid, currentid))
					complete = 1
				elif msg == 'end' and complete == 0:
					#print('bb node %d executor req %d interrupted due to bb node fault' %(self.bbid, currentid))
					complete = 1
					bbNodeEnd = 1
				else:
					#print('bb node %d executor req %d interrupted due to change of compute node new bandwidth %f' % (self.bbid, currentid, msg))
					if complete == 0:
						compNodeBW = msg
						remainsize = BBNode.calculate_remainsize(remainsize, currentT, startT, bandwidth)
						if reqType == 0:
							retval = BBNode.calculate_remainT(remainsize, self.writeBw, self.sharecnt, compNodeBW, reqType)
							#print('bb %d write change compute bandiwdth new bw %f remainT %f' % (self.bbid, retval[1], retval[0]))
						elif reqType == 1:
							retval = BBNode.calculate_remainT(remainsize, self.readBw, self.sharecnt, compNodeBW, reqType)
							#print('bb %d read change  sharing to %d new bw %f remainT %f' % (self.bbid, retval[1], retval[0]))
						#print('bb node %d executor req %d new wait time %f after change in compute node bandwidth at %f' % (self.bbid, currentid, remainT, self.env.now))
						remainT = retval[0]
						bandwidth = retval[1]	
				if bbNodeEnd != 1:
					#bandwidth = float(self.bw)/float(self.sharecnt[0])
					startT = currentT
		if bbNodeEnd == 1:
			#ended due to bb node fault
			event.succeed(value = 1)
			done.succeed(value = 1)
			#print('bbnode %d req % ended due to bb node fault at %f' % (self.bbid, currentid, self.env.now))
		else:
			event.succeed(value = 2)
			done.succeed(value = 2)
			#print('bb %d req %d finished at %f' % (self.bbid, currentid, self.env.now))

	def endAllExecutors(self, ioreqs, pfsIOReqs):
		print('bbnode %d terminating all on going io requests, keys' % self.bbid)
		keys = ioreqs.keys()
		for i in keys:
			executor = ioreqs[i]
			if executor.is_alive:
				#print('bbnode %d interrupting executor %d' % (self.bbid, i))
				executor.interrupt('end')

		keys = pfsIOReqs.keys()
		for k in keys:
			reqList = pfsIOReqs[k]
			for i in reqList:
				if i[0].is_alive:
					i[0].interrupt('end')
		
		#need to reset BB Node stat
		self.sharecnt[0] = 0

	@staticmethod
	def calculate_remainsize(jobsize, currentT, startT, bandwidth):
		duration = currentT - startT
		#print('duration %f, old bandwidth %f' % (duration, bandwidth))
		remainsize = jobsize - (duration * bandwidth)

		if remainsize <= 0:
			remainsize = 0
		#print('remainsize %f' % remainsize)
		return remainsize
	@staticmethod
	def calculate_remainT(remainsize, bw, sharecnt, compNodeBW, reqType):
		if reqType == 0:
			thisBandwidth = float((bw/float(sharecnt[0])))
		elif reqType == 1:
			thisBandwidth = float((bw/float(sharecnt[1])))

		if thisBandwidth > compNodeBW:
			thisBandwidth = compNodeBW
			#print('limited by compute node bandwidth of %f!' % thisBandwidth)
		else:
			pass
			#print('limimted by current shared bb node bandwidth %f' % thisBandwidth)
		#print('new bandwidth %f' % thisBandwidth)
		remainT = float(remainsize)/thisBandwidth
		return (remainT,thisBandwidth)

	@staticmethod
	def stageInCalculateRemainT(remainsize, bw, sharecnt, stageType):
		if stageType == 0:
			bandwidth = float((bw/float(sharecnt[0])))
		elif stageType == 1:
			bandwidth = float((bw/float(sharecnt[1])))
		#print('new bw after change of  share %f' % bandwidth)
		remainT = float(remainsize)/bandwidth
		return (remainT, bandwidth)


