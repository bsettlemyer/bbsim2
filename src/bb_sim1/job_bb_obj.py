from stats import *
from simpy import *
import simpy
class Job_localBBs():
	
	def __init__(self, env, jobdone, fd_finish, currentAvailable, bbWrite, bbRead, parity):
		self.env = env
		self.jobType = None
		self.pipeID = -1
		self.jobID = -1
		self.finish = jobdone
		self.timeToSchedule = -1
		self.pfsBandwidth = -1
		self.cpFreq = -1
		self.jobLen = -1
		self.computeNodeNumber = -1
		self.chkpRatio = -1
		self.compNodeMemSize = -1
		self.computeNodeProcs = []
		self.bbWrite = bbWrite
		self.bbRead = bbRead
		self.cpRecs = dict()
		self.stats = Stats()
		self.fd_finish = fd_finish
		self.run = None
		self.previousNodes = []
		self.currentTime = [0]
		self.currentAvailable = currentAvailable
		self.parity = parity
		self.reStageIn = 0
		self.latestFaultTime = -1
		self.schedulerProc = None
	def begin_job(self, restartFlag):
		self.run = self.env.process(self.start_job(restartFlag))

	def getRestartTime(self, mode):
		totalOutSize = -1
		if self.parity == 0:
			totalOutSize = self.compNodeMemSize * self.chkpRatio * self.computeNodeNumber
		elif self.parity == 1:
			totalOutSize = self.compNodeMemSize * self.chkpRatio * self.computeNodeNumber * 1.2
		pfsOutputTime = totalOutSize / self.pfsBandwidth
		cycle = Job_localBBs.findRestartPoint(self.cpRecs, self.computeNodeNumber, self.env.now, pfsOutputTime)
		time = -1
		if cycle >= 0:
			time = 0
			for i in range(0, cycle + 1):
				recs = self.cpRecs[i]
				time = recs[0][2] + time

		return time

	#checks if the job has the same sets of compute nodes for this allocation as before
	def checkIfSameNodes(self):
		temp = []
		for i in range(self.computeNodeNumber):
			temp.append(self.computeNodeProcs[i].nodeID)

		if self.reStageIn == 1:
			return

		if set(temp) == set(self.previousNodes):
			print('check same set')
			self.reStageIn = 0
		else:
			print('check differnt set')
			self.reStageIn = 1

	def start_job(self, restartFlag):
		stageOut = 0
		done = 0
		barriers = [] #used to synchronzie between compute nodes
		completeQ = [] #used to tell job that all complete nodes are done

		for i in range(self.computeNodeNumber):
			barriers.append(None)

		#check for restart point	
		#first find total size
		totalOutSize = 0
		if self.parity == 0:
			totalOutSize = self.compNodeMemSize * self.chkpRatio * self.computeNodeNumber
		elif self.parity == 1:
			totalOutSize = self.compNodeMemSize * self.chkpRatio * self.computeNodeNumber * 1.2
		pfsOutputTime = totalOutSize / self.pfsBandwidth
		print('pfs output time %f' % pfsOutputTime)
		cycle = Job_localBBs.findRestartPoint(self.cpRecs, self.computeNodeNumber, self.env.now, pfsOutputTime)
		#config compute nodes
		self.setComputeNodes(restartFlag, cycle, self.cpRecs, completeQ, barriers)

		checkNodes = self.checkIfSameNodes()

		#stage in phase
		if restartFlag != 1 or self.reStageIn == 1:
			print('job %d starts stage in at %f' % (self.jobID, self.env.now))
			time0 = self.env.now
			waitTime = (totalOutSize / self.computeNodeNumber) / self.bbWrite
			try:
				yield self.env.timeout(waitTime)
				print('job %d stage in finished at %f' % (self.jobID, self.env.now))
				time1 = self.env.now
				self.stats.recordStageInBandwidth((totalOutSize / (time1 - time0)))
			except simpy.Interrupt as i:
				#stage in failed due to computeBB node fail
				msg = i.cause
				if msg == -1:
					print('job %d no more time during stage in at %f' % (self.jobID, self.env.now))
					self.terminate()
					yield AllOf(self.env, completeQ)
				else:
					print('job %d got a fault during stage in at %f' % (self.jobID, self.env.now))
					self.latestFaultTime = self.env.now
					self.reStageIn = 1
					self.stats.recordFault()
					self.handleFault(msg, completeQ)
					self.freshStart = 1
				time1 = self.env.now
				self.stats.recordStageInTime(time1 - time0)
				self.updateComputeNodeIdleNonIdleFinishTime()
				self.savePreviousNodes()
				self.releaseNodes(msg)
				self.stats.recordFinishTime(self.env.now)
				self.finish.succeed()
				return
		time1 = self.env.now
		self.stats.recordStageInTime(time1 - time0)
		self.startComputeNodeProcs(restartFlag, cycle)

		job_done = 0
		print('job %d started at %f' % (self.jobID, self.env.now))
		msg = None
		while job_done == 0:
			try:
				yield AllOf(self.env, completeQ)
				job_done = 1
				stageOut = 1
				#self.stats.recordFinishTime(self.env.now)
				#self.fd_finish.write("%d %d %f %f %f %f %f\n" % (self.jobID, self.computeNodeNumber, self.jobLen, self.timeToSchedule, self.stats.startTime, self.stats.enqTime, self.stats.finishTime))
			except simpy.Interrupt as i:
				msg = i.cause
				if msg == -1:
					print('job %d out of time' % self.jobID)
					stageOut = 1
					self.terminate()
					yield AllOf(self.env, completeQ)
					#self.stats.recordFinishTime(self.env.now)
					#self.fd_finish.write("%d %d %f %f %f %f %f\n" % (self.jobID, self.computeNodeNumber, self.jobLen, self.timeToSchedule, self.stats.startTime, self.stats.enqTime, self.stats.finishTime))
					print('all nodes shut down')
				else:
					self.latestFaultTime = self.env.now
					self.stats.recordFault()
					for x in self.handleFault(msg, completeQ):
						yield x
				job_done = 2
		print('job %d finished at %f' % (self.jobID, self.env.now))

		if stageOut == 1:
			print('job %d starts stage out at %f' % (self.jobID, self.env.now))
			time1 = self.env.now
			#start stageOut
			try:
				waitTime = (totalOutSize / self.computeNodeNumber) / self.bbRead
				yield self.env.timeout(waitTime)
				print('job %d finished stage out at %f' % (self.jobID, self.env.now))
				time3 = self.env.now
				self.stats.recordStageOutBandwidth((totalOutSize / (time3 - time1)))
			except simpy.Interrupt as i:
				#stage out failed, need to restart
				msg = i.cause
				if msg == -1:
					#ran out of time..code will no get here..
					print('job %d ran out of time during stage out at %f' % (self.jobID, self.env.now))
					self.terminate()
					yield AllOf(self.env, completeQ)
				else:
					#fault during stage out, must restart from beginning
					print('job %d had a faul during stage out %f' % (self.jobID, self.env.now))
					self.latestFault = self.env.now
					self.stats.recordFault()
					self.handleFault(msg, completeQ)
			time2 = self.env.now
			self.stats.recordStageOutTime(time2 - time1)

		self.updateComputeNodeIdleNonIdleFinishTime()
		self.savePreviousNodes()
		self.releaseNodes(msg)
		self.stats.recordFinishTime(self.env.now)
		#self.computeNodeProcs = []
		self.finish.succeed()

	def savePreviousNodes(self):
		self.previousNodes = []
		for i in range(self.computeNodeNumber):
			self.previousNodes.append(self.computeNodeProcs[i].nodeID)

	def updateComputeNodeIdleNonIdleFinishTime(self):
		print('job %d update end idle nonidle time at %f' % (self.jobID, self.env.now))
		for i in range(self.computeNodeNumber):
			compNode = self.computeNodeProcs[i]
			compNode.updateEndIdleNonIdleTime()

	def handleFault(self, msg, completeQ):
		for i in range(self.computeNodeNumber):
			#print('job %d sending interrupt to cn node %d' % (self.jobID, self.computeNodeProcs[i].nodeID))
			self.computeNodeProcs[i].run.interrupt(msg)

		yield AllOf(self.env, completeQ)
		#self.releaseNodes(msg)

	def terminate(self):
		print('job %d shutting down all compute nodes' % self.jobID)
		for i in range(self.computeNodeNumber):
			self.computeNodeProcs[i].run.interrupt(-1)

	def releaseNodes(self, wrongNode):
		if wrongNode == None or wrongNode < 0:
			count = 0
			for i in range(0, self.computeNodeNumber):
				self.computeNodeProcs[i].nodeStat = -1
				count = count + 1
			self.currentAvailable[0] = self.currentAvailable[0] + count
		else:
			count = 0
			for i in range(0, self.computeNodeNumber):
				if self.computeNodeProcs[i].nodeID != wrongNode:
					self.computeNodeProcs[i].nodeStat = -1
					count = count + 1
			self.currentAvailable[0] = self.currentAvailable[0] + count
		print('after job %d realsed %d nodes, current available nodes %d' % (self.jobID, count, self.currentAvailable[0]))
		self.computeNodeProcs = []

	@staticmethod
	def findRestartPoint(cpRecs, numComputeNodes, timeNow, pfsOutputTime):
		keys = cpRecs.keys()
		print('cp rec keys')
		print(keys)
		length = len(keys)
		print('length %d' % length)
		cycle = -1

		if length > 0:
			#more than 1 cycle recorded
			for i in range(length - 1, -1, -1):
				print('current i %d' % i)
				cycleRecs = cpRecs[i]
				nodes = list(cycleRecs.keys())
				if len(nodes) == numComputeNodes:
					#this is the latest completed checkpoint
					#value format is (time when cp finished, duration of the cp writing)
					nodeID = nodes[0]
					time = cycleRecs[nodeID][0]
					duration = cycleRecs[nodeID][1]
					print('time now %f cp time %f duration is %f' % (timeNow, time, duration))
					#we can use this checkpoint if it is completed drained from burst buffer
					if((timeNow - time) >= pfsOutputTime):
						cycle = i
						break

		print('returend checkpoint cycle %d ' % cycle)
		return cycle

	def setComputeNodes(self, restartFlag, cycle, cpRecs, completeQ, barriers):
		
		cycleRecs = None
		validCp = (restartFlag == 1 and cycle > -1)
		print(validCp)
		if restartFlag == 1 and cycle > -1:
			print('job % have a valid cp' % self.jobID)
			cycleRecs = cpRecs[cycle]
		print('job %d record start idle non idle at %f' % (self.jobID, self.env.now))
		for i in range(self.computeNodeNumber):
			complete = self.env.event()
			completeQ.append(complete)
			compNode = self.computeNodeProcs[i]
			compNode.updateStartIdleNonIdleTime()
			compNode.setJobID(self.jobID)
			compNode.setNodeLocalID(i)
			compNode.setJobLength(self.jobLen)
			compNode.setOutputRatio(self.chkpRatio)
			compNode.setCheckpointFreq(self.cpFreq)
			compNode.setCompleteEvent(complete)
			compNode.setBarriers(barriers)
			compNode.setChkpRecord(self.cpRecs)
			compNode.setCompNodeNumber(self.computeNodeNumber)

			#if validCp:
				#print('restart')
			#	compNode.createProcess(cycle, cycleRecs, self.stats, self.currentTime)
			#else:
			#	compNode.createProcess(-1, None, self.stats, self.currentTime)
				
	def startComputeNodeProcs(self, restartFlag, cycle):
		if restartFlag == 1 and cycle > -1:
			cycleRecs = self.cpRecs[cycle]
			for i in range(self.computeNodeNumber):
				compNode = self.computeNodeProcs[i]
				compNode.createProcess(cycle, cycleRecs, self.stats, self.currentTime)
		else:
			for i in range(self.computeNodeNumber):
				compNode = self.computeNodeProcs[i]
				compNode.createProcess(-1, None, self.stats, self.currentTime)
