import sys
import math
import random
import time
from statistics import mean
###start###
fileName = "local_config11"
fd = open(fileName, 'r')

eapJobSizes = []
lapJobSizes = []
stonJobSizes = []
vpicJobSizes = []
eapPipes = []
lapPipes = []
stonPipes = []
vpicPipes = []
eapHours = 0
lapHours = 0
stonHours = 0
vpicHours = 0
cnt = 0
line = fd.readline()
while line:
	if "job" in line:
		cnt = cnt + 1
		subline = fd.readline()
		jobType = subline.rstrip()
		subline = fd.readline()
		splits = subline.rstrip().split(':')
		pipeID = int(splits[1])
		fd.readline()
		fd.readline()
		subline = fd.readline()
		splits = subline.rstrip().split(':')
		jobSize = int(splits[1])
		jobHour = jobSize * 24 * 32
		if jobType == "EAP":
			eapHours = eapHours + jobHour
			eapJobSizes.append(jobSize)
			if pipeID not in eapPipes:
				eapPipes.append(pipeID)
		elif jobType == "LAP":
			lapHours = lapHours + jobHour
			lapJobSizes.append(jobSize)
			if pipeID not in lapPipes:
				lapPipes.append(pipeID)
		elif jobType == "STON":
			stonHours = stonHours + jobHour
			stonJobSizes.append(jobSize)
			if pipeID not in stonPipes:
				stonPipes.append(pipeID)
		elif jobType == "VPIC":
			vpicHours = vpicHours + jobHour
			vpicJobSizes.append(jobSize)
			if pipeID not in vpicPipes:
				vpicPipes.append(pipeID)
	line = fd.readline()

print("EAP pipelines %d" % (len(eapPipes)))
print("LAP pipelines %d" % (len(lapPipes)))
print("Silverton Pipelines %d" % (len(stonPipes)))
print("vpic pipelines %d" % (len(vpicPipes)))
print("**********************")
print("eap job mean %f, min %d, max %d" % ((mean(eapJobSizes)), (min(eapJobSizes)), (max(eapJobSizes))))
print("lap job mean %f, min %d, max %d" % ((mean(lapJobSizes)), (min(lapJobSizes)), (max(lapJobSizes))))
print("silverton job mean %f, min %d, max %d" % ((mean(stonJobSizes)), (min(stonJobSizes)), (max(stonJobSizes))))
print("vpic job mean %f, min %d, max %d" % ( (mean(vpicJobSizes)), (min(vpicJobSizes)), (max(vpicJobSizes))) )
print("***********************")
sumhours = eapHours + lapHours + stonHours + vpicHours
print("eap hours %f  lap hours %f  ston hours %f  vpic hours %f" % ((eapHours/sumhours), (lapHours/sumhours), (stonHours/sumhours), (vpicHours/sumhours)))
print("jobcnt %d" % cnt)
