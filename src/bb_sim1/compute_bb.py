import simpy
from simpy import *
import math
import random

class Compute_BB_Node:

	def __init__(self, env, nodeID, memsize, pfsBandwidth, ssdBandwidth, bbWrite, bbRead, parity):
		self.env = env
		self.nodeID = nodeID
		self.nodeStat = -1
		self.localID = -1
		self.memSize = memsize
		self.pfsBandwidth = pfsBandwidth
		self.ssdBandwidth = ssdBandwidth
		self.bbWrite = bbWrite
		self.bbRead = bbRead
		self.totalNonIdle = 0
		self.totalIdle = 0
		self.nonIdleStart = 0
		self.nonIdleEnd = 0
		self.idleStart = 0
		self.idleEnd = 0
		self.parity = parity
		self.run = None

	def setNodeLocalID(self, localID):
		self.localID = localID

	def setJobID(self, jobID):
		self.jobID = jobID

	def setJobLength(self, length):
		self.length = length

	def setOutputRatio(self, ratio):
		self.outputRatio = ratio

	def setBarriers(self, barriers):
		self.sync = barriers

	def setCheckpointFreq(self, freq):
		self.chkpFreq = freq

	def setCompleteEvent(self, complete):
		self.finished = complete

	def setChkpRecord(self, cpRecs):
		self.cpRecs = cpRecs

	def setCompNodeNumber(self, count):
		self.computeNodeNumber = count

	def createProcess(self, cycle, cycleRecs, stats, timeProgress):
		self.run = self.env.process(self.compNode_bb(cycle, cycleRecs, stats, timeProgress))

	def compNode_bb(self, cpCycle, cycleRecs, stats, timeProgress):
		#accumulate idle time
		#self.idleEnd = self.env.now
		#self.totalIdle = self.totalIdle + self.idleEnd - self.idleStart
		#update non-dile time
		#self.nonIdleStart = self.env.now
		if self.parity == 0:
			jobsize = self.memSize * self.outputRatio
		else:
			jobsize = self.memSize * self.outputRatio * 1.2
		totalSize = jobsize * self.computeNodeNumber
		readTime = jobsize / self.bbRead
		retval = 0
		cycle = cpCycle
		if cycle >= 0:
			#print('job %d comptenode %d recovery form cycle %d' % (self.jobID, self.nodeID, cycle))
			#recovery routine, due to simpy interrupt constrains, this has to be put here without a function
			####Start Recovery########
			if self.localID == 0:
				print('comp %d started recovering for job %d' % (self.nodeID, self.nodeStat))
			self.sync[self.localID] = (cycle, self.env.event(), self.env.event())
			time0 = self.env.now
			try:
				yield self.env.timeout(readTime)
				self.sync[self.localID][1].succeed()
				if self.localID == 0:
					length = len(self.sync)
					for i in range(length):
						if i != self.localID:
							yield self.sync[i][1]

					for i in range(length):
						self.sync[i][2].succeed()
				else:
					yield self.sync[self.localID][2]
				#now wait for all other compute node to finish
				#for i in range(len(self.sync)):
				#	if i != self.localID:
				#		if self.sync[i][0] == cycle:
				#			yield self.sync[i][1]
				retval = 1
			except simpy.Interrupt as i:
				#interrupted, some node failed
				retval = -2
			time1 = self.env.now
			if self.localID == 0:
				#stats.recordPFSIOTime(time1 - time0)
				stats.recordIOTime(cycle, time1 - time0)
				stats.recordReadTime(time1 - time0)

		if retval == -2:
			print('some node failed or ran out of time, now quit')	
			#self.finished.succeed()
			pass
		else:
			if retval == 1:
				#successfully recovered, go on to next cycle
				cycle = cycle + 1
			else:
				cycle = 0
				#need to read in datat first
				#time1 = self.env.now
				#try:
				#	yield self.env.timeout(readTime)
				#except simpy.Interrupt as i:
				#	time2 = self.env.now
				#	if self.localID == 0:
				#		stats.recordPFSIOTime(time2 - time1)
				#	self.finished.succeed()
				#	self.nonIdleEnd = self.env.now
				#	self.totalNonIdle = self.totalNonIdle + self.nonIdleEnd - self.nonIdleStart
				#	self.idleStart = self.env.now
					#must exit due to fault somehwere
				#	return
				#time2 = self.env.now
				#if self.localID == 0:
					#add time progress of the job
				#	stats.recordPFSIOTime(time2 - time1)

			#print('cn %d starting cycle is %d' % (self.nodeID, cycle))
			elapsed = 0
			state = 0
			startT = self.env.now
			freq = self.chkpFreq

			while elapsed <= self.length:
				if state == 0:
					time1 = self.env.now
					try:
						#if self.localID == 0:
						#	print('job %d cn %d start cmpute at %f' % (self.jobID, self.nodeID, self.env.now))
						yield self.env.timeout(freq)
						time2 = self.env.now
						if self.localID == 0:
							print('finished compute')
							stats.recordComputeTime(cycle, time2 - time1)
						#if freq != self.chkpFreq:
						#	break
						state = 1
					except simpy.Interrupt as i:
						time2 = self.env.now
						if self.localID == 0:
							stats.recordComputeTime(cycle, time2 - time1)
						#print('cn %d interrupted during compute' % self.nodeID)
						msg = i.cause
						break

				elif state == 1:
					time3 = self.env.now
					self.sync[self.localID] = (cycle, self.env.event(), self.env.event())
					try:
						#if self.localID == 0:
						#	print('job %d cn %d started checkpoing at %f' % (self.jobID, self.nodeID, self.env.now))
						yield self.env.timeout(jobsize / self.bbWrite)
						self.sync[self.localID][1].succeed()
						if self.localID == 0:
							length = len(self.sync)
							for i in range(length):
								if i != self.localID:
									if(self.sync[i][0] == cycle):
										yield self.sync[i][1]
							for i in range(length):
								self.sync[i][2].succeed()
						else:
							yield self.sync[self.localID][2]

						#for i in range(len(self.sync)):
						#	if i != self.localID:
						#		if(self.sync[i][0] == cycle):
						#			yield self.sync[i][1]
						time4 = self.env.now
						if self.localID == 0:
							bandwidth = jobsize * self.computeNodeNumber / (time4 - time3)
							#print('cp bandwidht %f, jobsize %f, node count %d, time %f' % (bandwidth, jobsize, self.computeNodeNumber, (time4-time3)))
							print('finished cp at %f cycle %d cp duration %f' % (self.env.now, cycle, time4 - time3))
							stats.recordCpBandwidth(bandwidth)
							stats.recordIOTime(cycle, time4 - time3)
							stats.recordWriteTime(time4 - time3)
						#at this point we know all compute nodes have finished writing to local SSD
						self.saveChkpRecord(cycle, self.env.now, time4 - time3, time4 - time1)
						cycle = cycle + 1
						#elapsed = self.env.now - startT
						#if self.length - elapsed < self.chkpFreq:
						#	freq = self.length - elapsed
						state = 0
					except simpy.Interrupt as i:
						time4 = self.env.now
						if self.localID == 0:
							stats.recordIOTime(cycle, time4 - time3)
							stats.recordWriteTime(time4 - time3)
						#print('cn %d interrupted during cp' % self.nodeID)
						msg = i.cause
						break

		self.finished.succeed()
		#self.nonIdleEnd = self.env.now
		#self.totalNonIdle = self.totalNonIdle + self.nonIdleEnd - self.nonIdleStart
		#self.idleStart = self.env.now

	def updateStartIdleNonIdleTime(self):
		self.idleEnd = self.env.now
		self.totalIdle = self.totalIdle + self.idleEnd - self.idleStart
		self.nonIdleStart = self.env.now

	def updateEndIdleNonIdleTime(self):
		self.nonIdleEnd = self.env.now
		self.totalNonIdle = self.totalNonIdle + self.nonIdleEnd - self.nonIdleStart
		self.idleStart = self.env.now

	def saveChkpRecord(self, cycle, time, duration, cycleDuration):
		#print('writing cycle %d' % cycle)
		if cycle in self.cpRecs:
			#some other compute node has created the key for this cycle
			thisRec = self.cpRecs[cycle]
			thisRec[self.localID] = (time, duration, cycleDuration)
		else:
			thisRec = dict()
			thisRec[self.localID] = (time, duration, cycleDuration)
			self.cpRecs[cycle] = thisRec
