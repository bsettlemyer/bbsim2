from simpy import *
import simpy
import math
import random
import os

#implements a poisson fault generator
#uses simpy interrupts to asychronously inform affected nodes that there is a fault
class PoissonFaultGenerator:

	def __init__(self, env, mode, scheduler, faultRate, jobs, computeNodeCnt, computeNodes, numBBNodes, bbNodes, recoveryTime, currentAvailable, fd_fault):
		self.env = env
		self.mode = mode
		self.faultRate = faultRate #assume fault rate is in hour
		self.jobs = jobs
		self.computeNodeCnt = computeNodeCnt
		self.computeNodes = computeNodes
		#self.computeNodeStats = computeNodeStats
		self.numBBNodes = numBBNodes
		self.bbNodes = bbNodes
		self.scheduler = scheduler
		self.start = None#env.process(self.run_concentratedBBs()) 
		self.nodeRecoverTime = recoveryTime
		self.fd = fd_fault
		self.currentAvailable = currentAvailable

	def start_fault(self):
		print('fault mode %d' % self.mode)
		if self.mode == 0:
			self.start = self.env.process(self.run_localBBs())
		else:
			print('test')
			self.start = self.env.process(self.run_concentratedBBs())

	def run_localBBs(self):
		done = 0
		while done == 0:
			nextFault = random.expovariate(1/self.faultRate)
			try:	
				yield self.env.timeout(nextFault)
				print("#####FAULT AT %f#####" % self.env.now)
				self.fd.write('%f\n' % self.env.now)
				victim = self.pickVictim_localBBs()
				self.notifyComputeNode(victim)
			except simpy.Interrupt as i:
				print('##FAULT GENRATOR INTERUPPTED###')
				done = 1

		print('###FAULT GENERATOR TERMINATED###')
			

	def run_concentratedBBs(self):
		print('concentrated BB fault')
		#victim = (0 , 0)
		#yield self.env.timeout(nextFault)
		#print("### HERE IS FAULT AT %f #######" % nextFault)
		#self.notifyComputeNode(victim[1])
		
		#yield self.env.timeout(nextFault)
		#print("### HERE IS FAULT AT %f #######" % self.env.now)
		#victim = (1, 0)
		#for y in self.notifyBBNode(victim[1]):
		#	yield y
		counter = [0]

		#yield self.env.timeout(204)
		#print('#### HERE IS FAULT AT %f ####' % self.env.now)
		#victim = (0, 1)
		#self.notifyComputeNode(victim[1])	
		done = 0
		while done == 0:
			nextFault = random.expovariate(1/self.faultRate)
			#self.fd.write('&********NEXT FAULT ARRIVING IN %f**********, fault rate is %f\n' % (nextFault, self.faultRate))
			try:
				yield self.env.timeout(nextFault)
				print("####FAULT AT %f####" % self.env.now)
				self.fd.write('%f\n' % self.env.now)
				victim = self.pickVictimNode()
				victim = (0, 0)
				print('victim roll ')
				print(victim)
				if victim[0] == 0 and (counter[0] < self.computeNodeCnt + self.numBBNodes):
					#a compute node
					self.notifyComputeNode(victim[1])
					#for x in self.notifyComputeNode(victim[1]):
					#	yield x
				elif victim[0] == 1 and (counter[0] < self.computeNodeCnt + self.numBBNodes):
					#a bb node
					for y in self.notifyBBNode(victim[1]):
						yield y
				else:
					#all nodes are faulty
					print('all nodes are faulty')
			except simpy.Interrupt as i:
				print('###FAULT GENERATOR INTERUPPTED###')
				done = 1
		print('###FAULT GENERATOR TERMINATING###')
	def pickVictim_localBBs(self):
		#assume that during the entire duration of the simulation,
		#only a subset of nodes will become faulty
		done = 0
		roll = None
		count = 0
		while done == 0:
			roll = random.randrange(0, self.computeNodeCnt, 1)
			computeNode = self.computeNodes[roll]
			if computeNode.nodeStat != -2:
				retval = roll
				done = 1
		return retval

	def pickVictimNode(self):
		done = 0
		roll = None
		bbID = None
		retval = None
		while done == 0:
			roll = random.randrange(0, self.computeNodeCnt + self.numBBNodes, 1)
			if roll < self.computeNodeCnt:
				computeNode = self.computeNodes[roll]
				if computeNode.nodeStat != -2:
					retval = (0,roll)
					done = 1
			else:
				bbID = roll - self.computeNodeCnt
				bbNode = self.bbNodes[bbID]
				if bbNode.status == 0:
					done = 1
					retval = (1,bbID)
		return retval

	def notifyComputeNode(self, index):
		jobID = self.computeNodes[index].nodeStat
		print('job id for bad compute node %d' % jobID)
		print('current available node before fault %d' % self.currentAvailable[0])
		if jobID != -1 and jobID != -2:
			#this node has a job running
			#change node status
			self.computeNodes[index].nodeStat = -2
			print('compute node %d in fault generator status after chagne is %d' %(self.computeNodes[index].nodeID, self.computeNodes[index].nodeStat))
			print('victim compute node is %d belong to job %d' % (index, jobID))
			#job = self.jobs[jobID]
			job = PoissonFaultGenerator.getJob(self.jobs, jobID)
			job.run.interrupt(index) #need to tell job which node is wrong
			#yield self.env.timeout(0.0000000001)	
			#at the same time tell scheduler that the job needs to be restarted
			self.scheduler.interrupt(jobID)
			#self.fd.write("interrupted job %d" % jobID)
			#DO NOT NEED TO TAKE 1 AVAILABLE NODE OFF BECAUSE WHEN THE JOB RELEASES THE NODES,
			#IT WILL RELEASE ONE FEWER.
		elif jobID == -1:
			#this node is available but there is no job running, just mark it as unavailable
			print('victim node %d does not have a job on it' % index)
			self.computeNodes[index].nodeStat = -2
			self.currentAvailable[0] = self.currentAvailable[0] - 1
			#self.fd.write("faulty node does not have a job on it")
		
		print('after fault, current available nodes %d' % self.currentAvailable[0])
		self.env.process(self.recoverBadCompNode(index))

	@staticmethod
	def getJob(jobs, jobID):
		job = None
		for i in range(len(jobs)):
			job = jobs[i]
			if job.jobID == jobID:
				break
		return job

	def notifyBBNode(self, index):
		print('here')
		bbNode = self.bbNodes[index]
		print('victim bb node is %d' % index)
		if bbNode.status == 0:
			#this bb node is available
			print('victim bb node is available, marked as unavailable')
			#bbNode.start.interrupt(-1)
			msg = (-1, self.env.event(), 0, -1)
			yield bbNode.reqQ.put(msg)
			#self.fd.write("fault BB %d" % index)
			#os.fsync(self.fd)
			self.env.process(self.recoverBadBB(index))

	def recoverBadBB(self, bbID):
		yield self.env.timeout(self.nodeRecoverTime)
		print('bb %d recovered at %f' % (bbID, self.env.now))
		self.bbNodes[bbID].status = 0

	def recoverBadCompNode(self, nodeID):
		yield self.env.timeout(self.nodeRecoverTime)
		self.computeNodes[nodeID].nodeStat = -1
		self.currentAvailable[0] = self.currentAvailable[0] + 1
		print('compute node %d is recovered at %f, current available nodes %d' % (nodeID, self.env.now, self.currentAvailable[0]))
