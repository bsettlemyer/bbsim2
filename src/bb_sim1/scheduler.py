from bb_node import *
from compute_node import *
from job_obj import *
from job_bb_obj import *
from compute_bb import *
from queue import *
from simpy import *

class Scheduler:

	def __init__(self, env, mode, computeNodes, bbNodes, jobs, jobdoneQ, fd_start, recoveryTime, scheduler, count, pfsReqQ):
		self.env = env
		self.mode = mode
		self.schedulePolicy = 0 #hardcoded for now
		self.computeNodes = computeNodes
		#self.computeNodeStats = computeNodeStats
		self.bbNodes = bbNodes
		self.jobs = jobs
		self.pfsReqQ = pfsReqQ
		self.jobDoneQ = jobdoneQ
		self.jobTimers = dict()
		self.fd_start = fd_start
		self.recoveryTime = recoveryTime
		self.schedulePolicy = scheduler
		self.currentAvailable = count
		self.eap_pipes = dict()
		self.lap_pipes = dict()
		self.ston_pipes = dict()
		self.vpic_pipes = dict()
		self.faultGen = None
		self.interruptions = 0
		self.isInterrupted = False
		self.interruptMsg = []
		self.run = None

	def addFaultGen(self, faultGen):
		self.faultGen = faultGen

	def startScheduler(self):
		print('in scheduler')
		#first sort jobs based on time to schedue
		if self.schedulePolicy == 0:
			q = Queue()
			Scheduler.sortJobOnScheduleTime(self.jobs, 0, q, self.env.now)
			self.run = self.env.process(self.scheduleFCFS(q))
		elif self.schedulePolicy == 1:
			print('scheduling policy is first fit if fail')
			q = []
			Scheduler.copyJobQueue(q, self.jobs)
			self.run = self.env.process(self.scheduleBackToQ(q))

	def checkPipelineJob(self, job):
		retval = 0
		if job.jobType == "EAP":
			if job.pipeID in self.eap_pipes.keys():
				retval = 1
		elif job.jobType == "LAP":
			if job.pipeID in self.lap_pipes.keys():
				retval = 1
		elif job.jobType == "STON":
			if job.pipeID in self.ston_pipes.keys():
				retval = 1
		elif job.jobType == "VPIC":
			if job.pipeID in self.vpic_pipes.keys():
				retval = 1
		return retval
		
	def recordPipeID(self, job):
		if job.jobType == "EAP":
			self.eap_pipes[job.pipeID] = 1
		elif job.jobType == "LAP":
			self.lap_pipes[job.pipeID] = 1
		elif job.jobType == "STON":
			self.ston_pipes[job.pipeID] = 1
		elif job.jobType == "VPIC":
			self.vpic_pipes[job.pipeID] = 1

	def removePipeID(self, job):
		if job.jobType == "EAP":
			del self.eap_pipes[job.pipeID]
		elif job.jobType == "LAP":
			del self.lap_pipes[job.pipeID]
		elif job.jobType == "STON":
			del self.ston_pipes[job.pipeID]
		elif job.jobType == "VPIC":
			del self.vpic_pipes[job.pipeID]

	#implements a back to q scheduling scheme
	def scheduleBackToQ(self, q):
		doneEvents = []
		completeJobs = []
		done = 0
		head = 0
		while done == 0:
			#ALWAYS TRY TO SCHEDULE THE HEAD OF THE Q
			job = q[head]
			#first check if there is the already a job in the pipeline being scheduled
			check = self.checkPipelineJob(job)
			retval = 1
			#only try to allocate current job at the head if there is no job running in the same pipeline
			if check == 0:
				#try to allocate the job after wati period
				print('this job is the only one in pipeline! trying to schedule job %d at %f' % (job.jobID, self.env.now))
				retval = self.scheduleToRun(self.computeNodes, job, doneEvents)
			if retval != 0:
				print('job %d cannot be scheduled at %f' % (job.jobID, self.env.now))
				cursor = head + 1
				ret = 1
				while ret != 0 and cursor < len(q):
					#this job failed to be scheduled to run, look for first job that can be scheduled
					nextJob = q[cursor]
					check = self.checkPipelineJob(nextJob)
					ret = 1
					if check == 0:
						print('next job pipeline id check passsed')
						print('trying to scheuld next job in line %d at %f' % (nextJob.jobID, self.env.now))
						print('current avaiblable nodes %d, required nodes %d' % (self.currentAvailable[0], nextJob.computeNodeNumber))
						ret = self.scheduleToRun(self.computeNodes, nextJob, doneEvents)
					cursor = cursor + 1
				if ret == 0:
					#remove from q
					print('job %d in the queue scheduled at %f' % (nextJob.jobID, self.env.now))
					print('JOB %d PIPELINE ID added' % nextJob.jobID)
					self.recordPipeID(nextJob)
					del q[cursor - 1]
				else:
					#no job can be scheduled. we must wait for some job to finish
					try:
						print('scheduler waiting for some job to finish to get more free nodes')
						for x in Scheduler.waitForSomeJob(self.env, doneEvents, self.recoveryTime):
							yield x
						print('scheduler finds that some job is done at %f!!!' % self.env.now)
					except simpy.Interrupt as i:
						print('scheduler interrupted at %f' % self.env.now)
						msg = i.cause
						self.interruptMsg.append(msg)
						self.interruptions = self.interruptions + 1
						#for x in self.interruptHandler(msg, doneEvents):
						#	yield x
					while self.interruptions > 0:
						print('2#@#$#@ im here234234')
						#self.interruptions = self.interruptions - 1
						currentMsg = self.interruptMsg[0]
						try:
							print('scheduler processing interrupts')
							for x in self.interruptHandler(currentMsg, doneEvents):
								yield x
							del self.interruptMsg[0]
							self.interruptions = self.interruptions - 1
						except simpy.Interrupt as i:
							print('scheduler interrupted again when processing an interrupt at %f' % self.env.now)
							msg = i.cause
							self.interruptMsg.append(msg)
							self.interruptions = self.interruptions + 1
					print('scheduler done for this loop at %f' % self.env.now)
			else:
				#job at the head of the q scheduled successfully, move it from the queue
				print('job %d size %d at the head of the queue scheuled at %f!' % (job.jobID, job.computeNodeNumber, self.env.now))
				#put the pipeline id in the records
				print('JOB %d PIPELINE ID ADDED' % job.jobID)
				self.recordPipeID(job)
				del q[head]
				if len(q) == 0:
					print('all job scheuled at %f' % self.env.now)
					done = 1
		done = 0
		while done == 0:
			try:
				yield AllOf(self.env, doneEvents)
				done = 1
			except simpy.Interrupt as i:
				msg = i.cause
				print('scheduler interrupted at %f' % self.env.now)
				for x in self.interruptHandler(msg, doneEvents):
					yield x
		print('all job finished')
		self.faultGen.interrupt()
		for i in range(len(self.computeNodes)):
			self.computeNodes[i].idleEnd = self.env.now
			self.computeNodes[i].totalIdle = self.computeNodes[i].idleEnd - self.computeNodes[i].idleStart + self.computeNodes[i].totalIdle
		for i in range(len(self.jobs)):
			print('job total fault %d; checkpoing freq %f; job stat total time: %f; total wasted time :%f; total IO Time: %f ' % (self.jobs[i].stats.totalFaults, self.jobs[i].cpFreq, self.jobs[i].stats.totalComputeTime, self.jobs[i].stats.wastedComputeTime, self.jobs[i].stats.totalIOTime))

	def interruptHandler(self,  msg, doneEvents):
		jobID = None
		mode = 0
		nowTime = self.env.now
		if type(msg) is int:
			jobID = msg
		elif type(msg) is tuple:
			jobID = msg[0]
			mode = msg[1]
		#Must restart the current job
		print('in scheduler interrupt handler, interrupted job id is %d' % jobID)
		job = None
		for i in range(len(self.jobs)):
			if self.jobs[i].jobID == jobID:
				job = self.jobs[i]
				break
		#wait for job done event to get triggered
		yield job.finish
		#yield self.env.timeout(0.01)
		#interrupt job timer
		jobTimerInfo = self.jobTimers[job.jobID]
		jobTimer = jobTimerInfo[0]
		endTime = self.env.now
		if jobTimer.is_alive:
			jobTimer.interrupt(1)

		yield self.env.timeout(0.0001) 
		jobTimerStart = jobTimerInfo[1]
		#Now restart the job with a different set of node
		print('trying to allocate new nodes now available nodes %d, required nodes %d' % (self.currentAvailable[0], job.computeNodeNumber))
		retval = Scheduler.allocateNodes(job, self.computeNodes)
		while retval != 0:
			### Instead of waiting for some job to finish, we wait for a short period of time to see if there
			### are enough nodes because we have node recovery mechanism now
			yield self.env.timeout(self.recoveryTime + 1)
			retval = Scheduler.allocateNodes(job, self.computeNodes)
		print("restart job schedueld to run at %f" % self.env.now)
		self.currentAvailable[0] = self.currentAvailable[0] - job.computeNodeNumber
		self.recordPipeID(job)
		print('after restart, available nodes %d' % self.currentAvailable[0])
		#now re-start the job
		job.finish = self.env.event()#need to reset the jobFinish event because it was set during the job interrupt
		doneEvents.append(job.finish)
		print('about to restart job %d' % job.jobID)
		#set new job timer before restarting job
		#remainTime = job.jobLen - job.currentTime[0]
		totalValidTime = job.getRestartTime(mode)
		print('total valid time %f' % totalValidTime)
		if totalValidTime >= 0:
			remainTime = job.jobLen - totalValidTime
		else:
			remainTime = job.jobLen
		print('remainTime %f' % remainTime)
		#job.jobLen = job.jobLen + nowTime - lastCPTime # compensate for wasted cycle time
		#if lastCPTime >= 0:
		#	job.jobLen = job.jobLen + nowTime - lastCPTime
		#	print('new joblen %f' % job.jobLen)
		#	remainTime = job.jobLen - job.currentTime[0] + (nowTime - lastCPTime)
		#elif lastCPTime == -1:
		#	job.jobLen = job.jobLen + job.currentTime[0]
		#	remainTime = job.jobLen
		newTimer = self.env.process(self.endJobTimer(job, remainTime))
		self.jobTimers[job.jobID] = (newTimer, self.env.now)
		#record pipeline id
		job.stats.recordStartTime(self.env.now)
		job.begin_job(1)


	@staticmethod
	def waitForSomeJob(env, doneEvents, recoveryTime):
		print('cant allocate the current job, needs to wait')
		retval = 1
		length = len(doneEvents)
		print('doneevents length %d' % length)
		if length == 0:
			print('no jobs running, wait for recovery time')
			yield env.timeout(recoveryTime)
			return
		yield AnyOf(env, doneEvents)
		print('some job finished at %f' % env.now)
		for i in range(length):
			event = doneEvents[i]
			if event.triggered == True:
				doneEvents.pop(i)
				break


			
	@staticmethod
	def sortJobOnScheduleTime(jobs, mode, q, time):#simple selection sort, small size of job so should be fine
		length = len(jobs)
		for i in range(length):
			current = i
			for j in range(i+1, length):
				if(jobs[j].timeToSchedule <= jobs[current].timeToSchedule):
					current = j
			if(jobs[current].timeToSchedule <= jobs[i].timeToSchedule):
				temp = jobs[current]
				jobs[current] = jobs[i]
				jobs[i] = temp
				if mode == 0:
					q.put(jobs[i])
				elif mode == 1:
					q.append(jobs[i])
				jobs[i].stats.recordEnQTime(time)
	
	def scheduleToRun(self, computeNodes, job, doneEvents):
		# pre-check if there is enough nodes, return early it not
		if self.currentAvailable[0] < job.computeNodeNumber:
			print('required node %d, current available nodes %d' % (job.computeNodeNumber, self.currentAvailable[0]))
			retval = 1
			return retval
		#allocate compute nodes to job first
		retval = Scheduler.allocateNodes(job, computeNodes)
		#while retval != 0:
			#wait for 30 seconds to see if any node comes up
			
		#	retval = Scheduler.allocateNodes(job, computeNodes)

		if retval == 0:
			print('job id %d scheduled at %f' % (job.jobID, self.env.now))
			#start a job timer
			timer = self.env.process(self.endJobTimer(job, job.jobLen - job.currentTime[0]))
			#add it for book keeping
			startTime = self.env.now
			self.jobTimers[job.jobID] = (timer, startTime)
			job.stats.recordStartTime(startTime)
			doneEvents.append(job.finish)
			job.begin_job(0)
			self.currentAvailable[0] = self.currentAvailable[0] - job.computeNodeNumber
			print('available node after allocating job %d' % self.currentAvailable[0])
			#record job start time
			self.fd_start.write("%d %d %f %f %f %f\n" % (job.jobID, job.computeNodeNumber, job.jobLen, job.timeToSchedule, startTime, job.stats.enqTime))	
			
		return retval

	def endJobTimer(self, job, remain):
		print('job end timer remain %f' % remain)
		try:
			yield self.env.timeout(remain + 0.001)
			if job.finish.triggered != True and job.run.is_alive:
				#job is running out of time, must terminate
				job.run.interrupt(-1)
			#need to remove the pipeID from records
			print('in regular tryJOB %d pipeline record removed' % job.jobID)
			self.removePipeID(job)
		except simpy.Interrupt as i:
			print('In interrupt JOB %d pipeline record removed' % job.jobID)
			#self.removePipeID(job)
			return
			
	@staticmethod
	def copyJobQueue(q, jobs):
		length = len(jobs)
		for i in range(0, length):
			job = jobs[i]
			q.append(job)

	@staticmethod
	def allocateNodes(job, computeNodes):
		retVal = 1
		allocated = 0
		cursor = 0
		length = len(computeNodes)

		indices = []

		while allocated != job.computeNodeNumber and cursor < length:
			#find nodes that are available and save their indices
			#if computeNodeStats[cursor] == -1:
			#print('node %d stat %d' % (computeNodes[cursor].nodeID, computeNodes[cursor].nodeStat))
			if computeNodes[cursor].nodeStat == -1:
				indices.append(cursor)
				allocated = allocated + 1
			cursor = cursor + 1

		if allocated == job.computeNodeNumber:
			#there are enough nodes
			retVal = 0
			for i in range(allocated):
				#job.computeNodeIndices.append(indices[i])
				computeNode = computeNodes[indices[i]]
				job.computeNodeProcs.append(computeNode)
				computeNode.nodeStat = job.jobID

		return retVal

		
