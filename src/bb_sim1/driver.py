import sys
import random
import datetime
import simpy
from sim_obj import *

#### Read in input parameters ####
random.seed(1) ## now we set the seed for reproductibility
time1 = datetime.datetime.now()
inputFileName = sys.argv[1]
env = simpy.Environment()
simobj = SimObj(env, inputFileName)
simobj.start_simulation()
time2 = datetime.datetime.now()
print("Simulation started at %s" % time1)
print("Simulation ended at %s" % time2) 

