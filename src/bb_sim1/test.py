import simpy

def cn(env, eventList, myid, bandwidth):
	bw = bandwidth
	cnid = myid
	for i in range(2):
		print('cn %d computing at time %d' % (cnid, env.now))
		yield env.timeout(5)
		print('cn %d doing checkpoint io at time %d' % (cnid, env.now))	
		wakeup = env.event()
		yield eventList[0].put(wakeup)
		yield wakeup
		print('cn %d finished checkpointing at time %d' %(cnid, env.now))

### burst buffer node model ###
def bb(env, myid, bandwidth, requestQ, jobsize):
	sharecnt = 0 #this variable counts the number of compute nodes currently using this bb node
	reqcounter = 0
	bbid = myid
	bw = bandwidth
	iorequests = dict() #map a request id to a request execution process
	reqstatus = dict() #map a request id to a request execution status: 0 = not done; 1 = done
	while True:
		event = yield requestQ.get()
		reqid = reqcounter
		iorequests[reqid] = iorequest
		reqstatus[reqid] = 0 #mark request as incomplete
		
		#tell other requests executioner that here is a change in the degree of sharing
		keys = reqstatus.keys()
		for k in keys:
			if k != reqid:
				#need to interrupt this io request executioner
				stat = reqstatus.get(k)
				if stat == 0:
					#io req has not done yet, need to be interrupted
					iorequests[reqid].interrupt('inc')
		iorequest = env.process(bbexe(env, bbid, event, reqid, bw, jobsize, sharecnt, iorequests, reqstatus))
		#need to tell other current processes there is a change in sharing
		
### bbexe - request executioner process ###
def bbexe(env, myid, event, reqid, bandwidth, jobsize, sharecnt, iorequests, reqstatus):
	currentshare = sharecnt
	bbid = myid
	bworig = bandwidth #full bandwidth of this bb node
	currentbw = bandwidth / currentshare #current bandwidth of this bb node(when more than 1 compute node is using it)
	timestart = env.now #start time of the IO
	waittime = jobsize / currentbw #calculate current IO time 
	timewaited = 0
	done = 0
	print('burst buffer node %d got checkpoint requeset at time %d' % (bbid, env.now))
	while done == 0:
		try:
			yield env.timeout(waittime)
			done = 1
		except simpy.Interrupt as i:
			chgmode = i.cause #either increment or decrement message
			currenttime = env.now
			if chgmode == 'inc' and done == 0: #increment share count and only need to recalculate remaining waittime if done== 0
				sharecnt = sharecnt + 1
				#calculate remaining size 
				waittime = get_new_waittime(jobsize, sharecnt, bworig, currentbw, timestart, currenttime)
			elif chgmode == 'dec' and done == 0: #decrement share count
				sharecnt = sharecnt - 1
				waittime = get_new_waittime(jobsize, sharecnt, bworig, currentbw, timestart, currenttime)
			timestart = currenttime #update reference start time

	reqstatus[reqid] = 1
	event.succeed()

### section of auxillary functions ###
def get_new_waittime(jobsize, sharecnt, bworig, currentbw, timestart, currenttime):
	remain = jobsize - ((timestart - currenttime) * currentbw) #calculate remaining jobsize
	waittime = remain / (bandwidth / sharecnt) #calculate remaining waittime
	return waittime

numcn = 2
numBBnodes = 10
env = simpy.Environment()
eventList = []
bbnodes = []
cnodes = []
for i in range(numBBnodes):
	reqQ = simpy.Store(env)
	eventList.append(reqQ)
	#bbnodes.append(env.process(bb(env, i, reqQ)))
	env.process(bb(env, i, reqQ))
for i in range(numcn):
	cnodes.append(env.process(cn(env, eventList, i)))

env.run(until=100)
