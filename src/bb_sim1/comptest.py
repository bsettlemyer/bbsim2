from simpy import *
def compnode(env, jid, myid, cdur, eventlist, barriers):

        duration = 50
        cpfreq = 20
        numBBuse = 2
        jobisze = 16
        startT = env.now
        elapsed = 0
        remain = duration
        jobIter = 0
        while jobIter == 0:
                print('job iter %d' % jobIter)
                print('cn %d compute at %f' %(myid, env.now))
                yield env.timeout(cpfreq)

                print('cn %d checkpoint at %f' %(myid, env.now))
                wakeups = []
                yields = []
                for i in range(numBBuse):
                        wakeup = env.event()
                        wakeups.append(wakeup)
                        print('job %d cnode %d sending to %d' %(jid, myid, i + jid + myid))
                        yields.append(eventlist[i+jid+ myid].put(wakeup))

                yield AllOf(env, yields)
                yield AllOf(env, wakeups)
                print('job %d node %d is done writing at %f' % (jid, myid, env.now))
                #tell others that my node is done writing to bb
                barriers[myid].succeed()
                #now wait for others to finish
                for i in range(len(barriers)):
                        if i != myid:
                                yield barriers[i]

                print('job %d cn %d knows all other nodes completed at time %f' % (jid, myid, env.now))
                #now reset barrier event for next use
                barriers[myid] = env.event()
                jobIter = jobIter + 1

                #if env.now - startT + cpfreq > duration:
                 #       cpfreq = env.now - startT + cpfreq - duration #this makes sure that the job will only run within duration
