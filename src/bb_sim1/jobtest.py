import simpy
from compute_node import *
class Job:
	def __init__(self, env, eventlist, myid):
		self.jobid = myid
		self.env = env
		self.nodeCount = 2
		self.duration = 50
		self.scheduleT = 3+ myid*1
		self.cpfreq = 20
		self.bbcnt = 2
		self.events = eventlist
		self.nodes = []
		self.nodememsize = 16
		self.ratio = 0.8
		self.run = env.process(self.start_job())

	def start_job(self):
		yield self.env.timeout(self.scheduleT) #schedule time
		print('job %d  starting at time %f' %(self.jobid, self.env.now))
		done = 0
		barriers = []
		#now allocate compute nodes for simulation
		for i in range(self.nodeCount):
			barrier = self.env.event()
			barriers.append(barrier)

		for i in range(self.nodeCount):
			computeNode = ComputeNode( self.env, self.jobid, i, self.nodememsize, self.ratio, self.duration, self.cpfreq, self.bbcnt, self.events, barriers )
			self.nodes.append(computeNode)
