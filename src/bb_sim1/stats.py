class Stats:
	def __init__(self):
		self.totalTimeSpent = 0
		self.totalStageInTime = 0
		self.totalStageOutTime = 0
		self.totalComputeTime = 0
		self.totalIOTime = 0
		self.totalReadTime = 0	
		self.totalWriteTime = 0
		self.totalPFSIOTime = 0
		self.wastedComputeTime = 0
		self.effectiveComputeTime = 0
		self.totalFaults = 0
		self.computeTimeRecs = dict()
		self.IOTimeRecs = dict()
		self.startTime = []
		self.finishTime = []
		self.cpBandwidth = []
		self.stageInBandwidth = []
		self.stageOutBandwidth = []
		self.enqTime = -1

	def recordStageInTime(self, duration):
		self.totalStageInTime = self.totalStageInTime + duration

	def recordStageOutTime(self, duration):
		self.totalStageOutTime = self.totalStageOutTime + duration

	def recordComputeTime(self, cycle, duration):
		self.totalComputeTime = self.totalComputeTime + duration
		if cycle in self.computeTimeRecs:
			#we need to record wasted time
			oldRec = self.computeTimeRecs[cycle]
			self.wastedComputeTime = self.wastedComputeTime + oldRec

		self.computeTimeRecs[cycle] = duration

	def recordStageInBandwidth(self, bandwidth):
		self.stageInBandwidth.append(bandwidth)

	def recordStageOutBandwidth(self, bandwidth):
		self.stageOutBandwidth.append(bandwidth)

	def recordReadTime(self, duration):
		self.totalReadTime = self.totalReadTime + duration

	def recordWriteTime(self, duration):
		self.totalWriteTime = self.totalWriteTime + duration

	def recordPFSIOTime(self, duration):
		self.totalPFSIOTime = self.totalPFSIOTime + duration

	def recordCpBandwidth(self, bandwidth):
		self.cpBandwidth.append(bandwidth)

	def recordIOTime(self, cycle, duration):
		self.totalIOTime = self.totalIOTime + duration

	def recordFault(self):
		self.totalFaults = self.totalFaults + 1
	
	def recordStartTime(self, startT):
		self.startTime.append(startT)
	
	def recordFinishTime(self, finishT):
		self.finishTime.append(finishT)

	def recordEnQTime(self, qT):
		self.enqTime = qT
