import math
import random
from simpy import *
from job_obj import *
from job_bb_obj import *
from bb_node import *
from compute_node import *
from compute_bb import *
from scheduler import *
from fault_generator import *
from pfs import *

class SimObj:
#this class describes attributes read from an input file
	
	#initialize simulation object
	def __init__(self, env, inputFileName):
		self.env = env
		self.inputFileName =inputFileName
		self.fileName = ''
		self.numberOfJobs = 0
		self.faultRate = 0.0
		self.burstBufferNodeNumber = 0
		self.burstBufferNodeBandwidth = 0.0
		self.burstBufferAggregBw = 0
		self.burstBufferNodeCapacity = 0.0
		self.bbTotalWrite = 0.0
		self.bbTotalRead = 0.0
		self.bbWrite = 0.0
		self.bbRead = 0.0
		self.bbAggWrite = 0.0
		self.bbAggRead = 0.0
		self.bbWrite = 0.0
		self.bbRead = 0.0
		self.burstBuffers = []
		self.totalComputeNode = 0
		self.computeNodeMem = 0
		self.computeNodeBandwidth = 0
		self.nodeRecoveryTime = -1
		self.parity = -1 # 0 - parity off; 1 - parity on
		self.bbAssignment = -1 # 0 - random; 1 - random round robin
		self.scheduling = 1 # 0 - FCFS; 1 - Back to Q
		#self.computeNodeStats = []
		self.pfs = None
		self.computeNodes = []
		self.reqQs = []
		self.jobs = []
		self.mode = -1
		self.output_prefix = None
	def start_simulation(self):
		#fd_log_start = open("job_log_start.txt", "w")
		#fd_log_finish = open("job_log_finish.txt", "w")
		#fd_log_bb = open("job_log_bb.txt", "w")
		#fd_cp = open("sim_cp_bw.txt", "w")
		#fd_log_start.write("#jobid   compute_node_count   user_requested_time   arrival_time   start_time   enqueue_time\n")
		#fd_log_finish.write("#jobid   compute_node_count   user_requested_time   arrival_time   start_time   enqueue_time   finish_time\n")
		#fd_log_bb.write("#bbid   reqs   time\n")
		#fd_cp.write("cpID    bandwidth \n")
		jobdoneQ = []

		fd = open(self.inputFileName, 'r')
		### read in output file prefix ###
		line = fd.readline().rstrip()
		exp_prefix = line
		self.output_prefix = exp_prefix
		fd_log_start = open(exp_prefix+"job_log_start.txt", "w")
		fd_log_finish = open(exp_prefix+"job_log_finish.txt", "w")
		fd_log_bb = open(exp_prefix+"job_log_bb.txt", "w")
		fd_cp = open(exp_prefix+"sim_cp_bw.txt", "w")
		fd_fault = open(exp_prefix+"fault_log.txt", "w")
		fd_si = open(exp_prefix + "stage_in.txt", "w")
		fd_so = open(exp_prefix + "stage_out.txt", "w")
		fd_log_start.write("#jobid   compute_node_count   user_requested_time   arrival_time   start_time   enqueue_time\n")
		fd_log_finish.write("#jobtype  pipeid  jobid   compute_node_count   user_requested_time   arrival_time   start_time   enqueue_time   finish_time\n")
		fd_log_bb.write("#bbid   reqs   time\n")
		fd_cp.write("cpID    bandwidth \n")
		### read in bb architecture ###
		line = fd.readline().rstrip()
		if line == "concentrated":
			self.mode = 1
			self.continue_concentratedBBs(jobdoneQ, fd, fd_log_start, fd_log_finish, fd_log_bb, fd_cp, fd_fault,fd_si, fd_so)
		elif line == "local":
			self.mode = 0
			self.continue_localBBs(jobdoneQ, fd, fd_log_start, fd_log_finish, fd_log_bb, fd_cp, fd_fault, fd_si, fd_so)
		else:
			print('burst buffer architecture not recognized')
	def wait(self, jobdoneQ):
		event = yield AllOf(self.env, jobdoneQ)
		print('wait done')


	@staticmethod
	def setJobBBChkpFreq(computeNodeMem, ssdBandwidth, faultRate, newjob):
		chkpTime = computeNodeMem * newjob.chkpRatio / ssdBandwidth
		chkpFreq = math.sqrt(2* chkpTime * faultRate)
		newjob.cpFreq = chkpFreq
		print('job %d checkpoint requency %f' % (newjob.jobID, newjob.cpFreq))

	def setJobChkpFreq(self, newjob):

		#estimate the checkpoint Time
		chkpTime = newjob.computeNodeNumber * newjob.chkpRatio * self.computeNodeMem / (self.burstBufferNodeNumber * self.burstBufferNodeBandwidth)

		print('expected chkpTime %f' % chkpTime)
		chkpFreq = math.sqrt(2* chkpTime * self.faultRate) 
		print('chkpFreq %f' % chkpFreq)
		newjob.setChkpFreq(chkpFreq)
		print('job id %d checkpoint frequency %f' % (newjob.jobID, newjob.cpFreq))

	@staticmethod
	def getFloat(line):
		args = line.split(':')
		print(line)
		return float(args[1])

	@staticmethod
	def getInt(line):
		args = line.split(':')
		return int(args[1])

	@staticmethod
	def getStr(line):
		args = line.split(':')
		return args[1]

	@staticmethod
	def getJobParams(fd, job):
		### read job type ###
		line = fd.readline()
		jobType = line.rstrip()
		job.setJobType(jobType)		

		### read pipe id ###
		line = fd.readline()
		pipeID = SimObj.getInt(line.rstrip())
		job.setPipeID(pipeID)

		### read job id ###
		line = fd.readline()
		jobID = SimObj.getInt(line.rstrip())
		job.setJobID(jobID)

		### read job length ###
		line = fd.readline()
		jobLen = SimObj.getFloat(line.rstrip()) * 60 * 60
		job.setJobLen(jobLen)
		
		### read job compute node number ###
		line = fd.readline()
		numNode = SimObj.getInt(line.rstrip())
		job.setNodeNumber(numNode)

		### read output ratio ###
		line = fd.readline()
		chkpMemRatio = SimObj.getFloat(line.rstrip())
		job.setCheckpointMemoryRatio(chkpMemRatio)
	
		### read burst buffer number per node ###
		line = fd.readline()
		bbNodeNum = SimObj.getInt(line.rstrip())
		job.setBurstBufferUseNumber(bbNodeNum)
	
	@staticmethod
	def getJobBBParams(fd, job):
		### read job type
		line = fd.readline().rstrip()
		jobType = line
		job.jobType = jobType

		### read pipe id
		line = fd.readline().rstrip()
		pipeID = SimObj.getInt(line)
		job.pipeID = pipeID

		###read jobID
		line = fd.readline().rstrip()
		jobID = SimObj.getInt(line)
		job.jobID = jobID
		
		### read job len
		line = fd.readline()
		duration = SimObj.getFloat(line.rstrip()) * 60 * 60
		job.jobLen = duration
		print('job len %f' % job.jobLen)
		line = fd.readline()
		numNode = SimObj.getInt(line.rstrip())
		job.computeNodeNumber = numNode
		print('compute node number %d' % job.computeNodeNumber)
		line = fd.readline()
		chkpMemRatio = SimObj.getFloat(line.rstrip())
		job.chkpRatio = chkpMemRatio

	@staticmethod
	def setJobPfsBandwidth(job, pfsBandwidth):
		job.pfsBandwidth = pfsBandwidth

	@staticmethod
	def setJobCompMemSize(job, compMemSize):
		job.compNodeMemSize = compMemSize

	def continue_localBBs(self, jobdoneQ, fd, fd_start, fd_finish, fd_bb, fd_cp, fd_fault, fd_si, fd_so):
		#read in local BBs simulation config parameters
		print('in local BBs')
		#read fault rate
		line = fd.readline()
		self.faultRate = SimObj.getFloat(line.rstrip()) * 60 * 60
		print('fault rate %f' % self.faultRate)
		#read in recovery time
		line = fd.readline()
		self.nodeRecoveryTime = SimObj.getFloat(line.rstrip()) * 60 * 60
		#read in parity
		line = fd.readline()
		self.parity = SimObj.getInt(line.rstrip())
		#skip line
		fd.readline()
		#read BB total bandwidth
		line = fd.readline()
		self.bbTotalBandwidth = SimObj.getFloat(line.rstrip())
		#read BB total write bw
		line = fd.readline()
		self.bbTotalWrite = SimObj.getFloat(line.rstrip())
		#read BB total read bw
		line = fd.readline()
		self.bbTotalRead = SimObj.getFloat(line.rstrip())
		
		print('totalb bb bandwidth %f' % self.bbTotalBandwidth)
		#read PFS bandwidth
		line = fd.readline()
		self.pfsBandwidth = SimObj.getFloat(line.rstrip())
		print('pfs bandwidth %f' % self.pfsBandwidth)
		#read total compute node
		line = fd.readline()
		self.totalComputeNode = SimObj.getInt(line.rstrip())
		print('total compute node %f' % self.totalComputeNode)
		#compute per-node burst buffer bandwidth
		self.ssdBandwidth = self.bbTotalBandwidth / self.totalComputeNode
		self.bbWrite = self.bbTotalWrite / self.totalComputeNode
		self.bbRead = self.bbTotalRead / self.totalComputeNode
		print('bb write %f bb read %f' % (self.bbWrite, self.bbRead))
		#read compute node memory size
		line = fd.readline()
		self.computeNodeMem = SimObj.getFloat(line.rstrip())
		print('compute node mem %d' % self.computeNodeMem)
		#read compute node bandwidth
		line = fd.readline()
		self.computeNodeBandwidth = SimObj.getFloat(line.rstrip())
		fd.readline()
		print('computeNodeBandwidth %f' % self.computeNodeBandwidth)

		pfsReqQ = simpy.Store(self.env)
		self.pfs = PFS(self.env, self.pfsBandwidth, pfsReqQ)
		currentAvailable = [self.totalComputeNode]

		#initialize compute node with loca SSDs
		for i in range(self.totalComputeNode):
			computeNode = Compute_BB_Node(self.env, i, self.computeNodeMem, self.pfsBandwidth, self.ssdBandwidth, self.bbWrite, self.bbRead, self.parity)
			self.computeNodes.append(computeNode)

		jobid = 0
		line = fd.readline()
		while line[:3] != "END":
			args = line.split(' ')
			if args[0] == 'job':
				jobdone = self.env.event()
				jobdoneQ.append(jobdone)
				newJob = Job_localBBs(self.env, jobdone, fd_finish, currentAvailable, self.bbWrite, self.bbRead, self.parity)
				SimObj.getJobBBParams(fd, newJob)
				SimObj.setJobBBChkpFreq(self.computeNodeMem, self.ssdBandwidth, self.faultRate, newJob)
				SimObj.setJobPfsBandwidth(newJob, self.pfsBandwidth)
				SimObj.setJobCompMemSize(newJob, self.computeNodeMem)
				self.jobs.append(newJob)
				line = fd.readline()
				jobid = jobid + 1
		scheduler = Scheduler(self.env, self.mode, self.computeNodes, None, self.jobs, jobdoneQ, fd_start, self.nodeRecoveryTime, self.scheduling, currentAvailable, pfsReqQ)
		scheduler.startScheduler()
		for i in self.jobs:
			i.schedulerProc = scheduler.run
		faultGenerator = PoissonFaultGenerator(self.env, self.mode, scheduler.run, self.faultRate, self.jobs, self.totalComputeNode, self.computeNodes, None, None, self.nodeRecoveryTime, currentAvailable, fd_fault)
		faultGenerator.start_fault()
		scheduler.addFaultGen(faultGenerator.start)
		self.env.run(scheduler.run)
		
		fd = open(self.output_prefix+"simulation_result_stats1.txt", "w")
		fd.write("%s\n" % self.output_prefix)
		totalIdle = 0
		totalNonIdle = 0
		cpID = 0
		stageInID = 0
		stageOutID = 0
		for i in range(0, self.totalComputeNode):
			totalIdle = totalIdle + self.computeNodes[i].totalIdle
			totalNonIdle = totalNonIdle + self.computeNodes[i].totalNonIdle

		fd.write("total idle time %f\n" % totalIdle)
		fd.write("total non idle time %f\n" % totalNonIdle)
		fd.write("efficiency %f\n" % (totalNonIdle / (totalIdle + totalNonIdle)))
		for i in range(0, len(self.jobs)):
			fd_finish.write('%s %d %d %d %f %f %f ' % (self.jobs[i].jobType, self.jobs[i].pipeID, self.jobs[i].jobID,  self.jobs[i].computeNodeNumber, self.jobs[i].jobLen, self.jobs[i].timeToSchedule, self.jobs[i].stats.enqTime))
			length = len(self.jobs[i].stats.startTime)
			for j in range(0, length):
				fd_finish.write("%f %f " % (self.jobs[i].stats.startTime[j], self.jobs[i].stats.finishTime[j]))
			fd_finish.write('\n')
			fd.write("job %d stats\n" % self.jobs[i].jobID)
			fd.write("total PFS IO Time %f\n" % self.jobs[i].stats.totalPFSIOTime)
			fd.write('total stage in time %f\n' % self.jobs[i].stats.totalStageInTime)
			fd.write('total stage out time %f\n' % self.jobs[i].stats.totalStageOutTime)
			fd.write('total read time %f\n' % self.jobs[i].stats.totalReadTime)
			fd.write('total write time %f\n' % self.jobs[i].stats.totalWriteTime)
			fd.write("total IO Time %f\n" % self.jobs[i].stats.totalIOTime)
			fd.write("total ComputeTime %f\n" % self.jobs[i].stats.totalComputeTime)
			fd.write("total wasted time %f\n" % self.jobs[i].stats.wastedComputeTime)
			fd.write("effective percentage %f\n" % ((self.jobs[i].stats.totalComputeTime - self.jobs[i].stats.wastedComputeTime)/(self.jobs[i].stats.totalIOTime + self.jobs[i].stats.totalComputeTime)))
			fd.write("**************\n")

			length = len(self.jobs[i].stats.cpBandwidth)
			for j in range(0, length):
				fd_cp.write('%d %f\n' % (cpID, self.jobs[i].stats.cpBandwidth[j]))
				cpID = cpID + 1

			length = len(self.jobs[i].stats.stageInBandwidth)
			for j in range(0, length):
					fd_si.write('%d %f\n' % (stageInID, self.jobs[i].stats.stageInBandwidth[j]))
					stageInID = stageInID + 1

			length = len(self.jobs[i].stats.stageOutBandwidth)
			for j in range(0, length):
					fd_so.write('%d %f\n' % (stageOutID, self.jobs[i].stats.stageOutBandwidth[j]))
					stageOutID = stageOutID + 1
			
		fd.close()
		fd_start.close()
		fd_finish.close()
		fd_bb.close()
		fd_cp.close()
		fd_fault.close()
		fd_si.close()
		fd_so.close()

	def continue_concentratedBBs(self, jobdoneQ, fd, fd_start, fd_finish, fd_bb, fd_cp, fd_fault, fd_si, fd_so):
		#read in concentrated BB simulation config parameters
		
		#read fault rate
		line = fd.readline()
		self.faultRate = SimObj.getFloat(line.rstrip()) * 60 * 60 #convert to seconds
		#read recovery rate
		line = fd.readline()
		self.nodeRecoveryTime = SimObj.getFloat(line.rstrip()) * 60 * 60
		#parity setting
		line = fd.readline()
		self.parity = SimObj.getInt(line.rstrip())
		#burst buffer assignment
		line = fd.readline().rstrip()
		self.bbAssignment = SimObj.getInt(line.rstrip())
		#scheduling
		line = fd.readline().rstrip()
		self.scheduling = SimObj.getInt(line.rstrip())
		fd.readline() #skip seperating line
		#read PFS bandwidth
		line = fd.readline()
		self.pfsBandwidth = SimObj.getFloat(line.rstrip())

		#read in burst buffer info
		line = fd.readline()
		self.burstBufferNodeNumber = SimObj.getInt(line.rstrip())
		line = fd.readline()
		self.burstBufferNodeBandwidth = SimObj.getFloat(line.rstrip())
		#compute burst buffer aggregate bandwidth
		self.burstBufferAggregBw = self.burstBufferNodeNumber * self.burstBufferNodeBandwidth
		print('burst buffer aggregate bandwidth %f' % self.burstBufferAggregBw)
		line = fd.readline()
		self.burstBufferNodeCapacity = SimObj.getFloat(line.rstrip())
		#read burst buffer agg write bw
		line = fd.readline()
		self.bbAggWrite = SimObj.getFloat(line.rstrip())
		self.bbWrite = self.bbAggWrite / self.burstBufferNodeNumber
		print('bb node write bw %f' % self.bbWrite)
		#read in burst buffer agg read bw
		line = fd.readline()
		self.bbAggRead = SimObj.getFloat(line.rstrip())
		self.bbRead = self.bbAggRead / self.burstBufferNodeNumber
		print('bb node read bw %f' % self.bbRead)
		#read total compute node count
		line = fd.readline()
		self.totalComputeNode = SimObj.getInt(line.rstrip())
		#read compute node mem size
		line = fd.readline()
		self.computeNodeMem = SimObj.getFloat(line.rstrip())
		#read compute node bandwidth
		line = fd.readline()
		self.computeNodeBandwidth = SimObj.getFloat(line.rstrip())
		fd.readline() #skip seperating line

		#init pfs
		pfsReqQ = simpy.Store(self.env)
		self.pfs = PFS(self.env, self.pfsBandwidth, pfsReqQ)
		#create a map that keeps track of each node being allocated or not
		#-1 - not used; 1 - used; -2 is down
		#initialize some burst buffers
		for i in range(self.burstBufferNodeNumber):
			reqQ = simpy.Store(self.env)
			bbNode = BBNode(self.env, i, self.burstBufferNodeBandwidth, reqQ, fd_bb, pfsReqQ, self.bbWrite, self.bbRead)
			self.reqQs.append(reqQ)
			self.pfs.bbNodeReqQs.append(reqQ)
			self.burstBuffers.append(bbNode)

		#create computenodes
		for i in range(self.totalComputeNode):
			computeNode = ComputeNode(self.env, self.totalComputeNode, i, self.computeNodeMem, self.computeNodeBandwidth, self.pfsBandwidth, self.reqQs, self.burstBuffers, self.parity, self.bbAssignment)
			self.computeNodes.append(computeNode)

		currentAvailable = [self.totalComputeNode]

		print('burst buffer  reqs q %d' % len(self.reqQs))
		#start reading and creating jobs
		line = fd.readline()
		jobid = 0
		jobdoneQ = []
		while line[:3] != "END":
			args = line.split(' ')
			print('herer args')
			if args[0] == 'job':
				print(args)
				print('job id %d ' % jobid)
				jobdone = self.env.event()
				jobdoneQ.append(jobdone)
				newJob = Job(self.env, self.burstBufferNodeNumber, self.computeNodeMem, self.reqQs, jobdone, fd_finish, currentAvailable, self.parity, pfsReqQ, self.burstBuffers)
				SimObj.getJobParams(fd, newJob)
				self.setJobChkpFreq(newJob)
				SimObj.setJobPfsBandwidth(newJob, self.pfsBandwidth)
				self.jobs.append(newJob)
				line = fd.readline()
				jobid = jobid + 1

		print('total job count %d' % len(self.jobs))
		scheduler = Scheduler(self.env, self.mode, self.computeNodes, self.burstBuffers, self.jobs, jobdoneQ, fd_start, self.nodeRecoveryTime, self.scheduling, currentAvailable, pfsReqQ)
		scheduler.startScheduler()
		for i in self.jobs:
			i.schedulerProc = scheduler.run
		faultGen = PoissonFaultGenerator(self.env, self.mode, scheduler.run, self.faultRate, self.jobs, self.totalComputeNode, self.computeNodes, self.burstBufferNodeNumber, self.burstBuffers, self.nodeRecoveryTime, currentAvailable, fd_fault)
		faultGen.start_fault()
		scheduler.addFaultGen(faultGen.start)
		self.env.run(scheduler.run)
		print("finished at %f" % self.env.now)
		fd = open(self.output_prefix+"simulation_result_stats1.txt", "w")
		fd.write("%s\n"%self.output_prefix)
		
		totalIdle = 0
		totalNonIdle = 0
		cpID = 0
		stageInID = 0
		stageOutID = 0
		for i in range(0, self.totalComputeNode):
			totalIdle = totalIdle + self.computeNodes[i].totalIdle
			totalNonIdle = totalNonIdle + self.computeNodes[i].totalNonIdle
		fd.write("total idle time %f\n" % totalIdle)
		fd.write("total non idle time %f\n" % totalNonIdle)
		fd.write("efficiency %f\n" % (totalNonIdle / (totalIdle + totalNonIdle)))
		for i in range(0, len(self.jobs)):
			#write to job_log_finish first
			fd_finish.write('%s %d %d %d %f %f %f ' % (self.jobs[i].jobType, self.jobs[i].pipeID, self.jobs[i].jobID, self.jobs[i].computeNodeNumber, self.jobs[i].jobLen, self.jobs[i].timeToSchedule, self.jobs[i].stats.enqTime))
			length = len(self.jobs[i].stats.startTime)
			for j in range(0, length):
				fd_finish.write("%f %f " % (self.jobs[i].stats.startTime[j], self.jobs[i].stats.finishTime[j]))
			fd_finish.write('\n')

			fd.write("job %d stats\n" % self.jobs[i].jobID)
			fd.write("total PFS IO Time %f\n" % self.jobs[i].stats.totalPFSIOTime)
			fd.write('total stageIn time %f\n' % self.jobs[i].stats.totalStageInTime)
			fd.write('total stageOut Time %f\n' % self.jobs[i].stats.totalStageOutTime)
			fd.write('total read time %f\n' % self.jobs[i].stats.totalReadTime)
			fd.write('total write time %f\n' % self.jobs[i].stats.totalWriteTime)
			fd.write("total CP IO Time %f\n" % self.jobs[i].stats.totalIOTime)	
			fd.write("total ComputeTime %f\n" % self.jobs[i].stats.totalComputeTime)
			fd.write("total wasted time %f\n" % self.jobs[i].stats.wastedComputeTime)
			fd.write("effective percentage %f\n" % ((self.jobs[i].stats.totalComputeTime - self.jobs[i].stats.wastedComputeTime)/(self.jobs[i].stats.totalIOTime + self.jobs[i].stats.totalComputeTime + self.jobs[i].stats.totalStageInTime + self.jobs[i].stats.totalStageOutTime)))
			fd.write("**************\n")
			#now write out cp and bandwidth
			length = len(self.jobs[i].stats.cpBandwidth)
			for j in range(0, length):
				fd_cp.write('%d %f\n' % (cpID, self.jobs[i].stats.cpBandwidth[j]))
				cpID = cpID + 1
			length = len(self.jobs[i].stats.stageInBandwidth)
			for j in range(0, length):
				fd_si.write('%d %f\n' % (stageInID, self.jobs[i].stats.stageInBandwidth[j]))
				stageInID = stageInID + 1

			length = len(self.jobs[i].stats.stageOutBandwidth)
			for j in range(0, length):
				fd_so.write("%d %f\n" % (stageOutID, self.jobs[i].stats.stageOutBandwidth[j]))
				stageOutID = stageOutID + 1
		fd.close()
		fd_start.close()
		fd_finish.close()
		fd_bb.close()
		fd_cp.close()
		fd_fault.close()
		fd_si.close()
		fd_so.close()
