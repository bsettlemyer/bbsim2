import sys
import os
import math
####start####
fileName = sys.argv[1]
fullSize = sys.argv[2]
outPath = sys.argv[3]
full = int(fullSize)
half = full / 2
quarter = half / 2
print("%d %d %d" %(full, half, quarter))
fd = open(fileName, 'r')
fd_out = open(outPath + 'job_queue_log.dat', "w")
fd_out.write("minute full_size half_size quarter_size\n")
#read file line by line and convert each line into each job stats
#date format(separated by space):
#jobid, compute node count, user requested time, arrival time, start time, enqueue time, finish time
fd.readline()
jobStats = []
endTime = -1
segment = 60 #1 minute
skip = 0
for line in fd:
	if line[0] == '#':
		skip
	splits = line.rstrip().split(' ')
	#convert strings into numericals
	#find the end time of the simulation
	nums = []
	for i in splits:
		nums.append(float(i))
	
	print(nums)
	length = len(nums)
	if endTime <= nums[length - 1]:
		endTime = nums[length - 1]
	jobStats.append(nums)
print(endTime)

numSegments = math.ceil(endTime / segment)
print("number of hours %d" % math.ceil(numSegments))
count_full = 0
count_half = 0
count_quarter = 0
for i in jobStats:
	size = int(i[1])
	print(size)
	if size == full:
		count_full = count_full + 1
	elif size == half:
		count_half = count_half + 1
	elif size == quarter:
		count_quarter = count_quarter + 1
fd_out.write("0 %d %d %d\n" % (count_full, count_half, count_quarter))
for i in range(1, numSegments+1):
	count_full = 0
	count_half = 0
	count_quarter = 0
	for j in jobStats:
		size = int(j[1])
		length = len(j)
		#find how many time pairs are there
		pairs = int((length - 5) / 2)
		for k in range(0, pairs):
			if (j[5 + k*2]>=(i-1)*segment and j[6 + k*2] < i*segment) or (j[5 + k*2]<(i-1)*segment and j[6 + k*2]>(i-1)*segment) or(j[5+k*2]<i*segment and j[6+k*2]>=i*segment):
				if size == full:
					count_full = count_full + 1
				elif size == half:
					count_half = count_half + 1
				elif size == quarter:
					count_quarter = count_quarter + 1
				break
		#print("j4 %f j6 %f " % (j[4], j[6]))
	fd_out.write("%d %d %d %d\n" % (i, count_full, count_half, count_quarter))
fd_out.close()
