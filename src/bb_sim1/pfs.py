import simpy
from simpy import *
class PFS:

	def __init__(self, env, bandwidth, requestQ):
		self.env = env
		self.bandwidth = bandwidth
		self.reqQ = requestQ
		self.shareCnt = []
		self.shareCnt.append(0)
		self.start = env.process(self.run())
		self.checkpoints = dict() #stores checkpoint job
		self.pfsProc = env.process(self.run())
		self.bbNodeReqQs = []
		self.jobToExecutor = dict()

	def findReqCounts(self, bbInfo):
		count = 0
		for i in bbInfo:
			for j in i:
				count = count + 1
		return count

	def run(self):

		state = 0
		reqID = 0
		ioReqs = dict()
		#jobToExecutor = dict()
		jobID = -1

		while True:
			if state == 0:
				msg = yield self.reqQ.get()
				msgType = msg[0]
				#msg type: 0 - stageIn; 1 - stageOut; -1 - cancel
				if msgType == 0:
					jobID = msg[1]
					reqCounts = self.findReqCounts(msg[2])
					self.shareCnt[0] = self.shareCnt[0] + reqCounts
					print('job %d total bb reqs %d' % (jobID, reqCounts))
					stageInProc = self.env.process(self.stageInExecutor(msg))
					self.jobToExecutor[jobID] = stageInProc
					print('keys in pfs')
					print(self.jobToExecutor.keys())
				elif msgType == -1:
					jobID = msg[1]
					state = -1
					
				if msgType == 1:
					jobID = msg[1]
					print('pfs got stageout requeset for job %d' % jobID)
					reqCounts = self.findReqCounts(msg[2])
					self.shareCnt[0] = self.shareCnt[0] + reqCounts
					stageOutProc = self.env.process(self.stageOutExecutor(msg))
					self.jobToExecutor[jobID] = stageOutProc
				elif msgType == 2:
					pass

			elif state == -1:
				print('pfs got cancle request from job %d at %f' % (jobID, self.env.now))
				print(self.jobToExecutor.keys())
				executor = self.jobToExecutor[jobID]
				if executor.is_alive:
					executor.interrupt(-1)
				state = 0

	def stageInExecutor(self, msg):
		jobID = msg[1]
		currentBw = self.bandwidth / self.shareCnt[0]
		print('stage in executor for job %d current bandwidth %f at %f' % (jobID, currentBw, self.env.now))
		#yield self.env.timeout(5)
		bbNodesInfo = msg[2]
		reqSize = msg[3]
		syncEvents = []
		doneEvents = []
		doneEventsWithInfo = []
		time1 = self.env.now
		jobReqID = 0
		for i in bbNodesInfo:
			for j in i:
				#construct stage-In request to bbNodes
				doneEvent = self.env.event()
				doneEvents.append(doneEvent)
				doneEventsWithInfo.append((doneEvent, j))
				bbMsg = (-2, doneEvent, reqSize, None, jobID, jobReqID, 0, currentBw)
				syncEvents.append(self.bbNodeReqQs[j].put(bbMsg))
				jobReqID = jobReqID + 1
		try:
			yield AllOf(self.env, syncEvents)
			#now start the waiting process
			done = 0
			count = 0
			nodeCnt = len(syncEvents)
			current = nodeCnt
			waitCnt = nodeCnt
			while done == 0:
				yield AnyOf(self.env, doneEvents)
				for i in range(current):
					try:
						if i >= current:
							continue
						if doneEvents[i].value == 1:
							#this bb node faults
							done = 2
							break
						elif doneEvents[i].value == 2:
							waitCnt = waitCnt - 1
							del doneEvents[i]
							del doneEventsWithInfo[i]
							current = len(doneEvents)
							count = count + 1
							#Need to inform other BB that there is a change in bandwidth
							#first inform all bb for this executor, then inform all other pfs executors
							for info in doneEventsWithInfo:
								bbID = info[1]
								#construct bb message
								
							if count == nodeCnt:
								done = 1
							
					except AttributeError:
						pass
		except simpy.Interrupt as i:
			msg = i.cause
			if msg == -1:
				for i in bbNodesInfo:
					for j in i:
						bbMsg = (-3, None, None, None, jobID)
						yield self.bbNodeReqQs[j].put(bbMsg) 
						print('pfs sent cancel message to bb %d' % j)
			
			return
		if done == 2:
			#some node failed, tell all other burst buffer nodes to cancel
			print('THERE IS A FAULT AT SOME BB, CANCEL THIS STAGE IN for job %d at %f' % (jobID, self.env.now))
			del syncEvents[:]
			jobReqID = 0
			for i in bbNodesInfo:
				for j in i:
					bbMsg = (-3, None, None, None, jobID)
					syncEvents.append(self.bbNodeReqQs[j].put(bbMsg))
			yield AllOf(self.env, syncEvents)
			msg[4].succeed(value = 1)
		elif done == 1:
			msg[4].succeed(value = 0)
			print('stage in for job %d finished at %f' % (jobID, self.env.now))
		time2 = self.env.now

	def stageOutExecutor(self, msg):
		jobID = msg[1]
		bbNodesInfo = msg[2]
		reqSize = msg[3]
		syncEvents = []
		doneEvents = []
		jobReqID = 0
		
		for i in bbNodesInfo:
			for j in i:
				doneEvent = self.env.event()
				doneEvents.append(doneEvent)
				bbMsg = (-2, doneEvent, reqSize, None, jobID, jobReqID, 1)
				syncEvents.append(self.bbNodeReqQs[j].put(bbMsg))
				jobReqID = jobReqID + 1
		
		try:
			yield AllOf(self.env, syncEvents)
			#now start the waiting process
			done = 0
			count = 0
			nodeCnt = len(syncEvents)
			current = nodeCnt
			waitCnt = nodeCnt
			while done == 0:
				yield AnyOf(self.env, doneEvents)
				for i in range(current):
					try:
						if i >= current:
							continue
						if doneEvents[i].value == 1:
							#bb node faults
							done = 2
							break
						elif doneEvents[i].value == 2:
							waitCnt = waitCnt - 1
							del doneEvents[i]
							current = len(doneEvents)
							count = count + 1
							if count == nodeCnt:
								done = 1
					except AttributeError:
						pass
		except simpy.Interrupt as i:
			msg = i.cause
			if msg == -1:
				print('stage out executor interrupted at %f' % self.env.now)
				for i in bbNodesInfo:
					bbMsg = (-3, None, None, None, jobID)
					yield self.bbNodeReqQs[j].put(bbMsg)
			return

		if done == 2:
			print('SOME BB FAILED CANCLE STAGE OUT for job %d at %f' % (jobID, self.env.now))
			del syncEvents[:]
			jobReqID = 0
			for i in bbNodesInfo:
				for j in i:
					bbMsg = (-3, None, None, None, jobID)
					syncEvents.append(self.bbNodeReqQs[j].put(bbMsg))
			yield AllOf(self.env, syncEvents)
			msg[4].succeed(value = 1)
		elif done == 1:
			msg[4].succeed(value = 0)
			print('stage out for job %d finished at %f' % (jobID, self.env.now))

	def writeExecutor(self, event, completeEvent, jobID, cpID, partCnt, reqSize, srcBandwidth):
		complete = 0
		startT = float(self.env.now)
		remainSize = float(reqSize)
		startBandwidth = float(self.bandwidth) / float(self.shareCnt[0])
		bbBandwidth = srcBandwidth
		bw = startBandwidth
		if startBandwidth > bbBandwidth:
			#limited by bbBandwidth
			bw = bbBandwidth
		else:
			pass

		remainT = remainSize / bw
		while complete == 0:
			try:
				yield self.env.timeout(remainT)
				complete = 1
			except simpy.Interrupt as i:
				msg = i.cause
				currentT = float(self.env.now)
				if msg == 'changeCount' and complete == 0:
					remainSize = self.calculateRemainSize(remainSize, currentT, startT, bw)
					remainT = self.calculateRemainT(remainSize, bbBandwidth)
					
				elif msg == 'bbFault' and complete == 0:
					complete = 1
				
				elif msg == 'end' and complete == 0:
					complete = 2
					
				else:
					if complete == 0:
						bbBandwidth = msg
						remainSize = self.calculateRemainSize(remainSize, currentT, startT, bw)
						remainT = self.calculateRemainT(remainSize, bbBandwidth)
				startT = currentT

	def completeExecutor(self, completeEvent, jobID, cpID, partCnt):
		yield completeEvent
		self.sharedCnt[0] = self.sharedCnt[0] - 1
		try:
			if done.value == 1:
				return
		except AttributeError:
			pass
		keys = ioreqs.keys()
		

	def calculateRemainSize(self, remainSize, currentT, startT, bandwidth):
		duration = currentT - startT
		remain = remainSize - (duration * bandwidth)
		return remain

	def calculateRemainT(self, remainSize, bbBandwidth):
		thisBandwidth = float(self.bandwidth) / float(self.shareCnt[0])
		if thisBandwidth > bbBandwidth:
			thisBandwidth = bbBandwidth
		else:
			pass
		remainT = remainSize / thisBandwidth
		return remainT
