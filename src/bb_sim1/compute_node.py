import simpy
from simpy import *
import math
import random
import time
#models a compute node
class ComputeNode:
	
	#def __init__(self, env, jid, index, myid, bbnum, memsize, ratio, duration, chkpfq, bbcnt, eventlist, barriers, complete):
	def __init__(self, env, computeNodeNum, nodeid, memsize, bandwidth, pfsBandwidth, reqQs, bbNodes, parity, bbAssignment):
		self.env = env
		self.computeNodeNumber = computeNodeNum
		self.nodeStat = -1
		self.nodeID = nodeid
		self.localID = -1
		self.memSize = memsize
		self.bandwidth = bandwidth
		self.reqQs = reqQs
		self.bbNodes = bbNodes
		self.pfsBandwidth = pfsBandwidth
		self.run = None
		self.syncCounter = None
		self.totalNonIdle = 0
		self.totalIdle = 0
		self.nonIdleStart = 0
		self.nonIdleEnd = 0
		self.idleStart = 0
		self.idleEnd = 0
		self.jobComputeNodeCount = -1
		self.jobProc = None
		self.schedulerProc = None
		self.parity = parity
		self.bbAssignment = bbAssignment
		self.bbToUse = []
	def setJobComputeNodeCount(self, count):
		self.jobComputeNodeCount = count

	def setNodeLocalID(self, localID):
		self.localID = localID

	def setJobID(self, jobID):
		self.jobID = jobID

	def setJobLength(self, length):
		self.length = length

	def setTotalBurstBufferNumber(self, totalBBNum):
		self.totalBBNum = totalBBNum

	def setBurstBufferUsageNumber(self, bbCnt):
		self.bbCnt = bbCnt

	def setOutputRatio(self, ratio):
		self.outputRatio = ratio

	def setCheckpointFreq(self, freq):
		self.chkpFreq = freq

	def setBarriers(self, barriers):
		self.bbSync = barriers

	def setCompleteEvent(self, complete):
		self.finished = complete

	def createProcess(self, cycle, stats, timeProgress):
		proc = self.env.process(self.compNode(cycle, stats, timeProgress))
		self.run = proc
	
	def setChkpRecord(self, records):
		self.chkpRecs = records

	def assignBBNodes(self):
		if self.bbAssignment == 0:
			self.pickBBNodes_random()
		elif self.bbAssignment == 1:
			self.pickBBNodes_randomRoundRobin()
		print('comp %d bb assginemtn %d' % (self.nodeID, self.bbAssignment))
		print(self.bbToUse)

	def compNode(self, cpCycle, stats, timeProgress):
		#accumulate idle time
		#self.idleEnd = self.env.now
		#self.totalIdle = self.totalIdle + self.idleEnd - self.idleStart
		#update non-idle time
		#self.nonIdleStart = self.env.now
		#print('input cycle is %d' % cpCycle)
		jobsize = 0
		totalSize = 0
		if self.parity == 0:
			jobsize = self.memSize * self.outputRatio / self.bbCnt
			totalSize = self.memSize * self.outputRatio * self.jobComputeNodeCount
		else:
			jobsize = self.memSize * self.outputRatio * 1.2 / self.bbCnt
			totalSize = self.memSize * self.outputRatio * self.jobComputeNodeCount * 1.2
		retval = 0
		cycle = cpCycle
		readTime = totalSize / self.pfsBandwidth

		#if self.bbAssignment == 0:
		#	print('cn %d using random bb' % self.nodeID)
		#	bbToUse = self.pickBBNodes_random(self.nodeID, self.bbNodes, self.totalBBNum, self.bbCnt)
		#elif self.bbAssignment == 1:
		#	print('cn %d using random round robin' % self.nodeID)
		#	self.pickBBNodes_randomRoundRobin(bbToUse)
		#print('comp node %d picked bb nodes ' % self.nodeID)
		#print(bbToUse)
		#print('readTime is %f' % readTime)
		#print('freq %f' % self.chkpFreq)
		if cycle >= 0:
			time0 = self.env.now
			############ RECOVERY ###############
			#we know that it needs to restart, restart from burst buffer because it is already staged in or in the bb
			print('compute %d started recovring at %f' % (self.nodeID, self.env.now))
			self.bbSync[self.localID] = (cycle, self.env.event(), self.env.event())
			wakeups = []
			backups = []
			yields = []
			bwPerBB = self.bandwidth / self.bbCnt
			for i in range(self.bbCnt):
				wakeup = self.env.event()
				wakeups.append((wakeup, i))
				backups.append(wakeup)
				msg = (self.nodeID, wakeup, jobsize, bwPerBB, 1)
				yields.append(self.reqQs[self.bbToUse[i]].put(msg))
			try:
				done = 0
				count = 0
				yield AllOf(self.env, yields)
				origCnt = len(wakeups)
				current = origCnt
				shareCnt = self.bbCnt

				while done == 0:
					yield AnyOf(self.env, backups)
					for i in range(current):
						try:
							if i >= current:
								continue
							if wakeups[i][0].value == 1:
								shareCnt = shareCnt - 1
								if shareCnt != 0:
									bbPerBB = self.bandwidth / shareCnt
									bbDoneID = wakeups[i][1]
									for j in range(0, len(wakeups)):
										if bbDoneID != wakeups[j][1]:
											msg = (self.nodeID, None, None, bbPerBB, 1)
											yield self.reqQs[self.bbToUse[j][1]].put(msg)
								done = 2
								break
							elif wakeups[i][0].value == 2:
								shareCnt = shareCnt - 1
								if shareCnt != 0:
									bwPerBB = self.bandwidth / shareCnt
									bbDoneID = wakeups[i][1]
									for j in range(0, len(wakeups)):
										msg = (self.nodeID, None, None, bwPerBB)
										yield self.reqQs[self.bbToUse[wakeups[j][1]]].put(msg)
								del wakeups[i]
								del backups[i]
								current = len(wakeups)
								count = count + 1
								if count == origCnt:
									done = 1
						except AttributeError:
							pass
			
				self.bbSync[self.localID][1].succeed()
				if done == 2:
					#some bb node failed, fail job immediately
					print('compute noe %d interrupting job %d' % (self.nodeID, self.nodeStat))
					self.jobProc.interrupt(-2)
					for i in range(self.bbCnt):
						msg = (self.nodeID, None, -1, None)
						yield self.reqQs[self.bbToUse[i]].put(msg)
					retval = -2
				else:
					if self.localID == 0:
						length = len(self.bbSync)
						for i in range(length):
							if i != self.localID:
								if(self.bbSync[i][0] == cycle):
									yield self.bbSync[i][1]
						for i in range(length):
							self.bbSync[i][2].succeed()
					else:
						yield self.bbSync[self.localID][2]
					print('recovery success %d' % self.nodeID)
					retval = 1
			except simpy.Interrupt as i:
				print('comp node %d interrupted during restart' % self.nodeID)
				retval = -2
				for i in range(self.bbCnt):
					msg = (self.nodeID, None, -1, None)
					yield self.reqQs[self.bbToUse[i]].put(msg)
			time1 = self.env.now
			if self.localID == 0:
				stats.recordIOTime(cycle, time1 - time0)
				stats.recordReadTime(time1 - time0)

		if retval == -2:
			#this means recovery failed due to some compute node failures
			#or the job ran out of time during recovery
			#print('some compute node failed during recovery, abort job')
			print('restart failed compute %d' % self.nodeID)
			pass
		else:
			#recovery successful or does not need restart
			if retval == 1:
				print('comp %d restart completed at %f' % (self.nodeID, self.env.now))
				cycle = cycle + 1
			else:
				cycle = 0
				#each compute node must read in a checkpoint
				#time1 = self.env.now
				#try:
				#	yield self.env.timeout(readTime)
				#except simpy.Interrupt as i:
				#	time2 = self.env.now
				#	if self.localID == 0:
				#		#fault, dont add time back
				#		stats.recordPFSIOTime(time2 - time1)
				#	self.finished.succeed()
				#	self.nonIdleEnd = self.env.now
				#	self.totalNonIdle = self.totalNonIdle + self.nonIdleEnd - self.nonIdleStart
				#	self.idleStart = self.env.now
				#	#must exit now because there is a fault at some node from the job
				#	return

				#time2 = self.env.now
				#if self.localID == 0:
				#	#timeProgress[0] = timeProgress[0] + time2 = time1
				#	#stats.recordIOTime(cycle, time2 - time1)
				#	stats.recordPFSIOTime(time2 - time1)

			elapsed = 0
			state = 0
			startT = self.env.now
			freq = self.chkpFreq
			good = 0
			while elapsed <= self.length:
				if state == 0:
					if self.localID == 0:
						print('current cycle %d' % cycle)
					#compute phase
					#print('job %d node %d beging cycle %d, compute Time %f' % (self.jobID, self.nodeID, cycle, freq))
					#self.bbSync[self.localID] = (cycle, self.env.event(), self.env.event())
					#if self.localID == 0:
					#	self.syncCounter[0] = 0
					#print('job %d cn %d start compute at %f' %(self.jobID, self.nodeID, self.env.now))
					time1 = self.env.now
					try:
						yield self.env.timeout(freq)
						time2 = self.env.now
						if self.localID == 0:
							print('finished compute')
							stats.recordComputeTime(cycle, time2 - time1)
						#if freq != self.chkpFreq:
						#	print('time to end freq %f chkpfreq %f' % (freq, self.chkpFreq))
						#	self.bbSync[self.localID][1].succeed()
						#	break
						state = 1
					except simpy.Interrupt as i:
						#print('job %d node %d cycle %d interrupted during compute phase at %f' % (self.jobID, self.nodeID, cycle, self.env.now))
						time2 = self.env.now
						if self.localID == 0:
							#this will be waster compute time, do not add it to progress
							stats.recordComputeTime(cycle, time2 - time1) 
						msg = i.cause
						#self.interruptHandler(cycle, msg)
						cycle = cycle - 1
						if cycle < 0:
							cycle = 0
	
						state = 0
						elapsed = self.env.now - startT
						good = -1
						break
				elif state == 1:
					#checkpoint phase
					self.bbSync[self.localID] = (cycle, self.env.event(), self.env.event())
					wakeups = []
					backups = []
					yields = []
					bwPerBB = self.bandwidth / self.bbCnt
					#bbToUse = ComputeNode.pickBBNodes_random(self.nodeID, self.bbNodes, self.totalBBNum, self.bbCnt)
					#print('job %d cn %d picked bb at %f' % (self.jobID, self.nodeID, self.env.now))
					for i in range(self.bbCnt):
						wakeup = self.env.event()
						wakeups.append((wakeup, i))
						backups.append(wakeup)
						msg = (self.nodeID, wakeup, jobsize, bwPerBB, 0)
						#print('job %d cn %d sending to bb node %d' % (self.jobID, self.nodeID, bbToUse[i]))
						yields.append(self.reqQs[self.bbToUse[i]].put(msg))
					time3 = self.env.now
					try:
						done = 0
						count = 0
						yield AllOf(self.env, yields) #wait for finishing putting reqs, not sure if needed
						origCnt = len(wakeups)
						current = origCnt
						tempRec = dict()
						time5 = self.env.now
						shareCnt = self.bbCnt
						while done == 0:
							yield AnyOf(self.env, backups)
							for i in range(current):
								try:
									if i >= current:
										continue
									if wakeups[i][0].value == 1:
										shareCnt = shareCnt - 1
										if shareCnt != 0:
											bwPerBB = self.bandwidth / shareCnt
											#notify other bb nodes that there is a change in compute node bandwidth
											bbDoneID = wakeups[i][1]
											for j in range(0, len(wakeups)):
												#send a message notification to all other bb nodes
												if bbDoneID != wakeups[j][1]:
													msg = (self.nodeID, None, None, bwPerBB)
													yield self.reqQs[self.bbToUse[wakeups[j][1]]].put(msg)
													#print('job %d comp node %d finished sending bandwidht changing message to bb %d' % (self.jobID, nodeID, wakeups[j][1]))
										#print('cn %d knows some bb node failed at time %f' % (self.nodeID, self.env.now))
										done = 2
										break
									elif wakeups[i][0].value == 2:
										shareCnt = shareCnt - 1
										if shareCnt != 0:
											bwPerBB = self.bandwidth / shareCnt
											#notify other bb nodes theres a change in compute node bandwidth
											bbDoneID = wakeups[i][1]
											for j in range(0, len(wakeups)):
												#send message to all other bb nodes
												msg = (self.nodeID, None, None, bwPerBB)
												yield self.reqQs[self.bbToUse[wakeups[j][1]]].put(msg)
												#print('job %d comp node %d finished sending bandwidht changing message to bb %d at %f' % (self.jobID, self.nodeID, wakeups[j][1], self.env.now))
										del wakeups[i]
										del backups[i]
										current = len(wakeups)
										count = count + 1
										if count == origCnt:
											done = 1
								except AttributeError:
									pass
	
						#even if there is a bb node failed for this  compute node, we still have to wait for all other compute nodes to finish their IO
						#before moving to the next cycle
						self.bbSync[self.localID][1].succeed()
						if self.localID == 0:
							length = len(self.bbSync)
							for i in range(length):
								if i != self.localID:
									if(self.bbSync[i][0] == cycle):
										yield self.bbSync[i][1]
							#now set others to succeed
							for i in range(length):
								if self.bbSync[i][2].triggered == False:
									self.bbSync[i][2].succeed()
						else:
							yield self.bbSync[self.localID][2]
						
						time4 = self.env.now
						if self.localID == 0:
							#calculate and record bandwidth
							print('finished cp at %f diff is %f' % (self.env.now,(time4 - time3)))
							timeProgress[0] = timeProgress[0] + time4 - time1
							if time4 - time3 != 0:
								bandwidth = jobsize * self.bbCnt * self.jobComputeNodeCount / (time4 - time3)
								#print('checkpoint bandwidth %f, jobsize %f, duration %f, compnodenum %d, bbcnt %d' % (bandwidth, jobsize,(time4 - time3), self.jobComputeNodeCount, self.bbCnt))
								stats.recordCpBandwidth(bandwidth)
								stats.recordIOTime(cycle, time4 - time3)
								stats.recordWriteTime(time4 - time3)

						#print('job %d cn %d cycle %d finished at %f' % (self.jobID, self.localID, cycle, self.env.now))
						time6 = self.env.now
						if done == 1:
							#now we know that the checkpoint was successful, save checkpoint bbnode record
							self.saveChkpRecord(cycle, self.env.now, time4 - time5, time4 - time1, 1)
						elif done == 2:
							self.saveChkpRecord(cycle, self.env.now, time4 - time5, time4 - time1, -1)
						cycle = cycle + 1
						#elapsed = self.env.now - startT
						#if self.length - elapsed < self.chkpFreq:
						#	freq = self.length - elapsed
					
						#self.bbSync[self.localID] = (cycle, self.env.event())
						state = 0
					except simpy.Interrupt as i:
						#print('job %d node %d cycle %d interrupted during checkpoint phase at %f, interrupting bbnodes' % (self.jobID, self.nodeID, cycle, self.env.now))
						msg = i.cause
						#self.interruptHandler(cycle, msg)
						for i in range(self.bbCnt):
							#print('computenode %d sending interrupt to bbnode %d' % (self.nodeID, bbToUse[i]))
							#self.bbNodes[bbToUse[i]].start.interrupt(self.nodeID)
							msg = (self.nodeID, None, -1, None)
							yield self.reqQs[self.bbToUse[i]].put(msg)
						#self.bbSync[self.localID] = self.env.event()
						yield AllOf(self.env, backups)
						time4 = self.env.now
						if self.localID == 0:
							#this is fault, dont add progress time in 
							stats.recordIOTime(cycle, time4 - time3)
							stats.recordWriteTime(time4 - time3)
						#print('cn %d all bb nodes done' % self.nodeID)
						cycle = cycle - 1
						if cycle < 0:
							cycle = 0
	
						state = 0
						good = -1
						break
			#print('job %d cn %d finished at %f with status %d' % (self.jobID, self.localID, self.env.now, good))
		self.finished.succeed(value = 0) #notify job process that this compute node is done
		#self.nonIdleEnd = self.env.now
		#self.totalNonIdle = self.totalNonIdle + self.nonIdleEnd - self.nonIdleStart
		#self.idleStart = self.env.now
	def updateStartIdleNonIdleTime(self):
		self.idleEnd = self.env.now
		self.totalIdle = self.totalIdle + self.idleEnd - self.idleStart
		self.nonIdleStart = self.env.now

	def updateEndIdleNonIdleTime(self):
		self.nonIdleEnd = self.env.now
		self.totalNonIdle = self.totalNonIdle + self.nonIdleEnd - self.nonIdleStart
		self.idleStart = self.env.now

	def saveChkpRecord(self, cycle, timeNow, duration, cycleDuration, statFlag):
		if cycle in self.chkpRecs:
			#some other compute node has created the key for this cycle, we only need to retrieve the node dictionary to insert this node's bbs used
			thisRec = self.chkpRecs[cycle]
			thisRec[self.localID] = (timeNow, duration, cycleDuration, statFlag) #insert our bbs
			#print('job %d cn %d putting records for cycle %d' % (self.jobID, self.nodeID, cycle))
		else:
			#we are the first compute node to write record for this cycle
			ourRec = dict()
			ourRec[self.localID] = (timeNow, duration, cycleDuration, statFlag)
			self.chkpRecs[cycle] = ourRec
			#print('job %d cn %d first to put record for cycle %d' % (self.jobID, self.nodeID, cycle))

	def interruptHandler(self, cycle, msg):
		print('job %d node %d cycle %d interrupted during checkpoint phase at %f' % (self.jobID, self.nodeID, cycle, self.env.now))
	
	def pickBBNodes_randomRoundRobin(self):
		self.bbToUse = []
		#first randomly pick a start point
		chosen = random.randrange(0, self.totalBBNum, 1)
		while self.bbNodes[chosen].status == -1:
			chosen = random.randrange(0, self.totalBBNum, 1)
		self.bbToUse.append(chosen)
		#round robin till the end
		count = 1
		while count != self.bbCnt:
			chosen = chosen + 1
			while self.bbNodes[chosen % self.totalBBNum].status == -1:
				chosen = chosen + 1
			self.bbToUse.append((chosen % self.totalBBNum))
			count = count + 1

	def pickBBNodes_random(self):
		self.bbToUse = []
		for i in range(self.bbCnt):
			#random.seed(nodeID + time.time())
			chosen = random.randrange(0, self.totalBBNum, 1)
			while (chosen in self.bbToUse) or (self.bbNodes[chosen].status == -1):
				chosen = random.randrange(0, self.totalBBNum, 1)
				
			self.bbToUse.append(chosen)
