from simpy import *
from jobtest import *
from bb_node import *
#class Job:
#	def __init__(self, env, eventlist):
#		self.env = env
#		self.nodeCount = 4
#		self.duration = 50
#		self.scheduleT = 3
#		self.cpfreq = 20
#		self.events = eventlist
#		self.nodes = []
#		self.run = env.process(start_job())

#	def start_job(self):
#		yield self.env.timeout(self.scheduleT) #schedule time
#		done = 0
#		barriers = []
		#now allocate compute nodes for simulation
#		for i in range(self.nodeCount):
#			barrier = env.event()
#			barriers.append(barrier)

#		for i in range(self.nodeCount):
#			self.nodes.append(env.process(compnode(self.env, i, self.events, barriers)))
		
		
			


#def compnode(env, myid, cdur, eventlist, barriers):

#	duration = 50
#	cpfreq = 20
#	numBBuse = 2
#	jobisze = 16
#	startT = env.now
#	elapsed = 0
#	remain = duration
#	jobIter = 0
#	while elapsed <= duration:
#		print('job iter %d' % jobIter)
#		print('cn %d compute at %f' %(myid, env.now))
#		yield env.timeout(cpfreq)

#		print('cn %d checkpoint at %f' %(myid, env.now))
#		wakeups = []
#		yields = []
#		for i in range(numBBuse):
#			wakeup = env.event()
#			wakeups.append(wakeup)
#			yields.append(eventlist[i].put(wakeup))
#
#		yield AllOf(env, yields)
#		yield AllOf(env, wakeups)
#		print('this node is done writing')
		#tell others that my node is done writing to bb
#		barriers[myid].succeed()
		#now wait for others to finish
#		for i in range(len(barriers)):
#			if i != myid:
#				yield barriers[i]

		#now reset barrier event for next use
#		barriers[myid] = env.event()
#		jobIter = jobIter + 1
		
#		if env.now - startT + cpfreq > duration:
#			cpfreq = env.now - startT + cpfreq - duration #this makes sure that the job will only run within duration

def bb(env, myid, requestQ, jobsize):
	reqid = 0
	bw = 6
	shareList = []
	shareList.append(0)

	ioreqs = dict()
	while True:
		curid = reqid
		msg = yield requestQ.get()
		event = msg[0]
		outsize = msg[1]
		print('bb %d got request %f with output size %f at time %f' %(myid, reqid, outsize, env.now))
		shareList[0] = shareList[0] + 1
		done = env.event()
		ioprocess = env.process(bbexe(env, myid, event, done, curid, outsize, bw, shareList))
		ioreqs[curid] = ioprocess
		#reqstats[curid] = 0

		## this section uses interrupt to inform other on-going IO requests that sharing is increased ##
		keys = ioreqs.keys()
		for k in keys:
			if k != curid:
				proc = ioreqs[k]
				if proc.is_alive:
					proc.interrupt('add')
					#print('telling bbexe %d increase degree of sharing new sharing %d at time %f' %(k, shareList[0], env.now))
		comp = env.process(complete(env, curid, done, ioreqs, shareList))
		reqid = reqid + 1


def complete(env, myid, done, ioreqs, shareList):
	yield done
	#print('request %d finished at %f' % (myid, env.now))
	shareList[0] = shareList[0] - 1
	keys = ioreqs.keys()
	for k in keys:
		if k != myid:
			proc = ioreqs[k]
			if proc.is_alive:
				proc.interrupt('done')
				#print('req %d done, telling %d at time %f new sharing degree %d' %(myid, k, env.now, shareList[0]))
	#now take the complete one out of dictionary
	del ioreqs[myid]

def bbexe(env, bbid, event, done, reqid, jobsize, bw, shareList):
	complete = 0
	startT = float(env.now)
	remainSize = float(jobsize)
	bandwidth = float(bw) / float(shareList[0])
	print('bb %d req %d start at time %f with sharing of %d' %(bbid, reqid, env.now, shareList[0]))
	remainT = float(jobsize) / float((bw/float(shareList[0])))
	print('bb %d req %d initial waittime %f'%(bbid, reqid, remainT))
	while complete == 0:
		try:
			yield env.timeout(remainT)
			complete = 1
		except simpy.Interrupt as i:
			msg = i.cause
			curT = float(env.now)
			if msg == 'done' and complete == 0:
				print('bb %d exeproc %d got to know some procs finished at time %f with new sharing degree %d' %(bbid, reqid, curT, shareList[0]))
				remainSize = calculate_remainSize(remainSize, curT, startT, bandwidth)
				print('bb %d exeproc %d remain job size %f' %(bbid, reqid, remainSize))
				remainT = calculate_remainT(remainSize, bw, shareList) #here must use bw, which is the full bandwidth to calculate the new bandwidth from the updated degree of sharing
				print('bb %d req %d new bw %f new waittime %f' % (bbid, reqid, bw/shareList[0], remainT))
				
			elif msg == 'add' and complete == 0:
				print('bb %d exeproc %d got to know increased sharing at time %f with new sharing degree %d' % (bbid, reqid, env.now, shareList[0]))
				remainSize = calculate_remainSize(remainSize, curT, startT, bandwidth)
				print('bb %d exeproc %d remain job size %f' %(bbid, reqid, remainSize))
				remainT = calculate_remainT(remainSize, bw, shareList)
				print('bb %d req %d new bw %f new waittime %f' % (bbid, reqid, bw/shareList[0],remainT))
			bandwidth = float(bw) / float(shareList[0])
			startT = curT
				
	event.succeed()
	done.succeed()
	print('bb %d req %d finished at %f' % (bbid, reqid, env.now))

def calculate_remainSize(jobsize, curT, startT, bw):
	duration = curT - startT
	remainSize = jobsize - (duration * bw)
	return remainSize
def calculate_remainT(remainSize, bw, shareList):
	remainT = float(remainSize) / float((bw/shareList[0]))
	return remainT

### start of simulation ###
numBB = 3
#numComp = 2
env = simpy.Environment()
reqQs = []
for i in range(numBB):
	reqQ = simpy.Store(env)
	reqQs.append(reqQ)
bbjobsize = 0.8*16 #12.8
bbNodes = []
for i in range(numBB):
	bbNode = BBNode(env, i, 6, reqQs[i])
	#bbNode = env.process(bb(env, i, reqQs[i], bbjobsize))

#for i in range(numComp):
#	env.process(compnode(env, i, 1, reqQs))

job1 = Job(env, reqQs,0)
#job2 = Job(env, reqQs,1)

env.run(until=100)
