import sys
import os
import math
import random

class Job:
	def __init__(self, jobType, jobID, pipeID, jobSize, jobLen, cpRatio):
		self.jobType = jobType
		self.jobID = jobID
		self.jobSize = jobSize
		self.jobLen = jobLen
		self.cpRatio = cpRatio
		self.pipeID = pipeID

random.seed(0)
totalNodes = float(sys.argv[1])
perNodeCores = float(sys.argv[2])
totalDuration = float(sys.argv[3]) * 24
archType = sys.argv[4]
jobSizeMultiple = float(sys.argv[5])
parity = int(sys.argv[6])
faultRate = int(sys.argv[7])
recoveryTime = int(sys.argv[8])
bbMapSize = int(sys.argv[9])
totalBBNodes = int(sys.argv[10])
totalCores = totalNodes * perNodeCores
jobs = []

### fixed stats ###
totalBBWriteBW = 10.25 * 1024 # GB/s
totalBBReadBW = 10.25 * 1024 # GB/s
if archType == "local" or archType == "localHybrid":
	totalBBWriteBW = (800.0 / 1024) * totalNodes # GB/s
	totalBBReadBW = (800.0 / 1024) * totalNodes # GB/s

if parity == 1:
	# assume 14+2
	totalBBWriteBW = totalBBWriteBW * 14.0/16.0
	totalBBReadBW = totalBBReadBW * 14.0/16.0
pfsBW = 1155.072 # GB/s
compNodeBW = 25 #GB/s
stageInPolicy = 0


### TOTAL DURATION PORTION ####
NGC_LAP_PORTION = 0.1
LAP_PORTION = 0.05
STON_PORTION = 0.05
NGC_EAP_PORTION = 0.4
EAP_PORTION = 0.3
VPIC_PORTION = 0.1

NORMAL_HRO_PERCENT = 0.5
NGC_HRO_PERCENT = 0.75

### CP RATIO ###
EAP_CP_RATIO = 0.1
LAP_CP_RATIO = 0.15
STON_CP_RATIO = 0.7
VPIC_CP_RATIO = 0.8
NGC_EAP_CP_RATIO = 0.75
NGC_LAP_CP_RATIO = 0.75


#### CIELO CORE STATS ####
CIELO_CORES = 131072.0
EAP_CIELO_AVG_CORES = 16384.0
EAP_CIELO_HRO_CORES = 65536.0
LAP_CIELO_AVG_CORES = 4096.0
LAP_CIELO_HRO_CORES = 32768.0
STON_CIELO_AVG_CORES = 32768.0
STON_CIELO_HRO_CORES = 131072.0
VPIC_CIELO_AVG_CORES = 30000.0
VPIC_CIELO_HRO_CORES = 70000.0
EAP_AVG_WALLTIME = 262.4
LAP_AVG_WALLTIME = 64.0
STON_AVG_WALLTIME = 128.0
VPIC_AVG_WALLTIME = 157.2

EAP_AVG_CORE_RATIO = EAP_CIELO_AVG_CORES / CIELO_CORES
EAP_HRO_CORE_RATIO = EAP_CIELO_HRO_CORES / CIELO_CORES
LAP_AVG_CORE_RATIO = LAP_CIELO_AVG_CORES / CIELO_CORES
LAP_HRO_CORE_RATIO = LAP_CIELO_HRO_CORES / CIELO_CORES
STON_AVG_CORE_RATIO = STON_CIELO_AVG_CORES / CIELO_CORES
STON_HRO_CORE_RATIO = STON_CIELO_HRO_CORES / CIELO_CORES
VPIC_AVG_CORE_RATIO = VPIC_CIELO_AVG_CORES / CIELO_CORES
VPIC_HRO_CORE_RATIO = VPIC_CIELO_HRO_CORES / CIELO_CORES



#### OTHER STATS ####
WALLTIME_RANGE_FACTOR = 1.1
JOBLEN = 24.0
NGC_EAP_PL = 10
NGC_LAP_PL = 20


#first compute per node memory
totalMem = 3 * 1024 * 1024 * 1.0 #GB
perNodeMem = totalMem/totalNodes
done = 0
perNodeMem =  math.pow(2, math.floor(math.log(perNodeMem)/math.log(2)))
while done == 0:
	approxTotalMem = perNodeMem*totalNodes/(1024*1024)
	if approxTotalMem >= 3 and approxTotalMem <= 3.2:
		print "perNodeMem %f total mem %f" % (perNodeMem, approxTotalMem)

		done = 1
	else:
		perNodeMem = perNodeMem + 16

### compute NGC_LAP ###
NGC_LAP_AVG_NODECNT = math.ceil(120*1.0*1024/perNodeMem)
NGC_LAP_HRO_NODECNT = math.ceil((256 * 1.0 * 1024/perNodeMem)/128) * 128
print "NGC_LA_AVG node cnt %d " % NGC_LAP_AVG_NODECNT
print "NGC_LA_HRO node cnt %d" % NGC_LAP_HRO_NODECNT

### compute NGC_EAP ###
NGC_EAP_AVG_NODECNT = math.ceil(1.0*1024*1024/perNodeMem)
NGC_EAP_HRO_NODECNT = math.ceil((1.5*1024*1024/perNodeMem)/128) * 128
print "NGC_EAP AVG node cnt %d" % NGC_EAP_AVG_NODECNT
print "NGC_EAP HRO node cnt %d" % NGC_EAP_HRO_NODECNT

### compute EAP ###
EAP_AVG_NODECNT = math.ceil(EAP_AVG_CORE_RATIO * totalCores / perNodeCores)
EAP_HRO_NODECNT = math.ceil((EAP_HRO_CORE_RATIO * totalCores / perNodeCores)/128) * 128
print "eap avg count %f eap hero cnt %f" % (EAP_AVG_NODECNT, EAP_HRO_NODECNT)

### compute LAP ###
LAP_AVG_NODECNT = math.ceil(LAP_AVG_CORE_RATIO * totalCores / perNodeCores)
LAP_HRO_NODECNT = math.ceil((LAP_HRO_CORE_RATIO * totalCores / perNodeCores)/128) * 128
print "lap avg cnt %f lap hro cnt %f" % (LAP_AVG_NODECNT, LAP_HRO_NODECNT)

### compute Silverton ###
STON_AVG_NODECNT = math.ceil(STON_AVG_CORE_RATIO * totalCores / perNodeCores)
STON_HRO_NODECNT = math.ceil((STON_HRO_CORE_RATIO * totalCores / perNodeCores)/128) * 128
print "ston avg cnt %f ston hero cnt %f" % (STON_AVG_NODECNT, STON_HRO_NODECNT)

### compute vpic ###
VPIC_AVG_NODECNT = math.ceil(VPIC_AVG_CORE_RATIO * totalCores / perNodeCores)
VPIC_HRO_NODECNT = math.ceil((VPIC_HRO_CORE_RATIO * totalCores / perNodeCores)/128) * 128
print "vpic avg cnt %f vpic hero cnt %f" % (VPIC_AVG_NODECNT, VPIC_HRO_NODECNT)

##### START ALLOCATING JOBS AND PIPELINES #####
pipeID = 0
jobID = 0




### allocate jobs for eap ###
jobType = "EAP"
eapTotalTime = totalDuration * EAP_PORTION
done = 0
eapRemainTime = eapTotalTime
### FIRST ALLOCATE HERO RUN ###
sigma = (EAP_AVG_WALLTIME * WALLTIME_RANGE_FACTOR - EAP_AVG_WALLTIME) / 3
jobNodeSigma = (EAP_AVG_NODECNT * WALLTIME_RANGE_FACTOR - EAP_AVG_NODECNT) /3
heroTotalPipeHours = NORMAL_HRO_PERCENT * eapRemainTime
print "eap set total time %f" % eapTotalTime
time = 0
while done == 0:
	heroPipeHours = random.gauss(EAP_AVG_WALLTIME, sigma)
	if heroTotalPipeHours - heroPipeHours <= 0:
		heroPipeHours = heroTotalPipeHours
		done = 1

	done1 = 0
	jobHoursRemain = heroPipeHours
	while done1 == 0:
		jobLen = JOBLEN
		if jobHoursRemain - JOBLEN <= 0:
			jobLen = jobHoursRemain
			done1 = 1
		job = Job(jobType, jobID, pipeID, EAP_HRO_NODECNT, jobLen, EAP_CP_RATIO)
		jobs.append(job)
		jobHoursRemain = jobHoursRemain - jobLen
		jobID = jobID + 1
		time  = time + jobLen
	heroTotalPipeHours = heroTotalPipeHours - heroPipeHours
	pipeID = pipeID + 1

print "eap hero time %f" % time
eapRemainTime = eapTotalTime - eapTotalTime * NORMAL_HRO_PERCENT
time = 0
done = 0
while done == 0:
	#first get pipeline time
	pipeHours = random.gauss(EAP_AVG_WALLTIME, sigma)
	if eapRemainTime - pipeHours <= 0:
		pipeHours = eapRemainTime
		done = 1
	done1 = 0
	jobHoursRemain = pipeHours
	while done1 == 0:
		#create job
		jobLen = JOBLEN
		if jobHoursRemain - JOBLEN <= 0:
			jobLen = jobHoursRemain
			done1 = 1
		nodeCnt = math.ceil((random.gauss(EAP_AVG_NODECNT, jobNodeSigma))/128) * 128
		job = Job(jobType, jobID, pipeID, nodeCnt, jobLen, EAP_CP_RATIO)
		jobs.append(job)
		jobID = jobID + 1
		jobHoursRemain = jobHoursRemain - jobLen
		time = time + jobLen
	eapRemainTime = eapRemainTime - pipeHours

	pipeID = pipeID + 1
print "eap normal time %f" % time

###allocate jobs for lap ###
jobType = "LAP"
lapTotalTime = totalDuration * LAP_PORTION
done = 0
lapRemainTime = lapTotalTime
heroTotalPipeHours = NORMAL_HRO_PERCENT * lapRemainTime
### HERO RUN FIRST
sigma = (LAP_AVG_WALLTIME * WALLTIME_RANGE_FACTOR - LAP_AVG_WALLTIME) / 3
jobNodeSigma = (LAP_AVG_NODECNT * WALLTIME_RANGE_FACTOR - LAP_AVG_NODECNT) / 3
while done == 0:
	heroPipeHours = random.gauss(LAP_AVG_WALLTIME, sigma)
	if heroTotalPipeHours - heroPipeHours <= 0:
		heroPipeHours = heroTotalPipeHours
		done = 1

	done1 = 0

	jobHoursRemain = heroPipeHours
	while done1 == 0:
		jobLen = JOBLEN
		if jobHoursRemain - JOBLEN <= 0:
			jobLen = jobHoursRemain
			done1 = 1
		job = Job(jobType, jobID, pipeID, LAP_HRO_NODECNT, jobLen, LAP_CP_RATIO)
		jobs.append(job)
		jobHoursRemain = jobHoursRemain - jobLen
		jobID = jobID + 1

	heroTotalPipeHours = heroTotalPipeHours - heroPipeHours
	pipeID = pipeID + 1

lapRemainTime = lapTotalTime - lapTotalTime * NORMAL_HRO_PERCENT
done = 0

while done == 0:
	pipeHours = random.gauss(LAP_AVG_WALLTIME, sigma)
	if lapRemainTime - pipeHours <= 0:
		pipeHours = lapRemainTime
		done = 1
	done1 = 0
	jobHoursRemain = pipeHours
	while done1 == 0:
		jobLen = JOBLEN
		if jobHoursRemain - JOBLEN <= 0:
			jobLen = jobHoursRemain
			done1 = 1
		nodeCnt = math.ceil((random.gauss(LAP_AVG_NODECNT, jobNodeSigma))/128) * 128
		job = Job(jobType, jobID, pipeID, nodeCnt, jobLen, LAP_CP_RATIO)
		jobs.append(job)
		jobID = jobID + 1
		jobHoursRemain = jobHoursRemain - jobLen

	lapRemainTime = lapRemainTime - pipeHours
	pipeID = pipeID + 1

###allocate jobs for silverton ###
jobType = "STON"
stonTotalTime = totalDuration * STON_PORTION
done = 0
stonRemainTime = stonTotalTime
heroTotalPipeHours = stonRemainTime * NORMAL_HRO_PERCENT
### HERO RUN FIRST
sigma = (STON_AVG_WALLTIME * WALLTIME_RANGE_FACTOR - STON_AVG_WALLTIME) / 3
jobNodeSigma = (STON_AVG_NODECNT * WALLTIME_RANGE_FACTOR - STON_AVG_NODECNT) / 3

while done == 0:
	heroPipeHours = random.gauss(STON_AVG_WALLTIME, sigma)
	if heroTotalPipeHours - heroPipeHours <= 0:
		heroPipeHours = heroTotalPipeHours
		done = 1

	done1 = 0
	jobHoursRemain = heroPipeHours
	while done1 == 0:
		jobLen = JOBLEN
		if jobHoursRemain - JOBLEN <= 0:
			jobLen = jobHoursRemain
			done1 = 1
		job = Job(jobType, jobID, pipeID, STON_HRO_NODECNT, jobLen, STON_CP_RATIO)
		jobs.append(job)
		jobHoursRemain = jobHoursRemain - jobLen
		jobID = jobID + 1
	heroTotalPipeHours = heroTotalPipeHours - heroPipeHours
	pipeID = pipeID + 1

stonRemainTime = stonRemainTime - stonTotalTime * NORMAL_HRO_PERCENT
done = 0

while done == 0:
	pipeHours = random.gauss(STON_AVG_WALLTIME, sigma)
	if stonRemainTime - pipeHours <= 0:
		pipeHours = stonRemainTime
		done = 1
	done1 = 0
	jobHoursRemain = pipeHours
	while done1 == 0:
		jobLen = JOBLEN
		if jobHoursRemain - JOBLEN <= 0:
			jobLen = jobHoursRemain
			done1 = 1
		nodeCnt = math.ceil((random.gauss(STON_AVG_NODECNT, jobNodeSigma))/128) * 128
		job = Job(jobType, jobID, pipeID, nodeCnt, jobLen, STON_CP_RATIO)
		jobs.append(job)
		jobID = jobID + 1
		jobHoursRemain = jobHoursRemain - jobLen

	stonRemainTime = stonRemainTime - pipeHours
	pipeID = pipeID + 1

### allocate jobs for vpic ###
jobType = "VPIC"
vpicTotalTime = totalDuration * VPIC_PORTION
done = 0
vpicRemainTime = vpicTotalTime
heroTotalPipeHours = vpicRemainTime * NORMAL_HRO_PERCENT
### HERO RUN FIRST
sigma = (VPIC_AVG_WALLTIME * WALLTIME_RANGE_FACTOR - VPIC_AVG_WALLTIME) / 3
jobNodeSigma = (VPIC_AVG_NODECNT * WALLTIME_RANGE_FACTOR - VPIC_AVG_NODECNT) / 3
print "vpic total time %f" % vpicTotalTime
time = 0
while done == 0:
	print "remain hero total hours %f" % heroTotalPipeHours
	heroPipeHours = random.gauss(VPIC_AVG_WALLTIME, sigma)
	if heroTotalPipeHours - heroPipeHours <= 0:
		heroPipeHours = heroTotalPipeHours
		done = 1
	done1 = 0
	jobHoursRemain = heroPipeHours
	while done1 == 0:
		jobLen = JOBLEN
		if jobHoursRemain - JOBLEN <= 0:
			jobLen = jobHoursRemain
			done1 = 1
		job = Job(jobType, jobID, pipeID, VPIC_HRO_NODECNT, jobLen, VPIC_CP_RATIO)
		jobs.append(job)
		jobHoursRemain = jobHoursRemain - jobLen
		jobID = jobID + 1
		time = time+jobLen
	heroTotalPipeHours = heroTotalPipeHours - heroPipeHours
	pipeID = pipeID + 1
vpicRemainTime = vpicTotalTime - vpicTotalTime * NORMAL_HRO_PERCENT
print "vpic remain time %f" % vpicRemainTime
done = 0

while done == 0:
	pipeHours = random.gauss(VPIC_AVG_WALLTIME, sigma)
	if vpicRemainTime - pipeHours <= 0:
		pipeHours = vpicRemainTime
		done = 1

	done1 = 0
	jobHoursRemain = pipeHours
	while done1 == 0:
		jobLen = JOBLEN
		if jobHoursRemain - JOBLEN <= 0:
			jobLen = jobHoursRemain
			done1 = 1
		nodeCnt = math.ceil((random.gauss(VPIC_AVG_NODECNT, jobNodeSigma))/128) * 128
		job = Job(jobType, jobID, pipeID, nodeCnt, jobLen, VPIC_CP_RATIO)
		jobs.append(job)
		jobID = jobID + 1
		jobHoursRemain = jobHoursRemain - jobLen
		time = time+ jobLen
	vpicRemainTime = vpicRemainTime - pipeHours
	pipeID = pipeID + 1
print "vpic real time %f" % time

###allocate ngc-eap ###
jobType = "NEAP"
neapTotalTime = totalDuration * NGC_EAP_PORTION
done = 0
neapRemainTime = neapTotalTime
heroTotalPipeHours = neapRemainTime * NGC_HRO_PERCENT
NGC_EAP_AVG_WALLTIME = neapTotalTime / NGC_EAP_PL
print "NGCEAP AVG wall %f" % NGC_EAP_AVG_WALLTIME
### HERO RUN FIRST
sigma = (NGC_EAP_AVG_WALLTIME * WALLTIME_RANGE_FACTOR - NGC_EAP_AVG_WALLTIME) / 3
jobNodeSigma = (NGC_EAP_AVG_NODECNT * WALLTIME_RANGE_FACTOR - NGC_EAP_AVG_NODECNT) / 3

for i in range(0, NGC_EAP_PL):
	if i < math.floor(NGC_HRO_PERCENT * NGC_EAP_PL):
		#hero pipelines
		heroPipeHours = random.gauss(NGC_EAP_AVG_WALLTIME, sigma)
		if heroTotalPipeHours - heroPipeHours <= 0:
			heroPipeHours = heroTotalPipeHours
			done = 1
		done1 = 0
		#print "hero pipe hours %f" % heroPipeHours
		jobHoursRemain = heroPipeHours
		while done1 == 0:
			jobLen = JOBLEN
			if jobHoursRemain - JOBLEN <= 0:
				jobLen = jobHoursRemain
				done1 = 1
			job = Job(jobType, jobID, pipeID, NGC_EAP_HRO_NODECNT, jobLen, NGC_EAP_CP_RATIO)
			jobs.append(job)
			jobID = jobID + 1
			jobHoursRemain = jobHoursRemain - jobLen

		heroTotalPipeHours = heroTotalPipeHours - heroPipeHours
		pipeID = pipeID + 1
		if i == math.ceil(NGC_HRO_PERCENT * NGC_EAP_PL) - 1:
			neapRemainTime = neapRemainTime - neapTotalTime * NGC_HRO_PERCENT
	else:
		pipeHours = random.gauss(NGC_EAP_AVG_WALLTIME, sigma)
		if neapRemainTime - pipeHours <= 0:
			pipeHours = neapRemainHours
		#print "regular pipe hours %f" % pipeHours
		done1 = 0
		jobHoursRemain = pipeHours
		while done1 == 0:
			jobLen = JOBLEN
			if jobHoursRemain - JOBLEN <= 0:
				jobLen = jobHoursRemain
				done1 = 1
			nodeCnt = math.ceil((random.gauss(NGC_EAP_AVG_NODECNT, jobNodeSigma))/128) * 128
			job = Job(jobType, jobID, pipeID, nodeCnt, jobLen, NGC_EAP_CP_RATIO)
			jobs.append(job)
			jobID = jobID + 1
			jobHoursRemain = jobHoursRemain - jobLen
		pipeID = pipeID + 1
		

### allocate ngc-lap ###
jobType = "NLAP"
nlapTotalTime = totalDuration * NGC_LAP_PORTION
done = 0
nlapRemainTime = nlapTotalTime
heroTotalPipeHours = nlapRemainTime * NGC_HRO_PERCENT
NGC_LAP_AVG_WALLTIME = nlapTotalTime / NGC_LAP_PL
print "ngc lap avg wall %f" % NGC_LAP_AVG_WALLTIME
### HERO RUN FIRST
sigma = (NGC_LAP_AVG_WALLTIME * WALLTIME_RANGE_FACTOR - NGC_LAP_AVG_WALLTIME) / 3
jobNodeSigma = (NGC_LAP_AVG_NODECNT * WALLTIME_RANGE_FACTOR - NGC_LAP_AVG_NODECNT) / 3
for i in range(0, NGC_LAP_PL):
        if i < math.floor(NGC_HRO_PERCENT * NGC_LAP_PL):
                #hero pipelines
                heroPipeHours = random.gauss(NGC_LAP_AVG_WALLTIME, sigma)
                if heroTotalPipeHours - heroPipeHours <= 0:
                        heroPipeHours = heroTotalPipeHours
                        done = 1
                done1 = 0
                #print "hero pipe hours %f" % heroPipeHours
                jobHoursRemain = heroPipeHours
                while done1 == 0:
			jobLen = JOBLEN
                        if jobHoursRemain - JOBLEN <= 0:
				jobLen = jobHoursRemain
                                done1 = 1
                        job = Job(jobType, jobID, pipeID, NGC_LAP_HRO_NODECNT, jobLen, NGC_LAP_CP_RATIO)
                        jobs.append(job)
                        jobID = jobID + 1
                        jobHoursRemain = jobHoursRemain - jobLen

                heroTotalPipeHours = heroTotalPipeHours - heroPipeHours
                pipeID = pipeID + 1
                if i == math.ceil(NGC_HRO_PERCENT * NGC_LAP_PL) - 1:
                        nlapRemainTime = nlapRemainTime - nlapTotalTime * NGC_HRO_PERCENT
        else:
                pipeHours = random.gauss(NGC_LAP_AVG_WALLTIME, sigma)
                if nlapRemainTime - pipeHours <= 0:
                        pipeHours = nlapRemainHours
                #print "regular pipe hours %f" % pipeHours
                done1 = 0
                jobHoursRemain = pipeHours
                while done1 == 0:
			jobLen = JOBLEN
                        if jobHoursRemain - JOBLEN <= 0:
				jobLen = jobHoursRemain
                                done1 = 1
                        nodeCnt = math.ceil((random.gauss(NGC_LAP_AVG_NODECNT, jobNodeSigma))/128)* 128
                        job = Job(jobType, jobID, pipeID, nodeCnt, jobLen, NGC_LAP_CP_RATIO)
                        jobs.append(job)
                        jobID = jobID + 1
                        jobHoursRemain = jobHoursRemain - jobLen
                pipeID = pipeID + 1



eapTime = 0
lapTime = 0
stonTime = 0
vpicTime = 0
neapTime = 0
nlapTime = 0
print "total pipeLines %d" % pipeID
print "checking job size correctness: MUST BE MULTIPLES OF 128"
for job in jobs:
	if job.jobType == "EAP":
		#print "job type %s" % job.jobType
		#print "pipe id %d" % job.pipeID
		#print "job id %d" % job.jobID
		#print "job node cnt %d" % job.jobSize
		#print "job len %d" % job.jobLen
		#print "###"
		eapTime = eapTime + job.jobLen
	elif job.jobType == "LAP":
		lapTime = lapTime + job.jobLen
	elif job.jobType == "STON":
		stonTime = stonTime + job.jobLen
	elif job.jobType == "VPIC":
		vpicTime = vpicTime + job.jobLen
	elif job.jobType == "NEAP":
		neapTime = neapTime + job.jobLen
	elif job.jobType == "NLAP":
		nlapTime = nlapTime + job.jobLen
realTime = eapTime + lapTime + stonTime + vpicTime + neapTime + nlapTime
print "total eap time %f; total set eap time %f, generated percent %f, set percent %f" % (eapTime, eapTotalTime, eapTime/realTime, EAP_PORTION)
print "total lap time %f; total set lap time %f, generated percent %f, set percent %f" % (lapTime, lapTotalTime, lapTime/realTime, LAP_PORTION)
print "total ston time %f; total set ston time %f, generated percent %f, set percent %f" % (stonTime, stonTotalTime, stonTime/realTime, STON_PORTION)
print "total vpic time %f; total set vpic time %f, generated percent %f, set percent %f" % (vpicTime, vpicTotalTime, vpicTime/realTime, VPIC_PORTION)
print "total neap time %f; total set neap time %f, generated percent %f, set percent %f" % (neapTime, neapTotalTime, neapTime/realTime, NGC_EAP_PORTION)
print "total nlap time %f; total set nlap time %f, generated percent %f, set percent %f" % (nlapTime, nlapTotalTime, nlapTime/realTime, NGC_LAP_PORTION)
print "generated total duration %f; set total duration %f" % (realTime, totalDuration)


outName = None
totalNodes = int(totalNodes)

if parity == 1 and archType == "shared":
	outName = archType+"_"+str(totalNodes)+"_"+"PerNodeCores"+str(perNodeCores)+"_"+"parityON"+"_"+"recoveryHour"+str(recoveryTime) + "_" + "faultRate_"+str(faultRate)
elif parity == 0 and archType == "shared":
	outName = archType + "_"+str(totalNodes)+"_"+"PerNodeCores"+str(perNodeCores)+"_"+"parityOFF"+"_"+"recoveryHour"+str(recoveryTime)+ "_" + "faultRate_"+str(faultRate)

elif parity == 1 and archType == "sharedPartition":
	outName = archType+"_"+str(totalNodes)+"_"+"PerNodeCores"+str(perNodeCores)+"_"+"bbPartitionSize"+str(bbMapSize)+"_"+"parityON"+"_"+"recoveryHour"+str(recoveryTime)+ "_" + "faultRate_"+str(faultRate)+"totalBB"+str(totalBBNodes/bbMapSize)+"_"+"BBMapSize"+str(bbMapSize)

elif parity == 0 and archType == "sharedPartition":
	outName = archType+"_"+str(totalNodes)+"_"+"PerNodeCores"+str(perNodeCores)+"_"+"bbPartitionSize"+str(bbMapSize)+"_"+"parityOFF"+"_"+"recoveryHour"+str(recoveryTime)+ "_" + "faultRate_"+str(faultRate)+"totalBB"+str(totalBBNodes/bbMapSize)+"_"+"BBMapSize"+str(bbMapSize)

elif parity == 1 and (archType == "local" or archType == "localHybrid"):
	outName = archType+"_"+str(totalNodes)+"_"+"PerNodeCores"+str(perNodeCores)+"_"+"parityON"+"_"+"recoveryHour"+str(recoveryTime)+ "_" + "faultRate_"+str(faultRate)

elif parity == 0 and (archType == "local" or archType == "localHybrid"):
	outName = archType + "_"+str(totalNodes)+"_"+"PerNodeCores"+str(perNodeCores)+"_"+"parityOFF"+"_"+"recoveryHour"+str(recoveryTime)+ "_" + "faultRate_"+str(faultRate)


print "output file Name %s" % outName
outFD = open(outName, "w")
###first write common parameters###
outFD.write("%s\n" % outName)
outFD.write("%s\n" % archType)
outFD.write("faulRate(Hours):%d\n" % faultRate)
outFD.write("nodeRecoveryTime(hour):%d\n" % recoveryTime)
outFD.write("stageInPolicy:%d\n" % stageInPolicy)
outFD.write("parity:%d\n" % parity)
outFD.write("PFSBandwidth(GB/s):%f\n" % pfsBW)
outFD.write("bbCapacity(PB):%d\n" % 100)
outFD.write("bbTotalWrite(GB/s):%f\n" % totalBBWriteBW)
outFD.write("bbTotalRead(GB/s):%f\n" % totalBBReadBW)
outFD.write("totalCompNodes:%d\n" % totalNodes)
outFD.write("compNodeMem(GB):%f\n" % perNodeMem)
outFD.write("compNodeBW(GB/s):%f\n" % compNodeBW)
if archType == "shared":
	outFD.write("totalBBNode:%d\n" % totalBBNodes)
elif archType == "sharedPartition":
	totalBBNodes = totalNodes / bbMapSize
	print "bb map size %d" % bbMapSize
	outFD.write("totalBBNode:%d\n" % totalBBNodes)
	outFD.write("bbPartSize:%d\n" % bbMapSize)
outFD.write("************\n")

counter = 0
random.shuffle(jobs)
for job in jobs:
	outFD.write("job %d\n" % counter)
	outFD.write("%s\n" % job.jobType)
	outFD.write("pipeID:%d\n" % job.pipeID)
	outFD.write("jobID:%d\n" % job.jobID)
	outFD.write("jobLen:%f\n" % job.jobLen)
	outFD.write("jobNodeCount:%d\n" % job.jobSize)
	outFD.write("chkpRatio:%f\n" % job.cpRatio)
	counter = counter + 1
outFD.write("END\n")
