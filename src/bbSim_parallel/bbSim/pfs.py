from SimianPie.simian import Simian
from SimianPie.entity import Entity

class Pfs(Entity):
	def __init__(self, baseInfo, *args):
		super(Pfs, self).__init__(baseInfo)
		#self.engine = args[0]
		self.scheduler = args[1]
		self.parity = args[2]
		self.parityOverhead = args[3]
		self.PFSBandwidth = args[4]
		self.compNodeBw = args[5]
		self.localWriteBw = args[6]
		self.localReadBw = args[7]
		self.wakeupArgs = [-99, None]
		self.shareCnt = 0
		self.jobToExecuteMap = dict()

	### Main Process ###
	def pfsExecutor_(self, this, procName, jobID, reqType, compNodes, bbNodes, reqSize):
		print "pfs start executing reqtype %d reqf for job %d share cnt %d" % (reqType, jobID, self.shareCnt)
		retval = 0
		length = len(compNodes)
		bbLength = len(bbNodes)
		myBW = self.PFSBandwidth / self.shareCnt
		bbReqMap = dict()
		for i in range(0, bbLength):
			#group reqs from different node to the same BB into 1 req to that bb
			bbReqSize = reqSize * length
			bbName = "bb"+str(bbNodes[i])
			reqName = None
			bbReqType = None
			if reqType == 0 or reqType == 6:
				reqName = "stageIn_"+str(jobID)+"_"+str(bbNodes[i])
				bbReqType = 2
			elif reqType == 1 or reqType == 7:
				reqName = "stageOut_"+str(jobID)+"_"+str(bbNodes[i])
				bbReqType = 3

			bbReqMap[reqName] = bbNodes[i]
			args = []
			args.append(bbReqType)
			args.append(jobID)
			args.append(len(compNodes))
			args.append(reqSize)
			args.append(myBW)
			args.append(reqName)
			print "pfs exe for job %d sending bbReq to bb %d" % (jobID, bbNodes[i])
			self.reqService(self.engine.minDelay, "bbRequest", args, bbName, bbNodes[i])
			
		done = bbLength
		print "total done %d" % done
		while done != 0:
			this.hibernate(*(self.wakeupArgs))
			if self.wakeupArgs[0] >= 0:
				print "pfs executor for job %d got bb req from bb %d done at %f" % (jobID, self.wakeupArgs[0], self.engine.now)
				done = done - 1
				self.shareCnt = self.shareCnt - 1
				retval = done
				#inform other
				bbID = self.wakeupArgs[0]
				jobID = self.wakeupArgs[1][0]
				targetReqName = None
				if reqType == 0 or reqType == 6:
					targetReqName = "stageIn_"+str(jobID)+"_"+str(bbNodes[i])
				elif reqType == 1 or reqType == 7:
					targetReqName = "stageOut_"+str(jobID)+"_"+str(bbNodes[i])

				#No NEED TO update bandwidth to other bbs because
				#1- if it is shared, since each node is stripe across everything,
				#all requests would finish at the same time. we only need to update bandwidth to others
				#when every requests on this executor is done, which is at the same time
				#2- if it is shared partitioned, every request is local.
				#if self.shareCnt != 0 and (reqType == 0 or reqType == 1):
					#ALWAYS NEED TO UPDATE NEW BW FOR PURELY SHARED ARCH
				#	print "pfs job %d sending update bandwidht to other executors" % jobID
				#	self.updatePfsBwToBBs(targetReqName, jobID, bbReqMap)
				#now remove this key from bbReqMap
				bbReqMap.pop(targetReqName, None)
			elif self.wakeupArgs[0] == -99:
				#job timer off, killed off
				retval = -1
				break
			elif self.wakeupArgs[0] == -2:
				#update bandwidth
				print "job %d wakeup due to share count change by one bb of its own finished, updating new bw" % jobID
				if self.shareCnt != 0:
					self.updatePfsBwToBBs(None, jobID, bbReqMap)
			elif self.wakeupArgs[0] == -1:
				#job gets a bb fault
				print "pfs sending cancel message to all BBs"
				#if reqType < 2:
				#	self.cancelBBRequests(jobID, compNodes, bbNodes)
				#if reqType >= 6:
				self.cancelingSharedPartitionBBreqs(jobID, compNodes, bbReqMap)
				retval = -2
				break
			elif self.wakeupArgs[0] == -3:
				#job gets a bb fault but has parity on
				### NEED TO REWRITE THIS PART!!
				bbVictim = self.wakeupArgs[1]
				self.cancelSingleBBNodeRequests(jobID, compNodes, bbNodes, bbVictim)
				done = done - 1
				self.shareCnt = self.shareCnt - 1
				retval = done
				bbID = bbVictim
				targetReqName = None
				if reqType == 0 or reqType == 6:
					targetReqName = "stageIn_"+str(jobID)+"_"+str(bbID)
				elif reqType == 1 or reqType == 7:
					targetReqName = "stageOut_"+str(jobID)+"_"+str(bbID)
				
				if self.shareCnt != 0:
					print "pfs job %d sneding update bandwidht to bbs due to 1 bb failed but has parity on" % jobID
					self.updatePfsBwToBBs(targetReqName, jobID, bbReqMap)
				bbReqMap.pop(targetReqName, None)
			elif self.wakeupArgs[0] == -4:
				#there is a new executor, change of share cnt
				print "job %d wakeup to change bandwidht due to some other job" % jobID
				self.updatePfsBwToBBs(None, None, bbReqMap)

		#remove proc from map first
		if jobID in self.jobToExecuteMap:
			self.jobToExecuteMap.pop(jobID)

		if self.shareCnt != 0 and (reqType == 6 or reqType == 7):
			#batch update bandwidth change
			print "job %d BATCH UPDATING BANDWIDTH" % jobID
			self.batchUpdateBWToExecutors()
		print "pfs executor for job %d finished at %f" % (jobID, self.engine.now)
		if retval == 0:
			print "pfs executor for job %d sending notification of done with share cnt %d" % (jobID, self.shareCnt)
			self.reqService(self.engine.minDelay, "pfsNotify", retval, "job"+str(jobID), jobID)

	def pfsExecutor(self, this, procName, jobID, reqType, compNodes, bbNodes, reqSize, totalBBReqCnt):
		print "pfs start executing reqtype %d reqf for job %d share cnt %d" % (reqType, jobID, self.shareCnt)
		print "reqSize %f" % reqSize
		retval = 0
		length = len(compNodes)
		myBW = self.PFSBandwidth / self.shareCnt
		bbReqMap = dict()
		#issue bb requests
		for i in range(0, length):
			if reqType < 2:
				bbs = bbNodes[i]
				for bb in bbs:
					bbName = "bb"+str(bb)
					reqName = None
					bbReqType = None
					if reqType == 0:
						reqName = "stageIn_"+str(jobID)+"_"+str(compNodes[i])+"_"+str(bb)
						bbReqType = 2
					elif reqType == 1:
						reqName = "stageOut_"+str(jobID)+"_"+str(compNodes[i])+"_"+str(bb)
						bbReqType = 3
					bbReqMap[reqName] = bb
					args = []
					args.append(bbReqType)
					args.append(jobID)
					args.append(compNodes[i])
					args.append(reqSize)
					args.append(myBW)
					args.append(reqName)
					print "pfs exe for job %d sending bbrewq to bb %d" % (jobID, bb)
					self.reqService(self.engine.minDelay, "bbRequest", args, bbName, bb)

			elif reqType >= 6:
				#shared partitioned
				for bb in bbNodes:
					bbName = "bb" + str(bb)
					reqName = None
					bbReqType = None
					if reqType == 6:
						reqName = "stageIn_"+str(jobID)+"_"+str(compNodes[i])+"_"+str(bb)
						bbReqType = 2
					elif reqType == 7:
						reqName = "stageOut_"+str(jobID)+"_"+str(compNodes[i])+"_"+str(bb)
						bbReqType = 3
					bbReqMap[reqName] = bb
					args = []
					args.append(bbReqType)
					args.append(jobID)
					args.append(compNodes[i])
					args.append(reqSize)
					args.append(myBW)
					args.append(reqName)
					print "pfs exe for job %d sending bbreq from node %d to bb %d" % (jobID, i, bb)
					self.reqService(self.engine.minDelay, "bbRequest", args, bbName, bb)

		#wait for every single bb req to finish
		done = totalBBReqCnt
		print "total done %d" % done
		while done != 0:
			this.hibernate(*(self.wakeupArgs))
			if self.wakeupArgs[0] >= 0:
				print "pfs executor for job %d got 1 bb req done" % jobID
				done = done - 1
				self.shareCnt = self.shareCnt - 1
				retval = done
				#Inform other 
				bbID = self.wakeupArgs[0]
				jobID = self.wakeupArgs[1][0]
				nodeID = self.wakeupArgs[1][1]
				targetReqName = None
				if reqType == 0 or reqType == 6:
					targetReqName = "stageIn_"+str(jobID)+"_"+str(nodeID)+"_"+str(bbID)
				elif reqType == 1 or reqType == 7:
					targetReqName = "stageOut_"+str(jobID)+"_"+str(nodeID)+"_"+str(bbID)
				if self.shareCnt != 0:
					print "pfs job %d sending update bb" % jobID
					self.updatePfsBwToBBs(targetReqName, jobID, bbReqMap)
				bbReqMap.pop(targetReqName, None)
			elif self.wakeupArgs[0] == -99:
				#job timer off, killed off
				retval = -1
				break
			elif self.wakeupArgs[0] == -2:
				#update bandwidth
				if self.shareCnt != 0:
					self.updatePfsBwToBBs(None, jobID,  bbReqMap)
			elif self.wakeupArgs[0] == -1:
				#job gets a bb fault
				print "pfs sending cancel message to all BBes"
				if reqType < 2:
					self.cancelBBRequests(jobID, bbReqMap)
				elif reqType >= 6:
					self.cancelingSharedPartitionBBreqs(jobID, compNodes, bbNodes)
				retval = -2
				break
			elif self.wakeupArgs[0] == -3:
				#job gets a bb fault but has parity on
				bbVictim = self.wakeupArgs[1]
				if reqType < 2:
					self.cancelSingleBBNodeRequests(jobID, compNodes, bbNodes, bbVictim)
				elif reqType >= 6:
					self.cancelingSharedPartitionBBreqs(jobID, compNodes, bbNodes, bbVictim)
				done = done - 1
			elif self.wakeupArgs[0] == -4:
				#there is a new executor, change of share cnt
				if self.shareCnt != 0:
					self.updatePfsBwToBBs(None, None, bbReqMap)
		#remove proc from map first
		if jobID in self.jobToExecuteMap:
			self.jobToExecuteMap.pop(jobID)
		print "pfs executor for job %d finished at %f" % (jobID, self.engine.now)

		#now send done signal to job
		if retval == 0:
			print "pfs executor for job %d sending notification of done" % jobID
			print "^^^^pfs share cnt %d" % self.shareCnt
			self.reqService(self.engine.minDelay, "pfsNotify", retval, "job"+str(jobID), jobID)

	def pfsExecutor_hybrid(self, this, procName, jobID, reqType, hybridNodes, reqSize, jobNodeCnt):
		retval = 0
		length = len(hybridNodes)
		myBW = self.PFSBandwidth / self.shareCnt
		hybridReqMap = dict()
		for i in range(0, length):
			reqName = None
			nodeName = "hybrid"+str(hybridNodes[i])
			if reqType == 2:
				#stagein
				reqName = "stageIn_"+str(jobID)+"_"+str(hybridNodes[i])
				stageType = 0
			elif reqType == 3:
				#stageout
				reqName = "stageOut_"+str(jobID)+"_"+str(hybridNodes[i])
				stageType = 1
			hybridReqMap[reqName] = hybridNodes[i]
			args = []
			args.append(stageType)
			args.append(jobID)
			args.append(myBW)
			args.append(reqName)
			args.append(jobNodeCnt)
			args.append(reqSize)
			if reqType == 2:
				self.reqService(self.engine.minDelay, "stageInReq", args, nodeName, hybridNodes[i])
			elif reqType == 3:
				self.reqService(self.engine.minDelay, "stageOutReq", args, nodeName, hybridNodes[i])

		
		print "pfs sent hybrid io req to hybrid nodes for job %d" % (jobID)
		done = jobNodeCnt
		while done != 0:
			this.hibernate(*(self.wakeupArgs))
			if self.wakeupArgs[0] >= 0:
				done = done - 1
			elif self.wakeupArgs[0] == -1:
				print "pfs executo sending job %d cancel msgs to nodes" % jobID
				self.cancelHybridPfsRequests(jobID, reqType, hybridNodes)	
				retval = -1
				break
			elif self.wakeupArgs[0] == -4:
				#there is a change in share cnt
				print "PFS EXECUTOR %s got share cnt change msg" % procName
				self.updateBwToNodes(jobID, hybridNodes, reqType)

		
		self.shareCnt = self.shareCnt - jobNodeCnt
		if self.shareCnt != 0:
			self.notifyShareChange(jobID)
		#remove proc from map
		if jobID in self.jobToExecuteMap:
			self.jobToExecuteMap.pop(jobID)

		print "pfs executor for job %d finished at %f" % (jobID, self.engine.now)
		if retval == 0:
			self.reqService(self.engine.minDelay, "pfsNotifyHybrid", retval, "job"+str(jobID), jobID)
		print "pfs share cnt after executor ends %d" % self.shareCnt

	def pfsExecutor_local(self, this, procName, jobID, reqType, localNodes, reqSize, jobNodeCnt):
		retval = 0
		length = len(localNodes)
		myBw = self.PFSBandwidth / self.shareCnt
		#for pure local architecture, there is no need to process on local nodes
		#because there is no interference. we can just let the pfs wait out the io time here
		reqType1 = reqType - 4
		bundle = self.computeIOTime(reqType1, reqSize, myBw)
		remain = reqSize
		start = self.engine.now
		ioTime = bundle[0]
		currentBw = bundle[1]
		print "req %s start bw %f, ioTime %f remain %f" % (procName, myBw, ioTime, reqSize)
		timeStamp = self.engine.now + ioTime + self.engine.minDelay
		self.reqService(ioTime + self.engine.minDelay, "wakeupFromIO", procName, "pfs", 0)
		print "req %s wakingup at %f" % (procName, timeStamp)
		done = 0
		while done == 0:
			this.hibernate(*(self.wakeupArgs))
			if self.wakeupArgs[0] == 0:
				#wake from io timer
				currentTime = self.engine.now
				if abs(currentTime - timeStamp) <= self.engine.minDelay:
					print "req %s finished" % procName
					done = 1
				else:
					print "req %s wakeup from older timer, VOID. currentTime %f timestamp %f" % (procName, currentTime, timeStamp)

			elif self.wakeupArgs[0] == -1:
				#there is a fault, cancel
				print "###pfs req %s canceling" % procName
				done = 2
				retval = -1

			elif self.wakeupArgs[0] == -4:
				#there is a share count change, compute new io time
				bundle = self.computeNewIOTime(reqType1, currentBw, start, remain)
				ioTime = bundle[0]
				currentBw = bundle[1]
				remain = bundle[2]
				start = self.engine.now
				timeStamp = self.engine.now + ioTime + self.engine.minDelay
				print "req %s got change share cnt msg. new Bw %f new IO Time %f new remain %f wakingup at %f" % (procName, currentBw, ioTime, remain, timeStamp)
				self.reqService(self.engine.minDelay + ioTime, "wakeupFromIO", procName, "pfs", 0)

		#finished
		self.shareCnt = self.shareCnt - jobNodeCnt
		if self.shareCnt != 0:
			self.notifyShareChange(jobID)
		#remove proc from map
		if jobID in self.jobToExecuteMap:
			self.jobToExecuteMap.pop(jobID)

		if retval == 0:
			self.reqService(self.engine.minDelay, "pfsNotifyLocal", retval, "job"+str(jobID), jobID)
		
			
	### Async. Event Handlers ###
	def pfsRequest(self, data, tx, txID):
		reqType = data[0]
		jobID = data[1]
		if reqType == 0:
			#stage in
			compNodes = data[2]
			bbNodes = data[3]
			reqSize = data[4]
			totalBBReqCnt = data[5]
			self.shareCnt = self.shareCnt + len(bbNodes)
			procName = "pfsReq_"+str(jobID)
			self.jobToExecuteMap[jobID] = procName
			#first need to notify other on going executors that there is a change in sharing
			self.notifyShareChange(jobID)
			self.createProcess(procName, self.pfsExecutor_)
			self.startProcess(procName, procName, jobID, reqType, compNodes, bbNodes, reqSize, totalBBReqCnt)

		elif reqType == 1:
			#stageout
			compNodes = data[2]
			bbNodes = data[3]
			reqSize = data[4]
			totalBBReqCnt = data[5]
			self.shareCnt = self.shareCnt + totalBBReqCnt
			procName = "pfsReq_"+str(jobID)
			self.jobToExecuteMap[jobID] = procName
			self.notifyShareChange(jobID)
			self.createProcess(procName, self.pfsExecutor)
			self.startProcess(procName, procName, jobID, reqType, compNodes, bbNodes, reqSize, totalBBReqCnt)

		elif reqType == 2 or reqType == 3:
			#hybrid arch stagein
			hybridNodes = data[2]
			reqSize = data[3]
			jobNodeCnt = data[4]
			self.shareCnt = self.shareCnt + jobNodeCnt
			print "pfs share count after new request %d" % self.shareCnt
			procName = "pfsReq_"+str(jobID)
			self.jobToExecuteMap[jobID] = procName
			print "pfs got stagein req from job %d reqsize %f" % (jobID, reqSize)
			self.notifyShareChange(jobID)
			self.createProcess(procName, self.pfsExecutor_hybrid)
			self.startProcess(procName, procName, jobID, reqType, hybridNodes, reqSize, jobNodeCnt)
			
		elif reqType == 4 or reqType == 5:
			#local arch stagein
			localNodes = data[2]
			reqSize = data[3]
			jobNodeCnt = data[4]
			self.shareCnt = self.shareCnt + jobNodeCnt
			print "pfs share count after new request %d" % self.shareCnt
			procName = "pfsReq_"+str(jobID)
			self.jobToExecuteMap[jobID] = procName
			print "pfs got stageout req from job %d reqsize %f" % (jobID, reqSize)
			self.notifyShareChange(jobID)
			self.createProcess(procName, self.pfsExecutor_local)
			self.startProcess(procName, procName, jobID, reqType, localNodes, reqSize, jobNodeCnt)
		
		elif reqType == 6 or reqType == 7:
			#shared partition
			compNodes = data[2]
			bbNodes = data[3]
			reqSize = data[4]
			self.shareCnt = self.shareCnt + len(bbNodes)
			procName = "pfsReq_"+str(jobID)
			self.jobToExecuteMap[jobID] = procName
			self.notifyShareChange(jobID)
			self.createProcess(procName, self.pfsExecutor_)
			self.startProcess(procName, procName, jobID, reqType, compNodes, bbNodes, reqSize)

		elif reqType == -1:
			#a fault
			compNodes = data[2]
			bbNodes = data[3]
			procName = "pfsReq_"+str(jobID)
			procStatus = self.statusProcess(procName)
			print"pfs handling fault for job %d" % jobID
			if procStatus == "suspended":
				self.wakeupArgs[0] = -1
				self.wakeProcess(procName, *(self.wakeupArgs))

		elif reqType == -2:
			#this is a bb fault during stageout of a job. need to cancel bb req on the bad bb
			bbVictim = data[4]
			procName = "pfsReq_"+str(jobID)
			procStatus = self.statusProcess(procName)
			if procStatus == "suspended":
				self.wakeupArgs[0] = -3
				self.wakeupArgs[1] = bbVictim
				self.wakeProcess(procName, *(self.wakeupArgs))

		elif reqType == -3:
			#hybrid or local  node cancel
			print "pfs got cancel message for job %d at %f" % (jobID, self.engine.now)
			procName = "pfsReq_"+str(jobID)
			procStatus = self.statusProcess(procName)
			if procStatus == "suspended":
				self.wakeupArgs[0] = -1
				self.wakeProcess(procName, *(self.wakeupArgs))


	def wakeupFromIO(self, data, tx, txID):
		procName = data
		procStatus = self.statusProcess(procName)
		if procStatus == "suspended":
			self.wakeupArgs[0] = 0
			self.wakeProcess(procName)

	def notifyPFS(self, data, tx, txID):
		jobID = data[0]
		nodeID = data[1]
		bbID = data[2]
		print "notify test job %d nodeid %d bbid %d" % (jobID, nodeID, bbID)
		procName = self.jobToExecuteMap[jobID]
		procStat = self.statusProcess(procName)
		if procStat == "suspended":
			self.wakeupArgs[0] = bbID
			self.wakeupArgs[1] = [jobID, nodeID]
			self.wakeProcess(procName, *(self.wakeupArgs))

	def notifyPFShybrid(self, data, tx, txID):
		jobID = data[0]
		nodeID = data[1]
		reqName = data[2]
		retval = data[3]
		print "pfs got done nofitication from hybrid node %d at %f" % (nodeID, self.engine.now)
		procName = self.jobToExecuteMap[jobID]
		procStat = self.statusProcess(procName)
		if procStat == "suspended":
			self.wakeupArgs[0] = nodeID
			self.wakeupArgs[1] = retval
			self.wakeProcess(procName, *(self.wakeupArgs))

	### Helper Functions ###
	def cancelHybridPfsRequests(self, jobID, reqType, hybridNodes):
		for nodeID in hybridNodes:
			reqName = None
			if reqType == 2:
				#stage in
				reqName = "stageIn_"+str(jobID)+"_"+str(nodeID)
			elif reqType == 3:
				#stage out
				reqName = "stageOut_"+str(jobID)+"_"+str(nodeID)
			self.reqService(self.engine.minDelay, "cancelPfsReq", [jobID, reqName], "hybrid"+str(nodeID), nodeID)
			

	def cancelSingleBBNodeRequests(self, jobID, compNodes, bbNodes, bbVictim):
		length = len(compNodes)
		for bb in bbNodes:
			if bb == bbVictim:
				#must tell this bb to cancel this stageout request
				bbName = "bb"+str(bb)
				args = []
				args.append(-2)
				args.append(jobID)
				args.append(length)
				print "###PFS SENDING CANCEL TO BB %d" % bbVictim
				self.reqService(self.engine.minDelay, "bbRequest", args, bbName, bb)
					
	def cancelingSharedPartitionBBreqs(self, jobID, compNodes, bbReqMap):
		length = len(compNodes)
		keys = bbReqMap.keys()
		for key in keys:
			bb = bbReqMap[key]
			#tell this bb to cancel
			bbName = "bb"+str(bb)
			args = []
			args.append(-3)
			args.append(jobID)
			args.append(length)
			print "pfs sending bb cancle to bb %d at %f" % (bb, self.engine.now)
			self.reqService(self.engine.minDelay, "bbRequest", args, bbName, bb)
			self.shareCnt = self.shareCnt - 1
			print "pfs share cnt %d after sending cancel to bb %d" % (self.shareCnt, bb)

	def cancelBBRequests(self, jobID, bbReqMap):
		keys = bbReqMap.keys()
		for key in keys():
			bb = bbReqMap[key]
			args = []
			bbName = "bb"+str(bb)
			args.append(-1)
			args.append(jobID)
			args.append(key)
			self.reqService(self.engine.minDelay, "bbRequest", args, bbName, bb)

		length = len(compNodes)
		for i in range(0, length):
			for bb in bbNodes:
				args = []
				bbName = "bb"+str(bb)
				args.append(-1)
				args.append(jobID)
				args.append(length)
				self.reqService(self.engine.minDelay, "bbRequest", args, bbName, bb)

	def notifyShareChange(self, jobID):
		jobIDs = self.jobToExecuteMap.keys()
		for job in jobIDs:
			if job != jobID:
				procName = self.jobToExecuteMap[job]
				procStat = self.statusProcess(procName)
				if procStat == "suspended":
					self.wakeupArgs[0] = -4
					print "job %d notifying job %d share count change to %d" % (jobID, job, self.shareCnt)
					self.wakeProcess(procName, *(self.wakeupArgs))
		

	def batchUpdateBWToExecutors(self):
		currentBW = self.PFSBandwidth / self.shareCnt
		keys = self.jobToExecuteMap.keys()
		for jobID in keys:
			procName = self.jobToExecuteMap[key]
			procStat = self.statusProcess(procName)
			if procStat == "suspended":
				print "updating job %d executor for bw change" % jobID
				self.wakeupArgs[0] = -2
				self.wakeProcess(procName)

	def updatePfsBwToBBs(self, targetName, jobID, bbReqMap):
		#first notify bbs for bandwidth change on this executor
		currentBW = self.PFSBandwidth / self.shareCnt
		print "new pfs Bw %f" % currentBW
		keys = bbReqMap.keys()
		for key in keys:
			if key != targetName:
				#send update bw notification to this node
				print "targetName %s; key %s" % (targetName, key)
				bb = bbReqMap[key]
				target = "bb"+str(bb)
				print "pfs sending bw %f update to bb %d" % (currentBW, bb)
				self.reqService(self.engine.minDelay, "updatePfsBandwidth", [key, currentBW], target, bb)
				
		#second notify other on going executors but only if it is the first one to call it
		if targetName != None:
			print "pfs job %d executor notifyingo ther executor bw change" % jobID
			keys = self.jobToExecuteMap.keys()
			if len(keys) == 0:
				print "no other job executors on pfs"
			for key in keys:
				if key != jobID:
					procName = self.jobToExecuteMap[key]
					procStat = self.statusProcess(procName)
					if procStat == "suspended":
						print "wake up pfs executor for job %d" % key
						self.wakeupArgs[0] = -2 # this is a bw change message
						self.wakeProcess(procName)

	def updateBwToNodes(self, jobID, hybridNodes, reqType):
		newBw = self.PFSBandwidth / self.shareCnt
		for nodeID in hybridNodes:
			reqName = None
			if reqType == 2:
				#stage in
				reqName = "stageIn_"+str(jobID)+"_"+str(nodeID)
			elif reqType == 3:
				#stage out
				reqName = "stageOut_"+str(jobID)+"_"+str(nodeID)
			
			self.reqService(self.engine.minDelay, "notifyPFSBw", [reqName, newBw], "hybrid"+str(nodeID), nodeID)

	def computeIOTime(self, reqType, reqSize, pfsBw):
		bandwidth = None
		if reqType == 0:
			#write
			bandwidth = min(pfsBw, self.compNodeBw, self.localWriteBw)
		elif reqType == 1:
			#read
			bandwidth = min(pfsBw, self.compNodeBw, self.localReadBw)

		ioTime = reqSize / bandwidth
		return [ioTime, bandwidth]

	def computeNewIOTime(self, reqType, currentBw, start, reqSize):
		now = self.engine.now
		duration = now - start
		print "reqSize %f current bw %f now %f, stat %f duration %f; remain %f" % (reqSize, currentBw, now, start, duration, reqSize)
		remain = abs(reqSize - (currentBw * duration))
		print "new remain %f" % remain
		minBandwidth = None
		if reqType == 0:
			#write
			minBandwidth = min(self.PFSBandwidth / self.shareCnt, self.compNodeBw, self.localWriteBw)
		elif reqType == 1:
			#read
			minBandwidth = min(self.PFSBandwidth / self.shareCnt, self.compNodeBw, self.localReadBw)

		ioTime = remain / minBandwidth
		return [ioTime, minBandwidth, remain]
