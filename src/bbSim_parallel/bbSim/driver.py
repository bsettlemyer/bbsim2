import sys
import random
import datetime
from simulation import *
from SimianPie.simian import Simian

###Setup###
random.seed(1)
inputFileName = sys.argv[1]
#Now we have to set the simulation end time to as long as possible because 
#Simian does not support ending simulation based on an event.
#We just manually set the endtime when All jobs are done
simName, startTime, endTime, minDelay, useMPI = "Simulation", 0, 999999999999999999999, 0.0000001, True 
simEngine = Simian(simName, startTime, endTime, minDelay, useMPI, "/usr/local/lib/libmpich.so")

simulation = Sim(simEngine, inputFileName)

simulation.startSimulation()
