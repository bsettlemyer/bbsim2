import math
import time
import copy
from SimianPie.simian import *
from SimianPie.entity import Entity

#implements a hybrid node
class HybridNode(Entity):
	def __init__(self, baseInfo, *args):
		super(HybridNode, self).__init__(baseInfo)
		self.totalNodeNumber = args[0]
		self.memSize = args[1]
		self.bandwidth = args[2]
		self.parity = args[3]
		self.parityOverhead = args[4]
		self.engine = args[5]
		self.localWriteBw = args[6]
		self.localReadBw = args[7]
		self.nodeID = args[8]
		self.localID = -1
		self.state = -1
		self.wakeupArgs = [-99, -1]
		self.jobID = None
		self.procID = 0
		self.stageReqName = None
		self.totalIdle = 0
		self.totalNonIdle = 0
		self.startIdle = 0
		self.endIdle = 0
		self.startNonIdle = 0
		self.endNonIdle = 0
	### Main processes ###
	def runHybrid(self, this, jobID, jobNodes, jobNodeCnt, chkpRatio, chkpFreq, startMode, startCycle, restartFlag):
		self.jobID = jobID
		procID = self.procID
		finish = 0
		#print "hybrid node %d running at %f" % (self.nodeID, self.engine.now)
		self.state = 0
		computeTime = 0
		ioTime = 0
		chkpSize = None
		#if self.parity == 1 and jobNodeCnt > 1:
		#	chkpSize = (1 + (1.0/(jobNodeCnt - 1))) * self.memSize * chkpRatio
		#else:
		chkpSize = self.memSize * chkpRatio

		#print "node checkpoint size %f" % chkpSize
		#we need to read from restart when we have parity protected
		ret = 0
		if restartFlag == 1:
			if self.localID == 0:
				print "jobID %d node %d start reading checkpoing" % (jobID, self.nodeID)
			ret = self.readCheckpoint(this, jobNodeCnt, chkpSize, jobID, procID)
			if ret == -1:
				finish = 1

		while finish == 0:
			#compute state
			if self.state == 0:
				#issue wakeup event
				self.reqService(self.engine.minDelay + chkpFreq, "wakeupFromCompute", [jobID, procID], "hybrid"+str(self.nodeID), self.nodeID)
				startTime = self.engine.now
				this.hibernate(*(self.wakeupArgs))
				endTime = self.engine.now
				wakeupVal = self.wakeupArgs[0]
				if wakeupVal == -1 or wakeupVal == -3:
					if self.localID == 0:
						print "hybridnode %d for job %d quitting in compute at %f" % (self.nodeID, jobID, self.engine.now)
					finish = 1
				elif wakeupVal == -2:
					#wakeup from compute
					if self.localID == 0:
						print "cn %d for job %d finished compute at %f" % (self.nodeID, jobID, self.engine.now)
					pass

				self.state = 1
				computeTime = endTime - startTime
				if self.localID == 0:
					self.reqService(self.engine.minDelay, "updateJobComputeTime", computeTime, "job"+str(jobID), jobID)
					if finish == 1:
						#this compute time is wasted since there is a fault
						self.reqService(self.engine.minDelay, "updateJobWastedComputeTime", computeTime, "job"+str(jobID), jobID)
			elif self.state == 1:
				#checkpoint state
				#just compute the maximun amount of wait time because there are no interference since it is local
				reqSize = chkpSize / jobNodeCnt
				ioTime = self.computeIOTime(jobNodeCnt, reqSize)
				#print "### checkpoint write time %f" % ioTime
				self.reqService(self.engine.minDelay + ioTime, "wakeupFromIO", [jobID, procID], "hybrid"+str(self.nodeID), self.nodeID)
				startTime = self.engine.now
				this.hibernate(*(self.wakeupArgs))
				endTime = self.engine.now
				ioTime = endTime - startTime
				wakeupVal = self.wakeupArgs[0]
				if wakeupVal == -1 or wakeupVal == -3:
					if self.localID == 0:
						print "hybridnode %d for job %d ending in IO at %f" % (self.nodeID, jobID, self.engine.now)
					finish = 1
				elif wakeupVal == -4:
					self.state = 0
					if self.localID == 0:
						print "hybridnode %d for job %d finished checkpointing at %f" % (self.nodeID, jobID, self.engine.now)
						self.reqService(self.engine.minDelay, "updateCycle", None, "job"+str(jobID), jobID)
						
				if self.localID == 0:
					bandwidth = self.memSize * chkpRatio * jobNodeCnt / (endTime - startTime)
					self.reqService(self.engine.minDelay, "recordCPBandwidth", bandwidth, "scheduler", 0)
					self.reqService(self.engine.minDelay, "updateJobIOTime", ioTime, "job"+str(jobID), jobID)
					if finish == 1:
						self.reqService(self.engine.minDelay, "updateJobWastedComputeTime", computeTime, "job"+str(jobID), jobID)
		self.procID = self.procID + 1

	def stageReq(self, this, reqName, reqType, jobID, pfsBw, jobNodeCnt, reqSize):
		print "reqName %s" % reqName
		#first compute job size
		retval = 0
		realReqSize = reqSize
		bundle = self.computeStageIOTime(reqType, pfsBw, jobNodeCnt, realReqSize)
		ioTime = bundle[0]
		startBw = bundle[1]
		remain = realReqSize
		print "node %d remain io size %f; remain io time %f; start bw %f" % (self.nodeID, remain, ioTime, startBw)
		print "iotime %f" % ioTime
		timeStamp = self.engine.now + ioTime + self.engine.minDelay
		self.reqService(self.engine.minDelay + ioTime, "wakeupFromStageIO", reqName, "hybrid"+str(self.nodeID), self.nodeID)
		startTime = self.engine.now
		done = 0
		while done == 0:
			this.hibernate(*(self.wakeupArgs))
			if self.wakeupArgs[0] == -1 or self.wakeupArgs[0] == -3:
				print "hybridnode %d for job %d ending in stage req at %f" % (self.nodeID, jobID, self.engine.now)
				retval = 1
			elif self.wakeupArgs[0] == -4:
				print "node fault during stage out %d" % self.nodeID
				retval = 2
			elif self.wakeupArgs[0] == -5:
				#wokeup normally
				currentTime = self.engine.now
				if abs(currentTime - timeStamp) <= self.engine.minDelay:
					print "hybridnode %d finished this Stage req" % self.nodeID
					done = 1
				else:
					print "node %d waking up from stage IO old timer, VOID" % self.nodeID
					pass
			elif self.wakeupArgs[0] == -6:
				#update pfs bandwidth
				
				newPfsBw = self.wakeupArgs[1]
				bundle = self.computeNewStageIOTime(reqType, newPfsBw, jobNodeCnt, remain, startTime, startBw)
				ioTime = bundle[0]
				startBw = bundle[1]
				remain = bundle[2]
				print "after pfs bw update, node %d remaining stage io size %f, remaining IO time %f, newbandwidth %f" % (self.nodeID, remain, ioTime, startBw)
				startTime = self.engine.now
				timeStamp = self.engine.now + ioTime + self.engine.minDelay
				self.reqService(self.engine.minDelay + ioTime, "wakeupFromStageIO", reqName, "hybrid"+str(self.nodeID), self.nodeID)
				
		#send done notification to pfs
		if retval == 0 or retval == 2:
			self.reqService(self.engine.minDelay, "notifyPFShybrid", [jobID, self.nodeID, reqName, retval], "pfs", 0)

	###Async. Event Handler###
	def stageInReq(self, data, tx, txID):
		stageType = data[0]
		jobID = data[1]
		pfsBw = data[2]
		reqName = data[3]
		jobNodeCnt = data[4]
		reqSize = data[5]
		self.stageReqName = reqName
		procName = reqName
		print "node %d got stage in io request size %f" % (self.nodeID, reqSize)
		self.createProcess(procName, self.stageReq)
		self.startProcess(procName, procName, stageType, jobID, pfsBw, jobNodeCnt, reqSize)

	def stageOutReq(self, data, tx, txID):
		stageType = data[0]
		jobID = data[1]
		pfsBw = data[2]
		reqName = data[3]
		jobNodeCnt = data[4]
		reqSize = data[5]
		self.stageReqName = reqName
		procName = reqName
		print "node %d got stage out io request size %f" % (self.nodeID, reqSize)
		self.createProcess(procName, self.stageReq)
		self.startProcess(procName, procName, stageType, jobID, pfsBw, jobNodeCnt, reqSize)


	def wakeupFromCompute(self, data, tx, txID):
		jobID = data[0]
		procID = data[1]
		if self.jobID == jobID and procID == self.procID:
			#only wakeup if it is for the same job
			procName = "runHybrid"
			procStatus = self.statusProcess(procName)
			if procStatus == "suspended":
				self.wakeupArgs[0] = -2
				self.wakeProcess(procName, *(self.wakeupArgs))

	def wakeupFromIO(self, data, tx, txID):
		jobID = data[0]
		procID = data[1]
		if self.jobID == jobID and procID == self.procID:
			procName = "runHybrid"
			procStatus = self.statusProcess(procName)
			if procStatus == "suspended":
				self.wakeupArgs[0] = -4
				self.wakeProcess(procName, *(self.wakeupArgs))

	def wakeupFromStageIO(self, data, tx, txID):
		print "node %d waking up from stage io at %f" % (self.nodeID, self.engine.now)
		procName = data
		procStatus = self.statusProcess(procName)
		if procStatus == "suspended":
			self.wakeupArgs[0] = -5
			self.wakeProcess(procName, *(self.wakeupArgs))

	def startHybridProcess(self, data, tx, txID):
		#parse args
		self.localID = data[0]
		jobID = data[1]
		jobNodeCnt = data[2]
		chkpRatio = data[3]
		chkpFreq = data[4]
		jobNodes = data[5]
		startMode = data[6]
		startCycle = data[7]
		restartFlag = data[8]
		procName = "runHybrid"
		self.createProcess(procName, self.runHybrid)
		self.startProcess(procName, jobID, jobNodes, jobNodeCnt, chkpRatio, chkpFreq, startMode, startCycle, restartFlag)

	def stopHybridProcess(self, data, tx, txID):
		procName = "runHybrid"
		procStatus = self.statusProcess(procName)
		if procStatus == "suspended":
			self.wakeupArgs[0] = -1
			self.wakeProcess(procName, *(self.wakeupArgs))

	def cancelHybridNode(self, data, tx, txID):
		procName1 = "runHybrid"
		procName2 = "stageReq"
		procStatus1 = self.statusProcess(procName1)
		procStatus2 = self.statusProcess(procName2)
		if procStatus1 == "suspended":
			self.wakeupArgs[0] = -3
			self.wakeProcess(procName1, *(self.wakeupArgs))
		if procStatus2 == "suspended":
			self.wakeupArgs[0] = -3
			self.wakeProcess(procName2, *(self.wakeupArgs))

	def cancelPfsReq(self, data, tx, txID):
		print "node %d got io cancel req at %f" % (self.nodeID, self.engine.now)
		jobID = data[0]
		reqName = data[1]
		procStatus = self.statusProcess(reqName)
		if procStatus == "suspended":
			self.wakeupArgs[0] = -3
			self.wakeProcess(reqName, *(self.wakeupArgs))

	#used for job to cancel a stageout req on a single faulty node due to parity protection so other nodes can keep goin
	def cancelStageOutReq(self, data, tx, txID):
		print "hybrid node %d got cancel stage out request at %f" % (self.nodeID, self.engine.now)
		procStatus = self.statusProcess(self.stageReqName)
		print "status %s" % procStatus
		if procStatus == "suspended":
			self.wakeupArgs[0] = -4
			self.wakeProcess(self.stageReqName, *(self.wakeupArgs))

	def notifyPFSBw(self, data, tx, txID):
		print "hybrid node %d got pfs bandwidht update msg" % self.nodeID
		reqName = data[0]
		newBw = data[1]
		procStatus = self.statusProcess(reqName)
		if procStatus == "suspended":
			self.wakeupArgs[0] = -6
			self.wakeupArgs[1] = newBw
			self.wakeProcess(reqName, *(self.wakeupArgs))

	def markNodeBusy(self, data, tx, txID):
		self.endIdle = self.engine.now
		self.startNonIdle = self.engine.now
		self.totalIdle = self.totalIdle + self.endIdle - self.startIdle
		#print "node %d marked busy. startIdle %f, endIdle %f, startNonIdle %f, endNonIdle %f, totalIdle %f, totalNonIdle %f" % (self.nodeID, self.startIdle, self.endIdle, self.startNonIdle, self.endNonIdle, self.totalIdle, self.totalNonIdle)

	def markNodeFree(self, data, tx, txID):
		self.endNonIdle = self.engine.now
		self.startIdle = self.engine.now
		self.totalNonIdle = self.totalNonIdle + self.endNonIdle - self.startNonIdle
		#print "node %d marked free. startIdle %f, endIdle %f, startNonIdle %f, endNOnIdle %f, totalIdle %f, totalNonidle %f" % (self.nodeID, self.startIdle, self.endIdle, self.startNonIdle, self.endNonIdle, self.totalIdle, self.totalNonIdle)

	def endNode(self, data, tx, txID):
		self.endIdle = self.engine.now
		self.totalIdle = self.endIdle - self.startIdle + self.totalIdle
		print "node %d totalIdle when simulation ends %f" % (self.nodeID, self.totalIdle)
	def getNodeStats(self, data, tx, txID):
		stats = []
		stats.append(self.totalNonIdle)
		stats.append(self.totalIdle)
		self.reqService(self.engine.minDelay, "reportNodeStats", [self.nodeID, stats], "scheduler", 0)

	### Helper Functions ###
	def readCheckpoint(self, this, jobNodeCnt, chkpSize, jobID, procID):
		retval = 0
		reqSize = chkpSize / jobNodeCnt
		ioTime = self.computeIOTime(jobNodeCnt, reqSize)
		#print "read checkpoint duration is %f" % ioTime
		self.reqService(self.engine.minDelay + ioTime, "wakeupFromIO", (jobID, procID), "hybrid"+str(self.nodeID), self.nodeID)
		start = self.engine.now
		this.hibernate(*(self.wakeupArgs))
		end = self.engine.now
		ioDuration = end - start
		wakeupVal = self.wakeupArgs[0]
		if wakeupVal == -4:
			if self.localID == 0:
				print "node %d finished reading checkpoint at %f" % (self.nodeID, self.engine.now)
			retval = 0
		elif wakeupVal == -1 or wakeupVal == -3:
			if self.localID == 0:
				print "node %d ending in checkpoint at %f" % (self.nodeID, self.engine.now)
			retval = -1
		if self.localID == 0:
			#upate job iotime
			self.reqService(self.engine.minDelay, "updateJobIOTime", ioDuration, "job"+str(jobID), jobID)
		return retval

	def computeIOTime(self, jobNodeCnt, chkpSize):
		numNetworkReqs = (jobNodeCnt - 1) * 2
		sharedNetworkBandwidth = self.bandwidth / numNetworkReqs
		sharedSSDBandwidth = self.localWriteBw / (numNetworkReqs + 1)
		#print "sharedNetworkBw %f; sharedSSDBandwidth %f" % (sharedNetworkBandwidth, sharedSSDBandwidth)
		minBandwidth = min(sharedNetworkBandwidth, sharedSSDBandwidth)
		#print "min bandwidht %f" % minBandwidth
		ioTime = chkpSize / minBandwidth
		#print "io time for checkpoint write %f reqsize %f" % (ioTime, chkpSize)
		return ioTime

	def computeNewStageIOTime(self, reqType, newPfsBw, jobNodeCnt, reqSize, startTime, startBw):
		timeNow = self.engine.now
		remain = reqSize - ((timeNow - startTime)*startBw)
		numNetworkReqs = (jobNodeCnt - 1) * 2
		sharedNetworkBandwidth = self.bandwidth / numNetworkReqs
		if reqType == 0:
			#write
			sharedSSDBandwidth = self.localWriteBw / (numNetworkReqs + 1)
		elif reqType == 1:
			#read
			sharedSSDBandwidth = self.localReadBw / (numNetworkReqs + 1)

		minBandwidth = min(sharedNetworkBandwidth, sharedSSDBandwidth, newPfsBw)
		ioTime = reqSize / minBandwidth
		return [ioTime, minBandwidth, remain]

	def computeStageIOTime(self, reqType, pfsBw, jobNodeCnt, reqSize):
		sharedSSDBandwidth = None
		numNetworkReqs = (jobNodeCnt - 1) * 2
		sharedNetworkBandwidth = self.bandwidth / numNetworkReqs
		if reqType == 0:
			#write
			sharedSSDBandwidth = self.localWriteBw / (numNetworkReqs + 1)
		elif reqType == 1:
			#read
			sharedSSDBandwidth = self.localReadBw / (numNetworkReqs + 1)

		print "node %d shared ssd bw %f shared network bw %f, pfs bw %f" % (self.nodeID, sharedSSDBandwidth, sharedNetworkBandwidth, pfsBw)
		minBandwidth = min(sharedNetworkBandwidth, sharedSSDBandwidth, pfsBw)
		ioTime = reqSize / minBandwidth
		return [ioTime, minBandwidth]

