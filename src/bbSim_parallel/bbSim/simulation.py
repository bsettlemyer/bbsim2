import math
import random
from SimianPie.simian import Simian
from computeNode import *
from bbNode import *
from job import *
from scheduler import *
from pfs import *
from hybridNode import *
from hybridJob import *
from localNode import *
from localJob import *
class Sim:
	def __init__(self, simEngine, inputFileName):
		self.engine = simEngine
		self.inputFileName = inputFileName
		self.bbArch = -1 # 0 - shared; 1 - local
		self.faultRate = 0.0
		self.stageInPolicy = 0 # 0 - at the beginning of job; 1 - speculative
		self.bbNumber = 0
		self.bbCapacity = 0.0
		self.bbReadBw = 0.0
		self.bbWriteBw = 0.0
		self.bbLocalWriteBw = 0.0
		self.bbNodes = []
		self.pfsBw = 0.0
		self.totalCompNodeNumber = 0
		self.compNodeMem = 0
		self.compNodeBw = 0.0
		self.nodeRecoveryTime = -1
		self.parity = -1 # 0 - parity off; 1 - parity on
		self.parityOverhead = 0
		self.bbSelect = -1 # 0 - random; 1 - random round robin
		self.bbPartSize = -1
		self.pfs = None
		self.compNodes = []
		self.reqQs = []
		self.jobs = []
		self.jobInfo = []
		self.scheduler = None
		self.output_prefix = None
		self.bcastEntities = []
		for i in range(0, self.engine.size):
			self.bcastEntities.append(None)

	def startSimulation(self):
		fdIn = open(self.inputFileName, 'r')
		## read in prefix ##
		line = fdIn.readline().rstrip()
		self.output_prefix = line
		###read in architecture###
		line = fdIn.readline().rstrip()
		if line == "shared":
			self.bbArch = 0
			self.startShared(fdIn)
		elif line == "local":
			self.bbArch = 1
			self.startLocal(fdIn)
		elif line == "hybrid":
			self.bbArch = 2
			self.startHybrid(fdIn)
		elif line == "sharedPartition":
			self.bbArch = 3
			self.startShared(fdIn)

	def startHybrid(self, fd):
		#read common parameters
		self.readCommonParameters(fd)
		#read bb group size
		

		#read skip line
		line = fd.readline()

		#compute local BB bandwidths
		localWriteBw = self.bbWriteBw / self.totalCompNodeNumber
		localReadBw = self.bbReadBw / self.totalCompNodeNumber
		print "local write bw %f" % localWriteBw
		print "local read bw %f" % localReadBw
		#setup compute nodes
		args = []
		args.append(self.totalCompNodeNumber)
		args.append(self.compNodeMem)
		args.append(self.compNodeBw)
		args.append(self.parity)
		args.append(self.parityOverhead)
		args.append(self.engine)
		args.append(localWriteBw)
		args.append(localReadBw)
		args.append(0)
		
		for i in range(0, self.totalCompNodeNumber):
			#build args
			length = len(args)
			args[length - 1] = i
			name = "hybrid"+str(i)
			rank = self.engine.addEntity(name, HybridNode, i, *args)
			
			self.compNodes.append(name)

		#read in job queues
		line = fd.readline()
		while line[:3] != "END":
			segments = line.split(" ")
			if segments[0] == 'job':
				args = []
				args.append(self.totalCompNodeNumber)
				args.append(self.compNodeMem)
				jobID = self.getJobParams(fd, args)
				args.append(self.engine)
				args.append("scheduler")
				args.append(self.stageInPolicy)
				args.append(self.parity)
				name = "job"+str(jobID)
				print "job ID %d" % jobID
				self.engine.addEntity(name, HybridJob, jobID, *args)
				self.jobs.append(name)
				line = fd.readline()

		print self.jobs
		#set up pfs
		args = []
		args.append(self.engine)
		args.append("scheduler")
		args.append(self.parity)
		args.append(self.parityOverhead)
		args.append(self.pfsBw)
		args.append(self.compNodeBw)
		args.append(localWriteBw)
		args.append(localReadBw)
		self.engine.addEntity("pfs", Pfs, 0, *args)

		#set up start scheduler
		args = []
		args.append(self.bbArch)
		args.append(self.nodeRecoveryTime)
		args.append(self.compNodes)
		args.append(self.jobInfo)
		args.append(self.engine)
		args.append(self.faultRate)
		args.append(self.parity)
		#build node stats
		hybridNodeStats = []
		for i in range(self.totalCompNodeNumber):
			hybridNodeStats.append(-1)
		args.append(hybridNodeStats)
		args.append(self.output_prefix)
		self.engine.addEntity("scheduler", Scheduler, 0, *args)
		self.engine.run()
		self.engine.exit()

	def startShared(self, fd):
		#read common parameters
		self.readCommonParameters(fd)

		#read total bb node number
		line = fd.readline()
		self.bbNumber = self.getInt(line.rstrip())

		if self.bbArch == 3:
			line = fd.readline()
			self.bbPartSize = self.getInt(line.rstrip())
		#skip line
		line = fd.readline()
		
		bbNodeWriteBw = self.bbWriteBw/self.bbNumber
		bbNodeReadBw = self.bbReadBw/self.bbNumber
		#initialize burst buffer nodes
		#set up for args
		args = []
		args.append(0)
		args.append(bbNodeWriteBw)
		args.append(bbNodeReadBw)
		args.append(self.engine)
		for i in range(1, self.bbNumber+1):
			args[0] = i
			name = "bb"+str(i)
			self.bbNodes.append(name)
			rank = self.engine.addEntity(name, BBNode, i, *args)
			if self.bcastEntities[rank] == None:
				self.bcastEntities[rank] = (name,i)
		#setup scheduler name
		scheduler = "scheduler"
		#initialize compute nodes
		args = []
		args.append(self.totalCompNodeNumber)
		args.append(self.compNodeMem)
		args.append(self.compNodeBw)
		args.append(self.parity)
		args.append(self.parityOverhead)
		args.append(self.engine)
		args.append(0)
		for i in range(0, self.totalCompNodeNumber):
			#build args
			length = len(args)
			args[length-1] = i
			name = "cn"+str(i)
			self.engine.addEntity(name, ComputeNode, i, *args)
			self.compNodes.append(name)
		#read in job queues
		line = fd.readline()
		i = 0 #this is for simian id for jobs
		while line[:3] != "END":
			segments = line.split(" ")
			if segments[0] == 'job':
				args = []
				args.append(self.totalCompNodeNumber)
				args.append(self.compNodeMem)
				jobID = self.getJobParams(fd, args)
				args.append(scheduler)
				args.append(self.stageInPolicy)
				args.append(self.parity)
				name = "job"+str(jobID)
				self.engine.addEntity(name, Job, jobID, *args)
				self.jobs.append(name)
				i = i + 1
				line = fd.readline()
		print self.jobs
		#start pfs
		args = []
		args.append(self.engine)
		args.append(scheduler)
		args.append(self.parity)
		args.append(self.parityOverhead)
		args.append(self.pfsBw)
		args.append(self.compNodeBw)
		args.append(bbNodeWriteBw)
		args.append(bbNodeReadBw)
		self.engine.addEntity("pfs", Pfs, 0, *args)

		#start scheduler
		args = []
		args.append(self.bbArch)
		args.append(self.nodeRecoveryTime)
		args.append(self.compNodes)
		args.append(self.jobInfo)
		args.append(self.engine)
		args.append(self.faultRate)
		args.append(self.parity)
		#build compute node stats
		compNodeStats = []
		for i in range(self.totalCompNodeNumber):
			compNodeStats.append(-1)
		args.append(compNodeStats)
		args.append(self.bbNodes)
		bbNodeStats = []
		for i in range(self.bbNumber):
			bbNodeStats.append(-1)
		args.append(bbNodeStats)
		args.append(self.bbSelect)
		args.append(self.bbNumber)
		args.append(self.bbPartSize)
		args.append(self.output_prefix)
		print "adding scheduler rank %d" % self.engine.rank
		self.engine.addEntity(scheduler, Scheduler, 0, *args)


		#start simulation
		self.engine.run()
		print "simulation done"
		self.engine.exit()
		
	def startLocal(self, fd):
		#read common parameters
		self.readCommonParameters(fd)

		#read skip line
		line = fd.readline()
		
		#compute local bb bandwidths
		localWriteBw = self.bbWriteBw / self.totalCompNodeNumber
		localReadBw = self.bbReadBw / self.totalCompNodeNumber
		print "local write bw %f" % localWriteBw
		print "local read bw %f" % localReadBw

		#set up local nodes
		args = []
		args.append(self.totalCompNodeNumber)
		args.append(self.compNodeMem)
		args.append(self.compNodeBw)
		args.append(self.parity)
		args.append(self.parityOverhead)
		args.append(self.engine)
		args.append(localWriteBw)
		args.append(localReadBw)
		args.append(0)

		for i in range(0, self.totalCompNodeNumber):
			#build args
			length = len(args)
			args[length - 1] = i
			name = "local"+str(i)
			self.engine.addEntity(name, LocalNode, i, *args)
			self.compNodes.append(name)

		#read in job queues
		self.readJobQueues(fd)

		#setup pfs
		args = []
		args.append(self.engine)
		args.append("scheduler")
		args.append(self.parity)
		args.append(self.parityOverhead)
		args.append(self.pfsBw)
		args.append(self.compNodeBw)
		args.append(localWriteBw)
		args.append(localReadBw)
		self.engine.addEntity("pfs", Pfs, 0, *args)

		#set up start scheduler
		args = []
		args.append(self.bbArch)
		args.append(self.nodeRecoveryTime)
		args.append(self.compNodes)
		args.append(self.jobInfo)
		args.append(self.engine)
		args.append(self.faultRate)
		args.append(self.parity)

		#build node stats
		localNodeStats = []
		for i in range(self.totalCompNodeNumber):
			localNodeStats.append(-1)
		args.append(localNodeStats)
		args.append(self.output_prefix)
		self.engine.addEntity("scheduler", Scheduler, 0, *args)
		print "before starting?"
		self.engine.run()
		self.engine.exit()
	#### Helper functions ####
	def readCommonParameters(self, fd):
		#read fault rate
		line = fd.readline()
		self.faultRate = self.getFloat(line.rstrip()) * 60 * 60

		#read recovery rate
		line = fd.readline()
		self.nodeRecoveryTime = self.getFloat(line.rstrip()) * 60 * 60

		#read stagein policy
		line = fd.readline()
		self.stageInPolicy = self.getInt(line.rstrip())

		#read parity
		line = fd.readline()
		self.parity = self.getInt(line.rstrip())

		#read pfs bandwidth
		line = fd.readline()
		self.pfsBw = self.getFloat(line.rstrip())

		#read total bb capacity
		line = fd.readline()
		self.bbCapacity = self.getFloat(line.rstrip())

		#read bb aggregate write bw
		line = fd.readline()
		self.bbWriteBw = self.getFloat(line.rstrip())

		#read bb aggregate read bw
		line = fd.readline()
		self.bbReadBw = self.getFloat(line.rstrip())
		
		#read total compute node number
		line = fd.readline()
		self.totalCompNodeNumber = self.getInt(line.rstrip())

		#read compute node mem size
		line = fd.readline()
		self.compNodeMem = self.getFloat(line.rstrip())

		#read compute node bandwidth
		line = fd.readline()
		self.compNodeBw = self.getFloat(line.rstrip())

	def readJobQueues(self, fd):
		line = fd.readline()
		while line[:3] != "END":
			segments = line.split(" ")
			if segments[0] == 'job':
				args = []
				args.append(self.totalCompNodeNumber)
				args.append(self.compNodeMem)
				jobID = self.getJobParams(fd, args)
				args.append(self.engine)
				args.append("scheduler")
				args.append(self.stageInPolicy)
				args.append(self.parity)
				args.append(self.parityOverhead)
				name = "job"+str(jobID)
				self.engine.addEntity(name, LocalJob, jobID, *args)
				self.jobs.append(name)
				line = fd.readline()

	def getJobLocalParams(self, fd, args):
		
		line = fd.readline().rstrip()
		jobType = line
		args.append(jobType)

		#read pipe id
		line = fd.readline().rstrip()
		pipeID = self.getInt(line)
		args.append(pipeID)

		#read job id
		line = fd.readline().rstrip()
		jobID = self.getInt(line)
		args.append(jobID)

		#read job length
		line = fd.readline().rstrip()
		jobLen = self.getFloat(line) * 60 * 60
		args.append(jobLen)

		#read job compute node count
		line = fd.readline().rstrip()
		numNode = self.getInt(line)
		args.append(numNode)

		#read output ratio
		line = fd.readline().rstrip()
		chkpMemRatio = self.getFloat(line)
		args.append(chkpMemRatio)

		#calculate checkpoint frequency
		chkpFreq = 0.0
		chkpFreq = self.calculateChkpFreq(numNode, chkpMemRatio)
		print "%f" % (chkpFreq)
		#read burst buffer number per node



	def getJobParams(self, fd, args):
		#read job type
		line = fd.readline().rstrip()
		jobType = line
		args.append(jobType)

		#read pipe id
		line = fd.readline().rstrip()
		pipeID = self.getInt(line)
		args.append(pipeID)

		#read job id
		line = fd.readline().rstrip()
		jobID = self.getInt(line)
		args.append(jobID)

		#read job length
		line = fd.readline().rstrip()
		jobLen = self.getFloat(line) * 60 * 60
		args.append(jobLen)

		#read job compute node size
		line = fd.readline().rstrip()
		numNode = self.getInt(line)
		args.append(numNode)

		#read output ratio
		line = fd.readline().rstrip()
		chkpMemRatio = self.getFloat(line)
		args.append(chkpMemRatio)

		#calculate checkpoint frequency
		chkpFreq = self.calculateChkpFreq(numNode, chkpMemRatio)
		args.append(chkpFreq)

		if self.bbArch == 0:
			#read burst buffer number per node
			thisJobInfo = JobInfo(jobType, pipeID, jobID, jobLen, numNode, self.parity, self.bbArch, chkpFreq)
		elif self.bbArch == 1 or self.bbArch == 2:
			thisJobInfo = JobInfo(jobType, pipeID, jobID, jobLen, numNode, self.parity, self.bbArch, chkpFreq)
		elif self.bbArch == 3:
			thisJobInfo = JobInfo(jobType, pipeID, jobID, jobLen, numNode, self.parity, self.bbArch, chkpFreq)
		self.jobInfo.append(thisJobInfo)
		return jobID
		
	def calculateChkpFreq(self, numNode, chkpRatio):
		#simple estimate on checkpoint frequency based on dally formula
		chkpTime = numNode * self.compNodeMem * chkpRatio / (self.bbWriteBw)
		chkpFreq = math.sqrt(2 * chkpTime * self.faultRate)
		return chkpFreq

	def getFloat(self, target):
		args = target.split(':')
		return float(args[1])

	def getInt(self, target):
		args = target.split(':')
		return int(args[1])

	def getStr(self, target):
		args = target.split(':')
		return args[1]
