import math
import time
import copy
from SimianPie.simian import *
from SimianPie.entity import Entity

#implements a pure local node
class LocalNode(Entity):
	def __init__(self, baseInfo, *args):
		super(LocalNode, self).__init__(baseInfo)
		self.totalNodeNumber = args[0]
		self.memSize = args[1]
		self.bandwidth = args[2]
		self.parity = args[3]
		self.parityOverhead = args[4]
		self.engine = args[5]
		self.localWriteBw = args[6]
		self.localReadBw = args[7]
		self.nodeID = args[8]
		self.localID = -1
		self.state = -1
		self.wakeupArgs = [-99, -1]
		self.jobID = None
		self.procID = 0
		self.stageReqName = None
		self.totalIdle = 0
		self.totalNonIdle = 0
		self.startIdle = 0
		self.endIdle = 0
		self.startNonIdle = 0
		self.endNonIdle = 0

	### Main processes ###
	def runLocal(self, this, jobID, jobNodes, jobNodeCnt, chkpRatio, chkpFreq, startMode, startCycle, restartFlag):
		self.jobID = jobID
		procID = self.procID
		finish = 0
		#print "local node %d running at %f" % (self.nodeID, self.engine.now)
		self.state = 0
		computeTime = 0
		ioTime = 0
		chkpSize = None
		#if self.parity == 1:
		#	chkpSize = (1 + (1.0/(jobNodeCnt - 1))) * self.memSize * chkpRatio
		#else:
		chkpSize = self.memSize * chkpRatio

		#print "node checkpoint size %f" % chkpSize
		#we need to read from restart when we have a parity protected
		ret = 0
		if restartFlag == 1:
			if self.localID == 0:
				print "job %d node %d start reading checkpoint" % (jobID, self.nodeID)
			ret = self.readCheckpoint(this, jobNodeCnt, chkpSize, jobID, procID)
			if ret == -1:
				finish = 1

		#print "local node %d chkp size %f, freq %f" % (self.nodeID, chkpSize, chkpFreq)
		while finish == 0:
			#compute state
			if self.state == 0:
				#schedule wakeup event
				self.reqService(self.engine.minDelay + chkpFreq, "wakeupFromCompute", [jobID, procID], "local"+str(self.nodeID), self.nodeID)
				startTime = self.engine.now
				this.hibernate(*(self.wakeupArgs))
				endTime = self.engine.now
				wakeupVal = self.wakeupArgs[0]
				if wakeupVal == -1 or wakeupVal == -3:
					if self.localID == 0:
						print "localnode %d for job %d quitting in compute at %f" % (self.nodeID, jobID, self.engine.now)
					finish = 1

				elif wakeupVal == -2:
					#wakeup from compute
					if self.localID == 0:
						print "cn %d for job %d finished compute at %f" % (self.nodeID, jobID, self.engine.now)
					pass
				self.state = 1
				computeTime = endTime - startTime
				if self.localID == 0:
					self.reqService(self.engine.minDelay, "updateJobComputeTime", computeTime, "job"+str(jobID), jobID)
					if finish == 1:
						#this compute time is wasted since there is a fault
						self.reqService(self.engine.minDelay, "updateJobWastedComputeTime", computeTime, "job"+str(jobID), jobID)
			elif self.state == 1:
				#checkpoint state
				#just compute maximum amount of wait time becaues no interference
				ioTime = chkpSize / self.localWriteBw
				#print "### checkpoint write time %f" % ioTime
				self.reqService(self.engine.minDelay + ioTime, "wakeupFromIO", [jobID, procID], "local"+str(self.nodeID), self.nodeID)
				startTime = self.engine.now
				this.hibernate(*(self.wakeupArgs))
				endTime = self.engine.now
				ioTime = endTime - startTime
				wakeupVal = self.wakeupArgs[0]

				if wakeupVal == -1 or wakeupVal == -3:
					if self.localID == 0:
						print "localNode %d for job %d ending in IO at %f" % (self.nodeID, jobID, self.engine.now)
						self.reqService(self.engine.minDelay, "updateJobWastedComputeTime", computeTime, "job"+str(jobID), jobID)

					finish = 1
						
				elif wakeupVal == -4:
					self.state = 0
					if self.localID == 0:
						print "localNode %d for job %d finished checkpointing at %f" % (self.nodeID, jobID, self.engine.now)
						self.reqService(self.engine.minDelay, "updateCycle", None, "job"+str(jobID), jobID)
				if self.localID == 0:
					bandwidth = self.memSize * chkpRatio * jobNodeCnt / (endTime - startTime)
					self.reqService(self.engine.minDelay, "recordCPBandwidth", bandwidth, "scheduler", 0)
					self.reqService(self.engine.minDelay, "updateJobIOTime", ioTime, "job"+str(jobID), jobID)
		
		self.procID = self.procID + 1

	def wakeupFromCompute(self, data, tx, txID):
		jobID = data[0]
		procID = data[1]
		if self.jobID == jobID and procID == self.procID:
			#only wakeup if it is for the same job
			procName = "runLocal"
			procStatus = self.statusProcess(procName)
			if procStatus == "suspended":
				self.wakeupArgs[0] = -2
				self.wakeProcess(procName, *(self.wakeupArgs))

	def wakeupFromIO(self, data, tx, txID):
		jobID = data[0]
		procID = data[1]
		if self.jobID == jobID and procID == self.procID:
			procName = "runLocal"
			procStatus = self.statusProcess(procName)
			if procStatus == "suspended":
				self.wakeupArgs[0] = -4
				self.wakeProcess(procName, *(self.wakeupArgs))

	def startLocalProcess(self, data, tx, txID):
		#parse args
		self.localID = data[0]
		jobID = data[1]
		jobNodeCnt = data[2]
		chkpRatio = data[3]
		chkpFreq = data[4]
		jobNodes = data[5]
		startMode = data[6]
		startCycle = data[7]
		restartFlag = data[8]
		procName = "runLocal"
		self.createProcess(procName, self.runLocal)
		self.startProcess(procName, jobID, jobNodes, jobNodeCnt, chkpRatio, chkpFreq, startMode, startCycle, restartFlag)

	def stopLocalProcess(self, data, tx, txID):
		procName = "runLocal"
		procStatus = self.statusProcess(procName)
		if procStatus == "suspended":
			self.wakeupArgs[0] = -1
			self.wakeProcess(procName, *(self.wakeupArgs))

	def cancelLocalNode(self, data, tx, txID):
		procName = "runLocal"
		procStatus = self.statusProcess(procName)
		if procStatus == "suspended":
			self.wakeupArgs[0] = -3
			self.wakeProcess(procName, *(self.wakeupArgs))

	def markNodeBusy(self, data, tx, txID):
		self.endIdle = self.engine.now
		self.startNonIdle = self.engine.now
		self.totalIdle = self.totalIdle + self.endIdle - self.startIdle
		#print "node %d marked busy. startIdle %f, endIdle %f, startNonIdle %f, endNonIdle %f, totalIdle %f, totalNonIdle %f" % (self.nodeID, self.startIdle, self.endIdle, self.startNonIdle, self.endNonIdle, self.totalIdle, self.totalNonIdle)
	
	def markNodeFree(self, data, tx, txID):
		self.endNonIdle = self.engine.now
		self.startIdle = self.engine.now
		self.totalNonIdle = self.totalNonIdle + self.endNonIdle - self.startNonIdle
		#print "node %d marked free. startIdle %f, endIdle %f, startNonIdle %f, endNonIdle %f, totalIdle %f, totalNonIdle %f" % (self.nodeID, self.startIdle, self.endIdle, self.startNonIdle, self.endNonIdle, self.totalIdle, self.totalNonIdle)

	def endNode(self, data, tx, txID):
		self.endIdle = self.engine.now
		self.totalIdle = self.endIdle - self.startIdle + self.totalIdle
		print "node %d totalIdle when simulation ends %f" % (self.nodeID, self.totalIdle)

	def getNodeStats(self, data, tx, txID):
		stats = []
		stats.append(self.totalNonIdle)
		stats.append(self.totalIdle)
		self.reqService(self.engine.minDelay, "reportNodeStats", [self.nodeID, stats], "scheduler", 0)

	### Helper Functions ###
	def readCheckpoint(self, this, jobNodeCnt, chkpSize, jobID, procID):
		retval = 0
		ioTime = chkpSize / self.localReadBw
		self.reqService(self.engine.minDelay + ioTime, "wakeupFromIO", (jobID, procID), "local"+str(self.nodeID), self.nodeID)
		start = self.engine.now
		this.hibernate(*(self.wakeupArgs))
		end = self.engine.now
		ioDuration = end - start
		wakeupVal = self.wakeupArgs[0]
		if wakeupVal == -4:
			if self.localID == 0:
				print "job %d node %d finished reading checkpoint at %f" % (jobID, self.nodeID, self.engine.now)
			retval = 0

		elif wakeupVal == -1 or wakeupVal == -3:
			if self.localID == 0:
				print "job %d node %d ending in checkpoint at %f" % (jobID, self.nodeID, self.engine.now)
			retval = -1

		if self.localID == 0:
			#update job iotime
			self.reqService(self.engine.minDelay, "updateJobIOTime", ioDuration, "job"+str(jobID), jobID)
		
		return retval

	
