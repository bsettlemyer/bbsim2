class JobInfo:
	#this class is dedicated to scheduler to keep job information
	def __init__(self, jobType, pipeID, jobID, jobLen, compNodeCnt, parity, bbArch, chkpFreq, bbPerNode=None):
		self.jobType = jobType
		self.pipeID = pipeID
		self.jobID = jobID
		self.jobLen = jobLen
		self.cycles = 0
		self.parity = parity
		self.compNodeCnt = compNodeCnt
		self.bbPerNode = bbPerNode
		self.bbNodes = None
		self.bbFaultStage = -1
		self.totalComputeTime = 0
		self.totalWastedComputeTime = 0
		self.totalIOTime = 0
		self.allocatedNodes = None
		self.stageInTime = 0
		self.stageOutTime = 0
		self.bbArch = bbArch
		self.chkpFreq = chkpFreq
		self.faultyNodes = []
	def setBBNodes(self, bbNodes):
		self.bbNodes = bbNodes
