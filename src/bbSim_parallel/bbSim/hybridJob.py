from SimianPie.simian import Simian
from SimianPie.entity import Entity
class HybridJob(Entity):
	def __init__(self, baseInfo, *args):
		super(HybridJob, self).__init__(baseInfo)
		self.totalCompNode = args[0]
		self.compNodeMem = args[1]
		self.jobType = args[2]
		self.pipeID = args[3]
		self.jobID = args[4]
		self.jobLen = args[5]
		self.jobCompNodeCnt = args[6]
		self.chkpMemRatio = args[7]
		self.chkpFreq = args[8]
		self.engine = args[9]
		self.scheduler = args[10]
		self.stageInPolicy = args[11]
		self.parity = args[12]
		#self.parityOverhead = args[13]
		self.cycles = 0
		self.totalComputeTime = 0
		self.procName = "runJob"
		self.wakeupArgs = [-99, -1]
		self.stageOutFlag = -1
		self.totalComputeTime = 0
		self.totalIOTime = 0
		self.totalWastedComputeTime = 0
		self.jobStage = 0
		self.badNode = -1
		#self.jobStat = JobStat(self.jobType, self.pipeID, self.jobID, self.job)

	def runJob(self, this, hybridNodes, startMode):
		self.badNode = -1
		#first mark nodes busy
		self.markNodesBusy(hybridNodes)
		self.stageOutFlag = -1
		restartFlag = 1
		done = 0
		self.jobStage = 0
		print "JOB %d starting with start mode %d" % (self.jobID, startMode)
		#for a local architecture, need to restage in as long as there is a fault on a node and there is no parity, we need to restart; or when there is parity but there is no commited cycles yet
		if startMode == 0 or (startMode != 0 and self.parity == 0) or (startMode != 0 and self.cycles == 0):
			self.cycles = 0
			self.jobStage = 1
			start = self.engine.now
			done = self.stageIn(this, hybridNodes)
			end = self.engine.now
			duration = end - start
			self.reqService(self.engine.minDelay, "updateJobStageInTime", [self.jobID, duration], "scheduler", 0)
			restartFlag = 0 #turn off restart flag because stage in 
			print "return value for stagein %d" % done

		if done == 0:
			#stage oue success, continue
			self.setupHybridNodes(hybridNodes, startMode, restartFlag)

		while done == 0:
			self.jobStage = 2
			this.hibernate(*(self.wakeupArgs))
			if self.wakeupArgs[0] == -99:
				done = 1
			elif self.wakeupArgs[0] == -3:
				#compute node sending progress update
				pass
			elif self.wakeupArgs[0] == -1 or self.wakeupArgs[0] == -2:
				#there is a fault in either a comp node or bb node
				print "job %d gets a fault type %d at %f" % (self.jobID, self.wakeupArgs[0], self.engine.now)
				self.cancelHybridNodes(hybridNodes)
				done = 2
		print "job %d wokenup at %f with committed cycle %d with done %d" % (self.jobID, self.engine.now, self.cycles, done)
		#stop comp nodes
		if done == 1:
			self.stopCompNodes(hybridNodes)
			print "job %d start stage out at %f" % (self.jobID, self.engine.now)
			self.jobStage = 3
			start = self.engine.now
			done = self.stageOut(this, hybridNodes)
			end = self.engine.now
			duration = end - start
			self.reqService(self.engine.minDelay, "updateJobStageOutTime", [self.jobID, duration], "scheduler", 0)

		self.markNodesFree(hybridNodes)
		#send a wakeup signal to scheduler, SEND EXIT STATUS
		self.reqService(self.engine.minDelay, "jobDoneEventHandler", [self.jobID, hybridNodes, done], "scheduler", 0)
		print "job %d quitting at %f" % (self.jobID, self.engine.now)


	### Async. Event handlers ###
	#Event to start Job process
	def startJob(self, data, tx, txID):
		print "JOB %d GOT START SIGNAL" % self.jobID
		hybridNodes = data[0]
		startMode = data[1]
		self.createProcess(self.procName, self.runJob)
		self.startProcess(self.procName, hybridNodes, startMode)

	#kill job process
	def killJob(self, data, tx, txID):
		print "job %d got kill signal at %f" % (self.jobID, self.engine.now)
		procStatus = self.statusProcess(self.procName)
		if procStatus == "suspended":
			self.wakeupArgs[0] = -99
			self.wakeProcess(self.procName, *(self.wakeupArgs))

	def pfsNotifyLocal(self, data, tx, txID):
		print "job %d got pfs done signal at %f" % (self.jobID, self.engine.now)
		procStatus = self.statusProcess(self.procName)
		if procStatus == "suspended":
			self.wakeupArgs[0] = -4
			self.wakeProcess(self.procName, *(self.wakeupArgs))

	def pfsNotifyHybrid(self, data, tx, txID):
		print "job %d got pfs done signal at %f" % (self.jobID, self.engine.now)
		procStatus = self.statusProcess(self.procName)
		if procStatus == "suspended":
			self.wakeupArgs[0] = -4
			self.wakeProcess(self.procName, *(self.wakeupArgs))

	def notifyHybridNodeFault(self, data, tx, txID):
		print "job %d got node fault at %f" % (self.jobID, self.engine.now)
		procStatus = self.statusProcess(self.procName)
		if procStatus == "suspended":
			self.wakeupArgs[0] = -1
			self.wakeupArgs[1] = data
			self.badNode = data
			self.wakeProcess(self.procName, *(self.wakeupArgs))

		self.reqService(self.engine.minDelay, "markNodeFree", None, "hybrid"+str(self.badNode), self.badNode)

	def updateCycle(self, data, tx, txID):
		self.cycles = self.cycles + 1
		print "######after cycle, job current comitted cycle %d" % self.cycles
		#also update cycle to scheduler
		self.reqService(self.engine.minDelay, "updateJobCycle", [self.jobID, self.cycles], "scheduler", 0)

	def updateJobComputeTime(self, data, tx, txID):
		self.totalComputeTime = self.totalComputeTime + data
		print "job %d udpated total compute time %f" % (self.jobID, self.totalComputeTime)
		#now send progress to scheduler
		self.reqService(self.engine.minDelay, "updateJobProgress", [self.jobID, self.totalComputeTime], "scheduler", 0)

	def updateJobIOTime(self, data, tx, txID):
		self.totalIOTime = self.totalIOTime + data
		print "job %d updated total IO Time %f" % (self.jobID, self.totalIOTime)
		#send update to scheduler
		self.reqService(self.engine.minDelay, "updateJobIOTime", [self.jobID, self.totalIOTime], "scheduler", 0)


	def updateJobWastedComputeTime(self, data, tx, txID):
		self.totalWastedComputeTime = self.totalWastedComputeTime + data 
		print "job %d update wasted compute time %f" % (self.jobID, self.totalWastedComputeTime)
		#now send update to scheduler
		self.reqService(self.engine.minDelay, "updateJobWastedComputeTime", [self.jobID, self.totalWastedComputeTime], "scheduler", 0)
 
	def rewriteJobWastedComputeTime(self, data, tx, txID):
		self.totalWastedComputeTime = data
		print "####after restart from begiinning, job %d total wasted time %f" % (self.jobID, data)

	def getJobStage(self, data, tx, txID):
		#report stage back
		self.reqService(self.engine.minDelay, "reportJobStage", [self.jobID, self.jobStage], "scheduler", 0)

	### Helper Functions ###
	def stageIn(self, this, hybridNodes):
		retval = 0
		args = []
		reqType = 4
		#if self.parity == 1:
		#	reqSize = self.compNodeMem * self.chkpMemRatio * (1 + (1/(self.jobCompNodeCnt - 1)))
		#else:
		reqSize = self.compNodeMem * self.chkpMemRatio


		args.append(reqType)
		args.append(self.jobID)
		args.append(hybridNodes)
		args.append(reqSize)
		args.append(self.jobCompNodeCnt)
		
		self.reqService(self.engine.minDelay, "pfsRequest", args, "pfs", 0)
		
		done = -1
		while done == -1:
			this.hibernate(*(self.wakeupArgs))
			if self.wakeupArgs[0] == -1 or self.wakeupArgs[0] == -99:
				#there is a node fail or job time is up, send cancel msg
				
				args[0] = -3
				print "JOB %d stage out failed" % self.jobID
				self.reqService(self.engine.minDelay, "pfsRequest", args, "pfs", 0)
				done = 1
				retval = 2
			elif self.wakeupArgs[0] == -4:
				print "job %d done stage in at %f" % (self.jobID, self.engine.now)
				done = 0
				retval = 0
		return retval

	def stageOut(self, this, hybridNodes):
		retval = 0
		args = []
		reqType = 5
		#if self.parity == 1:
		#	reqSize = self.compNodeMem * self.chkpMemRatio * (1 + (1/(self.jobCompNodeCnt - 1)))
		#else:
		reqSize = self.compNodeMem * self.chkpMemRatio
		args.append(reqType)
		args.append(self.jobID)
		args.append(hybridNodes)
		args.append(reqSize)
		args.append(self.jobCompNodeCnt)

		self.reqService(self.engine.minDelay, "pfsRequest", args, "pfs", 0)

		done = -1
		while done == -1:
			this.hibernate(*(self.wakeupArgs))
			if (self.wakeupArgs[0] == -1 and self.parity == 0):
				#node fail or job time is up, send cancel
				args[0] = -3
				self.reqService(self.engine.minDelay, "pfsRequest", args, "pfs", 0)
				done = 1
				retval = 2
			elif (self.wakeupArgs[0] == -1 and self.parity == 1):
				#one node failed in stage out but need not terminate becaues of parity protection, continue stage out
				#but need to notify scheduler that it will continue stage out
				#notify pfs this node is gone
				#badNodeID = self.wakeupArgs[1]
				print "job %d fault during stage out with parity on, continue stage out" % (self.jobID)
				#self.reqService(self.engine.minDelay, "cancelStageOutReq", None, "hybrid"+str(badNodeID), badNodeID)
			elif self.wakeupArgs[0] == -4:
				print "job %d done stage out at %f" % (self.jobID, self.engine.now)
				done = 0
				retval = 1
		return retval

	def setupHybridNodes(self, hybridNodes, startMode, restartFlag):
		for i in range(self.jobCompNodeCnt):
			args = []
			nodeName = "hybrid"+str(hybridNodes[i])
			#local ID
			args.append(i)
			#set jobID
			args.append(self.jobID)
			#set job node count
			args.append(self.jobCompNodeCnt)
			#set output ratio
			args.append(self.chkpMemRatio)
			#set chkp frequency
			args.append(self.chkpFreq)
			#send all selected hybrid nodes
			args.append(hybridNodes)
			#append start mode
			args.append(startMode)
			#append job starting cycle
			args.append(self.cycles)
			#append restart flag
			args.append(restartFlag)
			self.reqService(self.engine.minDelay, "startHybridProcess", args, nodeName, hybridNodes[i])

	def cancelHybridNodes(self, hybridNodes):
		for i in hybridNodes:
			nodeName = "hybrid"+str(i)
			self.reqService(self.engine.minDelay, "cancelHybridNode", None, nodeName, i)
		
	def stopCompNodes(self, hybridNodes):
		#send stop event to hybridnodes
		print "job %d sending stop signal to hybrid nodes at %f" % (self.jobID, self.engine.now)
		for i in range(self.jobCompNodeCnt):
			nodeName = "hybrid" + str(hybridNodes[i])
			self.reqService(self.engine.minDelay, "stopHybridProcess", None, nodeName, hybridNodes[i])

	def markNodesBusy(self, hybridNodes):
		for node in hybridNodes:
			#print "###JOB %d marking node %d busy" % (self.jobID, node)
			self.reqService(self.engine.minDelay, "markNodeBusy", None, "hybrid"+str(node), node)

	def markNodesFree(self, hybridNodes):
		if self.badNode == -1:
			for node in hybridNodes:
				#print "###JOB %d marking node %d free" % (self.jobID, node)
				self.reqService(self.engine.minDelay, "markNodeFree", None, "hybrid"+str(node), node)
		else:
			for node in hybridNodes:
				if node != self.badNode:
					self.reqService(self.engine.minDelay, "markNodeFree", None, "hybrid"+str(node), node)
