import math
import time
import copy
from SimianPie.simian import *
from SimianPie.entity import Entity

#models compute node without BB
class ComputeNode(Entity):
	def __init__(self, baseInfo, *args):
		super(ComputeNode, self).__init__(baseInfo)
		#self.engine = args[5]
		self.totalCompNode = args[0]
		#self.nodeStat = -1
		self.nodeID = args[6]
		self.localID = -1
		self.memSize = args[1]
		self.bandwidth = args[2]
		self.parity = args[3]
		self.parityOverhead = args[4]
		self.state = -1
		self.wakeupArgs = [-99]#used to pass return value from async event handlers
		self.jobID = None
		self.totalIdle = 0
		self.totalNonIdle = 0
		self.startIdle = 0
		self.endIdle = 0
		self.startNonIdle = 0
		self.endNonIdle = 0
		self.procID = 0
		self.status = -1
		self.bbArch = -1
	#################
	### Processes ###
	################
	def runCompute(self, this, jobID, compNodes, jobNodeCnt, chkpRatio, chkpFreq, bbNodes, startMode, startCycle, bbArch, bbPartSize=-1):
		self.jobID = jobID
		finish = 0
		procID = self.procID
		self.bbArch = bbArch
		#print "compute node %d running with bbPartsIZE %f" % (self.nodeID, bbPartSize)
		bbPerNode = len(bbNodes)
		self.state = 0# 0 - compute; 1 - checkpoint
		chkpSize = None
		#print "bb nodes len %d" % len(bbNodes)
		chkpSize = self.memSize * chkpRatio / (len(bbNodes))
		cycle = startCycle
		computeTime = 0
		#print "compnode %d chkp req size %f" % (self.nodeID, chkpSize)

		if (startMode == 1 and startCycle > 0) or (startMode == 2 and self.parity == 1 and startCycle > 0):
			#Must read from checkpoint
			#if self.localID == 0:
			#	print "job %d cn %d read checkpoint from bb" % (jobID, self.nodeID)
			#if jobID == 891:
			#	print "job %d cn %d read checkpoint from bb" % (jobID, self.nodeID)
			finish = self.readCheckpoint_(this, jobID, compNodes, jobNodeCnt, bbNodes, chkpSize, chkpRatio, 1, bbArch, bbPartSize)
			if finish == 1:
				#finished
				#print "job %d node %d read checkpoint failed, returning" % (jobID, self.nodeID)
				pass

		while finish == 0:
			if self.state == 0:
				cycle = cycle + 1
				#compute state
				if self.localID == 0:
				#if jobID == 891:
					print "job %d node %d start cycle %d compute phase, set to wake up node cn%d, going to sleep at %f" % (jobID, self.nodeID, cycle, self.nodeID, self.engine.now)
				self.reqService(chkpFreq, "wakeupFromCompute", jobID, "cn"+str(self.nodeID), self.nodeID)
				start = self.engine.now
				this.hibernate(*(self.wakeupArgs))
				end = self.engine.now
				wakeupVal = self.wakeupArgs[0]
				if wakeupVal == -1 or wakeupVal == -3:
					if self.localID == 0:
						print "job %d stopped at %f in cycle %d compute phase" % (jobID, self.engine.now, cycle)
					finish = 1
				elif wakeupVal == -2:
					#wakeup from compute
					if self.localID == 0:
						print "job %d finished cycle %d compute phase at %f" % (jobID, cycle, self.engine.now)
					pass
				duration = end - start
				computeTime = duration
				if self.localID == 0:
					print "###job %d compute duration %f" % (jobID, duration)
					self.reqService(self.engine.minDelay, "updateJobComputeTime", duration, "job"+str(jobID), jobID)
					if finish == 1:
						print "job %d finished during compute phase at %f" % (self.jobID, self.engine.now)
						self.reqService(self.engine.minDelay, "updateJobWastedComputeTime", computeTime, "job"+str(jobID), jobID)
				self.state = 1

			elif self.state == 1:
				#checkpoint state
				wakeupVal = -999
				start = self.engine.now
				finish = self.sharedPartitionCheckpoint(this, jobID, compNodes, jobNodeCnt, bbNodes, chkpSize, chkpRatio,0, bbArch, bbPartSize)
				end = self.engine.now
				if self.localID == 0:
					print "job %d checkpoint ret val %d at %f" % (jobID, finish, self.engine.now)
				#if jobID == 891:
				#	print "job %d node %d end %f start %f at %f" % (jobID, self.nodeID, end, start, self.engine.now)
				bandwidth = self.memSize * chkpRatio * jobNodeCnt / (end - start)
				#print "bandwidth %f" % bandwidth
				duration = end - start
				if self.localID == 0:
					#print "#### job %d checkpoint duration %f, start %f end %f" % (jobID, duration, start, end)
					self.reqService(self.engine.minDelay, "updateJobIOTime", duration, "job"+str(jobID), jobID)
					if finish == 0:
						self.reqService(self.engine.minDelay, "recordCPBandwidth", bandwidth, "scheduler", 0)
					if finish == 1:
						self.reqService(self.engine.minDelay, "updateJobWastedComputeTime", computeTime, "job"+str(jobID), jobID)
					#print "cn %d for job %d finished checkpoint phase at %f" % (self.nodeID, jobID, self.engine.now)	
				#if finish == 1 and self.localID == 0:
				#	print "cn %d for job %d stopped at %f in checkpoint phase" % (self.nodeID, jobID, self.engine.now)
				#	pass
				if self.localID == 0:
					print "job %d moving to compute states at %f with finish value %d" % (jobID, self.engine.now, finish)
				self.state = 0
		#print "cn %d for job %d existing at %f" % (self.nodeID, jobID, self.engine.now)
		self.procID = self.procID + 1


	### Async. Event Handlers ###
	def startComputeProcess(self, data, tx, txID):
		#parse arguments
		self.localID = data[0]
		jobID = data[1]
		jobNodeCnt = data[2]
		chkpRatio = data[3]
		chkpFreq = data[4]
		bbNodes = data[5]
		compNodes = data[6]
		startMode = data[7]
		startCycle = data[8]
		bbArch = data[9]
		bbPartSize = data[10]
		#start compute process
		procName = "runCompute"
		#if bbArch == 0:
		#	self.createProcess(procName, self.runCompute)
		#	self.startProcess(procName, jobID, compNodes, jobNodeCnt, chkpRatio, chkpFreq, bbNodes, startMode, startCycle)

		#elif bbArch == 3:
		#	print "starting shared partitioned"
		#	self.createProcess(procName, self.runCompute)
		#	self.startProcess(procName, jobID, compNodes, jobNodeCnt, chkpRatio, chkpFreq, bbNodes, startMode, startCycle)
		self.createProcess(procName, self.runCompute)
		self.startProcess(procName, jobID, compNodes, jobNodeCnt, chkpRatio, chkpFreq, bbNodes, startMode, startCycle, bbArch, bbPartSize)
	def cancelComputeProcess(self, data, tx, txID):
		procName = "runCompute"
		procStatus = self.statusProcess(procName)
		if procStatus == "suspended":
			self.wakeupArgs[0] = -3
			self.wakeProcess(procName, *(self.wakeupArgs))

	def stopComputeProcess(self, data, tx, txID):
		procName = "runCompute"
		procStatus = self.statusProcess(procName)
		if procStatus == "suspended":
			self.wakeupArgs[0] = -1
			self.wakeProcess(procName, *(self.wakeupArgs))

	def bbNotify(self, data, tx, txID):
		self.wakeupArgs[0] = data
		procName = "runCompute"
		procStatus = self.statusProcess(procName)
		if procStatus == "suspended":
			self.wakeProcess(procName, *(self.wakeupArgs))
	
	def notifyHeadLocalNode(self, localID, tx, txID):
		procName = "runCompute"
		procStatus = self.statusProcess(procName)
		if procStatus == "suspended":
			self.wakeupArgs[0] = localID
			self.wakeProcess(procName, *(self.wakeupArgs))

	def notifyContinue(self, data, tx, txID):
		procName = "runCompute"
		procStatus = self.statusProcess(procName)
		if procStatus == "suspended":
			self.wakeupArgs[0] = 0 
			self.wakeProcess(procName, *(self.wakeupArgs))

	def wakeupFromCompute(self, data, tx, txID):
		jobID = data
		#print "job %d in wakeupFromcompute %d" % (self.jobID, data)
		if self.jobID == jobID:
			#only wakeup if it is for the same job
			procName = "runCompute"
			procStatus = self.statusProcess(procName)
			if procStatus == "suspended":
				self.wakeupArgs[0] = -2
				self.wakeProcess(procName, *(self.wakeupArgs))

	def markNodeBusy(self, data, tx, txID):
		self.endIdle = self.engine.now
		self.startNonIdle = self.engine.now
		self.totalIdle = self.totalIdle + self.endIdle - self.startIdle

	def markNodeFree(self, data, tx, txID):
		self.endNonIdle = self.engine.now
		self.startIdle = self.engine.now
		self.totalNonIdle = self.totalNonIdle + self.endNonIdle - self.startNonIdle

	def endNode(self, data, tx, txID):
		self.endIdle = self.engine.now
		self.totalIdle = self.endIdle - self.startIdle + self.totalIdle

	def getNodeStats(self, data, tx, txID):
		stats = []
		stats.append(self.totalNonIdle)
		stats.append(self.totalIdle)
		self.reqService(self.engine.minDelay, "reportNodeStats", [self.nodeID, stats], "scheduler", 0)

	### Helper Functions ###
	def sharedCheckpoint(self, this, jobID, compNodes, jobNodeCnt, bbNodes, chkpSize):
		finish = 0
		currentBandwidth = self.bandwidth / len(bbNodes)
		for i in bbNodes:
			#send requests to each bbNode
			bbName = "bb"+str(i)
			bbReqType = 0
			args = []
			args.append(bbReqType)
			args.append(jobID)
			args.append(self.nodeID)
			args.append(chkpSize)
			args.append(currentBandwidth)
			self.reqService(self.engine.minDelay, "bbRequest", args, bbName, i)
		done = len(bbNodes)
		waitBBs = copy.deepcopy(bbNodes)
		while done != 0:
			#wait for all BB Nodes to finish
			this.hibernate(*(self.wakeupArgs))
			if self.wakeupArgs[0] == -1 or self.wakeupArgs[0] == -3:
				finish = 1
				self.cancelBBReqs(jobID, bbNodes)
				break
			elif self.wakeupArgs[0] >= 0:
				done = done - 1
				doneBB = self.wakeupArgs[0]
				if doneBB in waitBBs:
					waitBBs.remove(doneBB)
				print "compute node %d got bb %d req done at %f" % (self.nodeID, self.wakeupArgs[0], self.engine.now)
				#some bb node is done, notify there is a bandwidth change
				if done > 0:
					currentBandwidth = self.bandwidth / done
					self.updateNodeBandwidthToBBs(jobID, currentBandwidth, waitBBs)

		#nodes must synchronize before moving to the next compute cycle
		if finish != 1:
			#only need to sync when job is not yet killed or no fault
			sync = 1
			if self.localID == 0:
				#local 0 acts as coordinator. signals others to continue when it gets every done signal fromother local nodes
				while sync != jobNodeCnt:
					this.hibernate(*(self.wakeupArgs))
					if self.wakeupArgs[0] > 0:
						print "head node got done signal from %d" % self.wakeupArgs[0]
						sync = sync + 1
				#all other local nodes are done, now send continue signal
				for i in range(1, jobNodeCnt):
					print "head node sending continue signal to %d" % compNodes[i]
					self.reqService(self.engine.minDelay, "notifyContinue", self.localID, "cn"+str(compNodes[i]), compNodes[i])
			elif self.localID != 0:
				#first send done signal to local 0
				self.reqService(self.engine.minDelay, "updateCycle", None, "job"+str(jobID), jobID)
				#wait for continue signal from local 0 node
				this.hibernate(*(self.wakeupArgs))
				if self.wakeupArgs[0] == 0:
					pass

		return finish

	def sharedPartitionCheckpoint(self, this, jobID, compNodes, jobNodeCnt, bbNodes, chkpSize, chkpRatio, mode, bbArch, bbPartSize=-1):
		#mode - 0 is write; 1 is read
		finish = 0
		if self.localID == 0:
			reqSize = None
			cnBW = None
			if bbArch == 0:
				reqSize = chkpSize * jobNodeCnt
				cnBW = self.bandwidth / len(bbNodes) * jobNodeCnt
			elif bbArch == 3:
				reqSize = self.memSize * chkpRatio * bbPartSize
				#print "job %d compnode %d cpreqsize %f" % (jobID, self.nodeID, reqSize)
				cnBW = self.bandwidth * bbPartSize
				#print "node %d aggregate bandwidth %f" % (self.nodeID, cnBW)
			for i in bbNodes:
				bbName = "bb"+str(i)
				bbReqType = None
				if mode == 0:
					bbReqType = 4
				elif mode == 1:
					bbReqType = 5

				args = []
				args.append(bbReqType)
				args.append(jobID)
				args.append(jobNodeCnt)
				args.append(reqSize)
				args.append(cnBW)
				args.append(self.nodeID)
				args.append(bbPartSize)
				#print "node %d sending bb req to %d with reqType %d" % (self.nodeID, i, bbReqType)
				self.reqService(self.engine.minDelay, "bbRequest", args, bbName, i)
				
			done = len(bbNodes)
			waitBBs = copy.deepcopy(bbNodes)
			previous = self.engine.now
			now = None
			while done != 0:
				#wait for bbs
				this.hibernate(*(self.wakeupArgs))
				wakeupVal = self.wakeupArgs[0]
				if self.wakeupArgs[0] == -1 or self.wakeupArgs[0] == -3:
					finish = 1
					self.cancelBBReqsForSharedPartition(jobID, bbNodes, jobNodeCnt)
					break
				elif self.wakeupArgs[0] >= 0:
					#print "job %d node %d got a bb req done" % (jobID, self.nodeID)
					now = self.engine.now
					done = done - 1
					doneBB = self.wakeupArgs[0]
					if doneBB in waitBBs:
						waitBBs.remove(doneBB)
					#print "now - previous is %f" % (now - previous)
					if bbArch == 0 and done != 0 and (now - previous >= 3):
						#still need to inform other bbs to for bandwidth change
						currentBandwidth = self.bandwidth / done * jobNodeCnt
						self.updateNodeBandwidthToBBs(jobID, currentBandwidth, waitBBs)
					#elif bbArch == 0 and done != 0 and (now - previous < 3):
					#	print "job %d does not need to update compute node bandwidth because less than threshold" % jobID
					previous = now
			if finish != 1:
				#Need to notice other Nodes that we are done
				for i in range(1, jobNodeCnt):
					self.reqService(self.engine.minDelay, "notifyContinue", self.localID, "cn"+str(compNodes[i]), compNodes[i])
			#notify job to update cycle
			if mode == 0 and finish != 1:
				self.reqService(self.engine.minDelay, "updateCycle", None, "job"+str(jobID), jobID)
			
			##sync in time
			#this.sleep(self.engine.minDelay)
		else:
			this.hibernate(*(self.wakeupArgs))
			if self.wakeupArgs[0] == 0:
				pass
			elif self.wakeupArgs[0] == -3:
				finish = 1
		return finish
		
	def readCheckpointWithParity(self, this, jobID, compNodes, jobNodeCnt, bbNodes, chkpSize):
		retval = 0
		#first check if there are any replacement BBs
		replacement = 0
		count = 0
		for bb in bbNodes:
			if bb >= 0:
				count = count + 1

		print "compute %d bb count %d bb count without replacement %d" % (self.nodeID, (len(bbNodes)), count)

		currentBandwidth = self.bandwidth / count
		waitingBBs = []
		replaceIndx = []
		#this compute node has a replacement, need to read extra information
		#compute cost 
		indx = 0
		for bb in bbNodes:
			if bb >= 0:
				waitingBBs.append(bb)
				#only send requests to non replacements
				bbName = "bb"+str(bb)
				bbReqType = 1
				args = []
				args.append(bbReqType)
				args.append(jobID)
				args.append(self.nodeID)
				args.append(chkpSize)
				args.append(currentBandwidth)
				print "compute node %d sending bb read req to bb %d" % (self.nodeID, bb)
				self.reqService(self.engine.minDelay, "bbRequest", args, bbName, bb)
			else:
				replaceIndx.append(indx)
			indx = indx + 1

		print waitingBBs
		while count != 0:
			this.hibernate(*(self.wakeupArgs))
			if self.wakeupArgs[0] == -1 or self.wakeupArgs[0] == -3:
				#either a fault or time is up
				retval = 1
				print "there is either a fault or time up"
				self.cancelBBReqs(jobID, bbNodes)
			elif self.wakeupArgs[0] >= 0:
				print "a req done %d" % self.wakeupArgs[0]
				count = count - 1
				print "count val %d" % count
				doneBB = self.wakeupArgs[0]
				if doneBB in waitingBBs:
					waitingBBs.remove(doneBB)
				if count > 0:
					currentBandwidth = self.bandwidth / count
					self.updateNodeBandwidthToBBs(jobID, currentBandwidth, waitingBBs)
		print "recovery all req done"
		if retval != 1:
			#sucess, no synchronize
			sync = 1
			if self.localID == 0:
				while sync != jobNodeCnt:
					#0 coordinator
					this.hibernate(*(self.wakeupArgs))
					if self.wakeupArgs[0] > 0:
						#got done from a node
						sync = sync + 1
					elif self.wakeupArgs[0] < 0:
						retval = 1
						break
				if retval != -1:
					#send continue signal
					for i in range(1, jobNodeCnt):
						self.reqService(self.engine.minDelay, "notifyContinue", self.localID, "cn"+str(compNodes[i]), compNodes[i])
			elif self.localID != 0:
				#first send done signal to coordinator
				self.reqService(self.engine.minDelay, "notifyHeadLocalNode", self.localID, "cn"+str(compNodes[0]), compNodes[0])
				#wait for continue signal from local 0 node
				this.hibernate(*(self.wakeupArgs))
				if self.wakeupArgs[0] == 0:
					pass
				elif self.wakeupArgs[0] < 0:
					retval = 1

		print "recovery retval %d" % retval
		#now we must reverse replacement bb to positive for checkpoint write use
		for i in replaceIndx:
			print "before reversing sign, replacement bb is %d" % bbNodes[i]
			bbNodes[i] = bbNodes[i] * (-1)
			print "after reverse sign, now replacement bbid is %d" % bbNodes[i]
		return retval

	def readCheckpoint_(self, this, jobID, compNodes, jobNodeCnt, bbNodes, chkpSize, chkpRatio, mode, bbArch, bbPartSize):
		finish = 0
		if self.localID == 0:
			print "job %d node %d read checkpoint at %f" % (jobID, self.nodeID, self.engine.now)
		finish = self.sharedPartitionCheckpoint(this, jobID, compNodes, jobNodeCnt, bbNodes, chkpSize, chkpRatio, mode, bbArch, bbPartSize)
		return finish 

	def readCheckpoint(self, this, jobID, compNodes, jobNodeCnt, bbNodes, bbReqSize):
		retval = 0
		currentBandwidth = self.bandwidth / len(bbNodes)
		for bb in bbNodes:
			#send bb read request
			bbName = "bb"+str(bb)
			bbReqType = 1
			args = []
			args.append(bbReqType)
			args.append(jobID)
			args.append(self.nodeID)
			args.append(bbReqSize)
			args.append(currentBandwidth)
			self.reqService(self.engine.minDelay, "bbRequest", args, bbName, bb)

		done = len(bbNodes)
		outstandingBBs = copy.deepcopy(bbNodes)
		while done != 0:
			#wait for all BB Nodes to finish
			this.hibernate(*(self.wakeupArgs))
			if self.wakeupArgs[0] == -1 or self.wakeupArgs[0] == -3:
				#either a fault or time is up for this job
				retval = 1
				self.cancelBBReqs(jobID, bbNodes)
			elif self.wakeupArgs[0] >= 0:
				done = done - 1
				doneBB = self.wakeupArgs[0]
				if doneBB in outstandingBBs:
					outstandingBBs.remove(doneBB)
				#notify other on going bb reqs that there is a compute node side bandwidth change
				if done > 0:
					currentBandwidth = self.bandwidth / done
					self.updateNodeBandwidthToBBs(jobID, currentBandwidth, outstandingBBs)
		if retval != 1:
			#success, now synchronize
			sync = 1
			if self.localID == 0:
				#local 0 is coordinator
				while sync != jobNodeCnt:
					this.hibernate(*(self.wakeupArgs))
					if self.wakeupArgs[0] > 0:
						#got done signal from a comp node
						sync = sync + 1
					elif self.wakeupArgs[0] < 0:
						retval = 1
						break
				if retval != -1:
					#send continue signal when there is no fault or still have time
					for i in range(1, jobNodeCnt):
						self.reqService(self.engine.minDelay, "notifyContinue", self.localID, "cn"+str(compNodes[i]), compNodes[i])

			elif self.localID != 0:
				#first send done signal to coordinator
				self.reqService(self.engine.minDelay, "notifyHeadLocalNode", self.localID, "cn"+str(compNodes[0]), compNodes[0])
				#wait for continue signal from local 0 node
				this.hibernate(*(self.wakeupArgs))
				if self.wakeupArgs[0] == 0:
					pass
				elif self.wakeupArgs[0] < 0:
					retval = 1
		print "recovery retval %d" % retval
		return retval

	def updateNodeBandwidthToBBs(self, jobID, currentBandwidth, outstandingBBs):
		for i in outstandingBBs:
			#send updated bandwidth to other outstanding bbs
			target = "bb"+str(i)
			#print "node %d sending update bandwidth to bb %s at %f" % (self.nodeID, target, self.engine.now)
			self.reqService(self.engine.minDelay, "updateNodeBandwidth", [jobID, self.nodeID, currentBandwidth], target, i)
	
	def cancelBBReqs(self, jobID, bbNodes):
		for i in bbNodes:
			args = []
			bbName = "bb"+str(i)
			bbReqType = -1
			args.append(bbReqType)
			args.append(jobID)
			args.append(self.nodeID)
			args.append(None)
			args.append(None)
			print "cn %d sending cancel bb to bb %d" % (self.nodeID, i)
			self.reqService(self.engine.minDelay, "bbRequest", args, bbName, i)

	def cancelBBReqsForSharedPartition(self, jobID, bbNodes, jobNodeCnt):
		for bb in bbNodes:
			args = []
			bbName = "bb"+str(bb)
			bbReqType = -4
			args.append(bbReqType)
			args.append(jobID)
			args.append(self.nodeID)
			args.append(jobNodeCnt)
			args.append(None)
			print "cn %d sending cancelbb to bb %d" % (self.nodeID, bb)
			self.reqService(self.engine.minDelay, "bbRequest", args, bbName, bb)


	def testNode(self, data, tx, txID):
		print "node %d got test message" % self.nodeID


