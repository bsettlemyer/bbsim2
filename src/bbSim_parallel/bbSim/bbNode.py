from SimianPie.simian import *
from SimianPie.entity import Entity

#implements a burst buffer node
class BBNode(Entity):
	def __init__(self, baseInfo, *args):
		super(BBNode, self).__init__(baseInfo)
		#self.engine = args[3]
		self.status = 0 #0 - available; 1 - down
		self.bbID = args[0]
		self.BWs = [args[1], args[2]] #BWs[0] - write; BWs[1] - read
		self.wakeRetval = [-99]# when > 0: bandwidtn change; -1: sharecnt change
		self.nodeToExecute = dict()
		self.shareCnt = [0,0]
	### Processes ###
	def ioReq(self, this, procName, reqType, ioType, jobID, nodeID, reqSize, externalBW, origNode=-1, bbPartSize=-1):
		#iotype 0 is write, 1 is read. used to index into correct bandwidth
		#reqType 0 is from compute node, reqtype1 is from PFS ALSO nodeID is the total COMPUTE NODE COUNT FOR reqType 1
		#new Process, increase share count
		myExternalBW = externalBW
		myBandwidth = self.compareBandwidths(ioType, myExternalBW, reqType, nodeID, bbPartSize)
		#print "bb %d using reqSize %f bw %f, request type %d io type %d" % (self.bbID, reqSize, myBandwidth, reqType, ioType)
		#calculate IO time
		ioTime = reqSize / myBandwidth
		remainSize = reqSize
		start = self.engine.now
		#keep track of the right ioTIme
		correctExitTime = self.engine.now + ioTime + self.engine.minDelay
		done = 0
		#start by issuing the initial io time wake up event
		self.reqService(self.engine.minDelay + ioTime, "ioWakeup", procName, "bb"+str(self.bbID), self.bbID)
		while done == 0:
			#wait for io to finish
			#print "bb %d job %d node %d sleep for %f########" % (self.bbID, jobID, nodeID, ioTime)
			this.hibernate(*(self.wakeRetval))
			#print "bb %d job id %d node %d wake up#######" % (self.bbID, jobID, nodeID)
			ret = self.wakeRetval[0]
			#check wakeup message
			if ret == -1:
				#wakeup from scheduled IOtime event
				#check if correct time to finish
				diff = self.engine.now - correctExitTime
				#print "###diff %f; enginenow %f;  correct exit time %f" % (diff, self.engine.now, correctExitTime)
				if abs(diff) <= self.engine.minDelay:
					done = 1
					#print "bb %d job %d node %d finising at correct io time at %f" % (self.bbID, jobID, nodeID, self.engine.now)
			elif ret == -2 or ret > 0:
				#wakeup from either compute node/pfs bw change or bb share count change
				if ret > 0:
					myExternalBW = ret
				#print "bb %d job %d checking bandwidht change %f" % (self.bbID, jobID, myExternalBW)
				retval = self.updateRemainIOTime(ioType, myBandwidth, myExternalBW, remainSize, start, reqType, nodeID, bbPartSize)
				ioTime = retval[0]
				remainSize = retval[1]
				start = self.engine.now
				#print "bb %d job %d node %d new io time %f" % (self.bbID, jobID, nodeID, ioTime)
				#reschedule a new io wake up
				self.reqService(self.engine.minDelay + ioTime, "ioWakeup", procName, "bb"+str(self.bbID), self.bbID)
				correctExitTime = self.engine.now + ioTime + self.engine.minDelay
			elif ret == -3:
				#got cancel request
				done = 2#when it gets cancelled, no need to notify compute node
				#print "bb %d canceled request from job %d node %d at %f" % (self.bbID, jobID, nodeID, self.engine.now)

		target = "cn"+str(nodeID)

		#remove from node to executor
		
		if reqType == 0:
			procList = self.nodeToExecute[nodeID]
		elif reqType == 1 or reqType == 2:
			procList = self.nodeToExecute[jobID]
		if procName in procList:
			procList.remove(procName)
		if len(procList) == 0:
			#this node has no more executors here, remove this node key from dictionary
			#print "all bb req done for node id %d on bb %d" % (nodeID, self.bbID)
			if reqType == 0:
				self.nodeToExecute.pop(nodeID, None)
			elif reqType == 1:
				self.nodeToExecute.pop(jobID, None)
			elif reqType == 2:
				self.nodeToExecute.pop(jobID, None)

		if done == 1 and reqType == 0:
			#notify compute node that this bb req is done only when it finishes normally
			#print "bb %d sending done signal to node %d" % (self.bbID, nodeID)
			self.reqService(self.engine.minDelay, "bbNotify", self.bbID, target, nodeID)
		elif done == 1 and reqType == 1:
			#notify pfs bb req is done
			#print "bb req %s sending notify pfs" % procName
			self.reqService(self.engine.minDelay, "notifyPFS", [jobID, nodeID, self.bbID], "pfs", 0)

		elif done == 1 and reqType == 2:
			self.reqService(self.engine.minDelay, "bbNotify", self.bbID, "cn"+str(origNode), origNode)

		#first decrease share count
		if reqType == 0:
			self.shareCnt[ioType] = self.shareCnt[ioType] - 1
		elif reqType == 1 or reqType == 2:
			if bbPartSize == -1:
				self.shareCnt[ioType] =self.shareCnt[ioType] - nodeID
			else:
				self.shareCnt[ioType] = self.shareCnt[ioType] - bbPartSize
		#print "bb %d req %s finished, calling notifysharecnt change at %f" % (self.bbID, procName, self.engine.now)			
		self.notifyShareCntChange(procName)
		#print "bb %d job %d node %d share cnt change done and exiting" % (self.bbID, jobID, nodeID)

		
	### Async. Event Handler ###
	def ioWakeup(self, data, tx, txID):
		procName = data
		procStatus = self.statusProcess(procName)
		if procStatus == "suspended":
			self.wakeRetval[0] = -1
			#print "proc %s in iowakeup" % procName
			self.wakeProcess(procName, *(self.wakeRetval))

	def bbGoBad(self, data, tx, txID):
		self.status = -1
		print "bb %d changing stats to bad" % self.bbID

	def bbGoGood(self, data, tx, txID):
		self.status = 0
		print "bb %d chaning stats to good" % self.bbID

	def bbRequest(self, data, tx, txID):
		#get parameters
		bbReqType = data[0]
		jobID = data[1]
		nodeID = data[2]
		if self.status == -1 and bbReqType >= 0:
			#refuse this request due to bad
			#notify job checkpoint write/read will fail
			print "bb %d is down, notify job %d" % (self.bbID, jobID)
			self.reqService(self.engine.minDelay, "notifyJobBBFault", [self.bbID, 1], "job"+str(jobID), jobID)
			
		elif self.status == 0 and (bbReqType == 0 or bbReqType == 1):
			reqSize = data[3]
			cnBW = data[4]
			#0 - cp write; 1 - cp read
			self.shareCnt[bbReqType] = self.shareCnt[bbReqType] + 1
			#print "compnode bw %f" % (cnBW)
			#print "bb %d got req type %d from job %d node %d, shareCnt %d" % (self.bbID, bbReqType, jobID, nodeID, self.shareCnt)
			#checkpoint write request
			procName = "cpReq_"+str(jobID)+"_"+str(nodeID)
			self.createProcess(procName, self.ioReq)
			self.startProcess(procName, procName, 0, bbReqType, jobID, nodeID, reqSize, cnBW)

			#adding process to node-to-proc list
			procList = None
			if nodeID in self.nodeToExecute:
				#check if this compute node exists in dict
				procList = self.nodeToExecute[nodeID]
				procList.append(procName)
			else:
				procList = []
				procList.append(procName)
				self.nodeToExecute[nodeID] = procList
		
			#we need to notify other executors on this process that there is a change in share cnt
			self.notifyShareCntChange(procName)

		#### REWRITE PART
		elif self.status == 0 and (bbReqType == 2 or bbReqType == 3):
			#stage in/out for shared partition
			reqSize = data[3]
			pfsBW = data[4]
			reqName = data[5]
			compNodeCnt = data[2]
			reqSize = reqSize * compNodeCnt
			reqType = bbReqType - 2
			self.shareCnt[reqType] = self.shareCnt[reqType] + compNodeCnt
			print "bb %d got pfs req fo job %d nodeCnt %d, share cnt %d pfs bw %f reqType %d reqSize %f" % (self.bbID, jobID, compNodeCnt, self.shareCnt[reqType], pfsBW, reqType, reqSize)
			procList = None
			if jobID in self.nodeToExecute:
				procList = self.nodeToExecute[jobID]
				procList.append(reqName)
			else:
				procList = []
				procList.append(reqName)
				self.nodeToExecute[jobID] = procList
			self.createProcess(reqName, self.ioReq)
			self.startProcess(reqName, reqName, 1, reqType, jobID, compNodeCnt, reqSize, pfsBW)
			print "bb %d calling notify share cnt change due to new request %s" % (self.bbID, reqName)
			self.notifyShareCntChange(reqName)
		
		elif self.status == 0 and (bbReqType == 4 or bbReqType == 5):
			#shared partition checkpoint write, read
			reqType = bbReqType - 4
			jobNodeCnt = data[2]
			reqSize = data[3]
			cnBW = data[4]
			nodeID = data[5]
			bbPartSize = data[6]
			#print "current share cnt %d" % self.shareCnt[reqType]
			if bbPartSize == -1:
				self.shareCnt[reqType] = self.shareCnt[reqType] + jobNodeCnt
			else:
				self.shareCnt[reqType] = self.shareCnt[reqType] + bbPartSize
			#print "bb %d sharedcnt %d bbpartsize %d" % (self.bbID, self.shareCnt[reqType], bbPartSize)
			procName = "cpReq_"+str(jobID)
			#print "bb %d got req type %d for job %d at %f" % (self.bbID, reqType, jobID, self.engine.now)
			self.createProcess(procName, self.ioReq)
			self.startProcess(procName, procName, 2, reqType, jobID, jobNodeCnt, reqSize, cnBW, nodeID, bbPartSize)
			#add process to job-to-proc-list
			procList = None
			if jobID in self.nodeToExecute:
				#check if this job exists
				procList = self.nodeToExecute[jobID]
				procList.append(procName)
			else:
				procList = []
				procList.append(procName)
				self.nodeToExecute[jobID] = procList
			#print "bb %d calling notify share cnt change due to new request %s" % (self.bbID, procName)
			self.notifyShareCntChange(procName)
		#########################################################

		#elif self.status == 0 and (bbReqType == 2 or bbReqType == 3):
			#stageIn request, write
		#	reqSize = data[3]
		#	pfsBW = data[4]
		#	reqName = data[5]
		#	self.shareCnt = self.shareCnt + 1
		#	reqType = bbReqType - 2 # either 0 - write or 1 - read
			#print "bb %d got pfs req fo job %d node %d, share cnt %d pfs bw %f reqType %d" % (self.bbID, jobID, nodeID, self.shareCnt, pfsBW, reqType)
			#add process to node-to-proc list
		#	procList = None
		#	if nodeID in self.nodeToExecute:
		#		procList = self.nodeToExecute[nodeID]
		#		procList.append(reqName)
		#	else:
		#		procList = []
		#		procList.append(reqName)
		#		self.nodeToExecute[nodeID] = procList
		#	self.createProcess(reqName, self.ioReq)
		#	self.startProcess(reqName, reqName, 1, reqType, jobID, nodeID, reqSize, pfsBW)
			
		elif bbReqType == -1:
			#cancel request from compute node
			#print "bb %d got cancel request from node %d at %f" % (self.bbID, nodeID, self.engine.now) 
			self.cancelBBRequest(jobID, nodeID)

		elif bbReqType == -2:
			#cancel a single request
			self.cancelSingleBBReq(jobID, nodeID)

		elif bbReqType == -3:
			self.cancelSharedPartitionPFSReq(jobID, nodeID)
	
		elif bbReqType == -4:
			#shared partition compute node canceling bb req
			self.cancelSharedPartitionCNReq(jobID, nodeID)
		elif bbReqType == -5:
			#purely shared pfs cancel 
			self.cancelPFSRequest(jobID)

	def updateNodeBandwidth(self, data, tx, txID):
		#parse info
		jobID = data[0]
		nodeID = data[1]
		currentBandwidth = data[2]
		#find all running processes from that node.
		#print "bb %d got update bw request from job %d node %d with new bw %f" % (self.bbID, jobID, nodeID, currentBandwidth)
		if nodeID in self.nodeToExecute:
			procList = self.nodeToExecute[nodeID]
			#print procList
			for proc in procList:
				procStatus = self.statusProcess(proc)
				if procStatus == "suspended":
					#print "bb %d wake up for job %d node %d" % (self.bbID, jobID, nodeID)
					self.wakeRetval[0] = currentBandwidth
					self.wakeProcess(proc, *(self.wakeRetval))

	def updatePfsBandwidth(self, data, tx, txID):
		#parse info
		procName = data[0]
		splits = procName.split('_')
		jobID = int(splits[1])
		currentBandwidth = data[1]
		#print "bb %d got pfs update bw req for job %d with pfs bw %f" % (self.bbID, jobID, currentBandwidth)
		#find affected executor
		if jobID in self.nodeToExecute:
			#print "job %d still has running executors on bb %d" % (jobID, self.bbID)
			procList = self.nodeToExecute[jobID]
			
			for proc in procList:
				procStatus = self.statusProcess(proc)
				if procStatus == "suspended":
					#print "bb %d wakup for pfs bw update %s job %d" % (self.bbID, procName, jobID)
					self.wakeRetval[0] = currentBandwidth
					self.wakeProcess(proc, *(self.wakeRetval))

	def notifyShareCntChange(self, myProcName):
		#look for all ongoing requests
		for i in self.nodeToExecute.keys():
			procList = self.nodeToExecute[i]
			if len(procList) > 0:
				for procName in procList:
					#print procName
					if procName != myProcName:
						procStatus = self.statusProcess(procName)
						if procStatus == "suspended":
							self.wakeRetval[0] = -2
							#print "Waking up %s to change bandwidth" % procName
							self.wakeProcess(procName, *(self.wakeRetval))
			
		#print "bb %d %s notify done" % (self.bbID, myProcName)

	def getAffectedJobs(self, data, tx, txID):
		keys = self.nodeToExecute.keys()
		jobs = dict()#just used to test membership
		affectedJobs = []
		for i in keys:
			procList = self.nodeToExecute[i]
			for proc in procList:
				#extract job id
				splits = proc.split("_")
				jobID = splits[1]
				if (jobID in jobs) == False:
					jobs[jobID] = 1
					affectedJobs.append(jobID)
		#print "affected jobs on bb %d" % self.bbID
		print affectedJobs
		self.reqService(self.engine.minDelay, "receiveAffectedJobs", [self.bbID, affectedJobs], "scheduler", 0)

	def endSimulation(self, data, tx, txID):
		self.engine.endTime = data
		#print "rank %d changed end time to %f" % (self.engine.rank, self.engine.endTime)

	### Helper Functions ###
	def cancelSingleBBReq(self, jobID, nodeID):
		if jobID in self.nodeToExecute:
			procList = self.nodeToExecute[jobID]
			if len(procList) != 0:
				for proc in procList:
					print "cancelling stage out for %s" % proc
					procStat = self.statusProcess(proc)
					if procStat == "suspended":
						self.wakeRetval[0] = -3
						self.wakeProcess(proc, *(self.wakeRetval))

	def cancelSharedPartitionPFSReq(self, jobID, jobNodeCnt):
		targetID = jobID
		if targetID in self.nodeToExecute:
			procList = self.nodeToExecute[targetID]
			for proc in procList:
				procStat = self.statusProcess(proc)
				if procStat == "suspended":
					self.wakeRetval[0] = -3
					self.wakeProcess(proc, *(self.wakeRetval))

	def cancelSharedPartitionCNReq(self, jobID, nodeID):
		if jobID in self.nodeToExecute:
			procList = self.nodeToExecute[jobID]
			for proc in procList:
				procStat = self.statusProcess(proc)
				if procStat == "suspended":
					self.wakeRetval[0] = -3
					self.wakeProcess(proc, *(self.wakeRetval))


	def cancelBBRequest(self, jobID, nodeID):
		if nodeID in self.nodeToExecute:
			procList = self.nodeToExecute[nodeID]
			if len(procList) != 0:
				for proc in procList:
					#send cancel event to that process
					procStat = self.statusProcess(proc)
					if procStat == "suspended":
						self.wakeRetval[0] = -3
						self.wakeProcess(proc, *(self.wakeRetval))

	def compareBandwidths(self, reqType, externalBW, mode, cnt, bbPartSize=-1):
		#print "reqtype %d" % reqType
		myBandwidth = self.BWs[reqType] / self.shareCnt[reqType]
		if (mode == 4 or mode == 5 or mode == 2 or mode == 3) and bbPartSize == -1:
			myBandwidth = myBandwidth * cnt
			print "bb %d single bandwidth %f aggregated bandwidth %f" % (self.bbID, (self.BWs[reqType] / self.shareCnt[reqType]), myBandwidth)
		elif (mode == 4 or mode == 5 or mode == 2 or mode == 3) and bbPartSize != -1:
			myBandwidth = self.BWs[reqType]
			print "bb %d bandwidth %f is full bandwidht due to shared PARTITION" % (self.bbID, self.BWs[reqType])
		print "shared cnt %d full picked bw %f shared bw %f external bw %f" % (self.shareCnt[reqType], self.BWs[reqType], myBandwidth, externalBW)
		if externalBW < myBandwidth:
			myBandwidth = externalBW
		return myBandwidth

	def updateRemainIOTime(self, reqType, oldBW, newExternalBW, remainSize, start, mode, cnt, bbPartSize=-1):
		end = self.engine.now
		remain = abs(remainSize - ((end - start) * oldBW))
		#print "remain %f" % remain
		#print "old Bw %f new external Bw %f" % (oldBW, newExternalBW)
		newBW = self.compareBandwidths(reqType, newExternalBW, mode, cnt, bbPartSize)
		#print "new Bw %f" % newBW
		newIOTime = remain / newBW
		if newIOTime == 0:
			newIOTime = self.engine.minDelay
		return [newIOTime, remain]
		
