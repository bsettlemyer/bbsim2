from SimianPie.simian import Simian
from SimianPie.entity import Entity

class Job(Entity):
	def __init__(self, baseInfo, *args):
		super(Job, self).__init__(baseInfo)
		#self.engine = args[12]
		self.scheduler = args[9]
		self.stageInOutPolicy = args[10]
		self.parity = args[11]
		#self.parityOverhead = args[16]
		self.totalCompNode = args[0]
		self.compNodeMem = args[1]
		self.jobType = args[2]
		self.jobID = args[4]
		self.pipeID = args[3]
		self.jobLen = args[5]
		self.jobCompNodeCnt = args[6]
		self.chkpRatio = args[7]
		self.chkpFreq = args[8]
		self.availableCompNodes = None
		self.procName = "runJob"
		self.wakeupArgs = [-99, -1]
		self.cycles = 0
		self.stageOutFlag = -1
		self.totalComputeTime = 0
		self.totalIOTime = 0
		self.totalWastedComputeTime = 0
		self.jobStage = 0
		self.badNode = -1
		#self.jobStat = JobStat(self.jobType, self.pipeID, self.jobID, self.jobCompNodeCnt, self.bbPerNode)
	####################
	### Main process ###
	####################
	def runJob(self, this, compNodes, pickedBBNodes, startMode, bbArch, bbPartSize = -1):
		if self.jobID == 891:
			print "job %d got compNodes" % self.jobID
			print compNodes
		self.badNode = -1 #reset badnode at beginning
		print self.stageInOutPolicy
		print "job %d stageinout policy value %d chkp freq %f" % (self.jobID, self.stageInOutPolicy, self.chkpFreq)
		self.markNodesBusy(compNodes)
		print pickedBBNodes
		self.stageOutFlag = -1 #reset stageout indicator
		done = 0
		self.jobStage = 0
		#need to stage in when 1 - fresh start; 2 - bb fault and no parity; 3 - bb fault and there was not commited cycles
		print "job %d start mode %d" % (self.jobID, startMode)
		if (startMode == 0 or (startMode == 2 and self.parity == 0) or (self.cycles == 0)) and self.stageInOutPolicy != 0:
			#Mode 2 means job failed previously with bb fault, lost all progress
			#now job has a new set of BBs. must stage-in
			self.jobStage = 1
			self.cycles = 0
			print "job %d stage in " % self.jobID
			#self.switchBBSigns(pickedBBNodes)
			time1 = self.engine.now
			done = self.stageIn(this, compNodes, pickedBBNodes, bbArch)
			time2 = self.engine.now
			duration = time2 - time1
			self.reqService(self.engine.minDelay, "updateJobStageInTime", [self.jobID, duration], "scheduler", 0)
			startMode = 0
		else:
			print "job %d no need to stage in" % self.jobID
		print "job %d running at %f" % (self.jobID, self.engine.now)
		## set up compute nodes ##
		if done == 0:
			self.setupComputeNodes(compNodes, pickedBBNodes, startMode, bbArch, bbPartSize)

		while done == 0:
			self.jobStage = 2
			this.hibernate(*(self.wakeupArgs))
			if self.wakeupArgs[0] == -99:
				done = 1
			elif self.wakeupArgs[0] == -3:
				#compute node sending progress update
				pass
			elif self.wakeupArgs[0] == -1 or self.wakeupArgs[0] == -2 or self.wakeupArgs[0] == -5:
				#there is a fault in either a comp node or bb node
				print "job %d gets a fault type %d at %f" % (self.jobID, self.wakeupArgs[0], self.engine.now)
				self.cancelCompNodes(compNodes)
				done = 2
				if self.wakeupArgs[0] == -5:
					done = (3 + self.wakeupArgs[1]) * (-1)
		print "job %d woken up at %f with comitted cycle %d with done %d" % (self.jobID, self.engine.now, self.cycles, done)
		#Stop compute nodes
		if done == 1 and self.stageInOutPolicy != 0:
			self.stopCompNodes(compNodes)
			self.jobStage = 3
			#stageout when job ended normally
			print "stage out for job %d at %f" % (self.jobID, self.engine.now)
			self.stageOutFlag = 1
			time1 = self.engine.now
			done = self.stageOut(this, compNodes, pickedBBNodes, bbArch)
			time2 = self.engine.now
			duration = time2 - time1
			self.reqService(self.engine.minDelay, "updateJobStageOutTime", [self.jobID, duration], "scheduler", 0)
		elif done == 1:
			self.stopCompNodes(compNodes)
		elif done == 3:
			#used all time during stage in
			done = 1
		self.markNodesFree(compNodes)
		#send a wakeup signal to scheduler, MUST SEND EXIT STATUS BACK 
		self.reqService(self.engine.minDelay, "jobDoneEventHandler", [self.jobID, compNodes, done], self.scheduler, 0)
		print "job %d process quitting at %f" % (self.jobID, self.engine.now)

	############################
	### Async. Event Handler ###
	############################

	#Event to start job process
	def startJob(self, data, tx, txID):
		print "JOB %d GOT START SIGNAL!" % self.jobID
		compNodes = data[0]
		pickedBBNodes = data[1]
		startMode = data[2]
		bbArch = data[3]
		bbPartSize = None
		if bbArch == 3:
			bbPartSize = data[4]
		#print "job comp nodes"
		#print compNodes
		if bbArch == 0:
			self.createProcess(self.procName, self.runJob)
			self.startProcess(self.procName, compNodes, pickedBBNodes, startMode, bbArch)
		elif bbArch == 3:
			self.createProcess(self.procName, self.runJob)
			self.startProcess(self.procName, compNodes, pickedBBNodes, startMode, bbArch, bbPartSize)

	def pfsNotify(self, data, tx, txID):
		procStatus = self.statusProcess(self.procName)
		if procStatus == "suspended":
			self.wakeupArgs[0] = -4
			self.wakeupArgs[1] = data
			self.wakeProcess(self.procName, *(self.wakeupArgs))

	#Event to kill job process
	def killJob(self, data, tx, txID):
		#Wake up Job to end
		print "job %d got kill signal at %f" % (self.jobID, self.engine.now)
		procStatus = self.statusProcess(self.procName)
		if procStatus == "suspended":
			self.wakeupArgs[0] = -99
			self.wakeProcess(self.procName, *(self.wakeupArgs))

	def notifyJobCompNodeFault(self, data, tx, txID):
		print "job %d got fault msg at %f" % (self.jobID, self.engine.now)
		procStatus = self.statusProcess(self.procName)
		if procStatus == "suspended":
			self.wakeupArgs[0] = -1
			self.wakeupArgs[1] = data
			self.badNode = data
			self.wakeProcess(self.procName, *(self.wakeupArgs))
		#mark bad node idle
		self.reqService(self.engine.minDelay, "markNodeFree", None, "cn"+str(data), data)

	def notifyJobBBFault(self, data, tx, txID):
		procStatus = self.statusProcess(self.procName)
		if type(data) == int and procStatus == "suspended":
			self.wakeupArgs[0] = -2
			self.wakeupArgs[1] = data
			print "job %d got bb fault bb %d" % (self.jobID, self.wakeupArgs[1])
			self.wakeProcess(self.procName, *(self.wakeupArgs))
			self.reqService(self.engine.minDelay, "reportJobStage", [self.jobID, self.stageOutFlag], "scheduler", 0)
		elif type(data) == list and procStatus == "suspended":
			self.wakeupArgs[0] = -5
			self.wakeupArgs[1] = data[0]
			print "job %d encountred bb %d fault during compute phase" % (self.jobID, data[0])
			self.wakeProcess(self.procName, *(self.wakeupArgs))

	def updateCycle(self, data, tx, txID):
		self.cycles = self.cycles + 1
		print "###JOB %d commited new cycle %d" % (self.jobID, self.cycles)
		self.reqService(self.engine.minDelay, "updateJobCycle", [self.jobID, self.cycles], "scheduler", 0)

	def updateJobComputeTime(self, data, tx, txID):
		self.totalComputeTime = self.totalComputeTime + data
		print "job %d updated total Compute Time %f at %f" % (self.jobID, self.totalComputeTime, self.engine.now)
		#send update to scheduler
		self.reqService(self.engine.minDelay, "updateJobProgress", [self.jobID, self.totalComputeTime], "scheduler", 0)

	def updateJobIOTime(self, data, tx, txID):
		self.totalIOTime = self.totalIOTime + data
		print "###job %d updated total IO Time %f at %f" % (self.jobID, self.totalIOTime, self.engine.now)
		#send update to scheduler
		self.reqService(self.engine.minDelay, "updateJobIOTime", [self.jobID, self.totalIOTime], "scheduler", 0)
		
	def updateJobWastedComputeTime(self, data, tx, txID):
		self.totalWastedComputeTime = self.totalWastedComputeTime + data
		print "job %d update wasted compute time %f" % (self.jobID, self.totalWastedComputeTime)
		self.reqService(self.engine.minDelay, "updateJobWastedComputeTime", [self.jobID, self.totalWastedComputeTime], "scheduler", 0)

	def rewriteJobWastedComputeTime(self, data, tx, txID):
		self.totalWastedComputeTime = data
		print "####after restart from beginning, job %d total wasted time %f" % (self.jobID, data)

	def getJobStage(self, data, tx, txID):
		#report stage back
		self.reqService(self.engine.minDelay, "reportJobStage", [self.jobID, self.jobStage], "scheduler", 0)


	######################################

	#######################
	### Helper Function ###
	#######################
	def stageIn(self, this, compNodes, pickedBBNodes, bbArch):
		retval = 0
		args = []
		jobID = self.jobID
		reqType = None
		reqSize = None
		#if self.parity == 1 and bbArch == 0:
		#	reqSize = self.compNodeMem * self.chkpRatio * (1 + (1/(len(self.bbPerNode) - 1)))  #self.parityOverhead)
		reqSize = self.compNodeMem * self.chkpRatio
		print "job %d default reqsize %f" % (self.jobID, reqSize)
		#if bbArch == 0:
		#	#shared
		#	reqType = 0
		#	reqSize = reqSize / self.bbPerNode
		#elif bbArch == 3:
			#sharedpartition
		print "pickedbbnodes %d" % len(pickedBBNodes)
		reqType = 6
		reqSize = reqSize / len(pickedBBNodes)

		print "job %d reqSize per bb %f" % (self.jobID, reqSize)
		args.append(reqType)
		args.append(jobID)
		args.append(compNodes)
		args.append(pickedBBNodes)
		args.append(reqSize)
		self.reqService(self.engine.minDelay, "pfsRequest", args, "pfs", 0)
		print "wait for stage in to finish with bb arch %d" % bbArch
		#now wait for wakeup
		done = -1
		while done == -1:
			this.hibernate(*(self.wakeupArgs))
			if self.wakeupArgs[0] == -4:
				#got done signal
				done = 0
			elif self.wakeupArgs[0] == -1 or self.wakeupArgs[0] ==-2: 
				#there is a fault on this job or job time is up
				done = 2
				print "job %d cancel stage in and return" % self.jobID
				self.reqService(self.engine.minDelay, "pfsRequest", [-1, jobID, compNodes, pickedBBNodes], "pfs", 0)
			elif self.wakeupArgs[0] == -99:
				done = 3
				self.reqService(self.engine.minDelay, "pfsRequest", [-1, jobID, compNodes, pickedBBNodes], "pfs", 0)
			elif self.wakeupArgs[0] == -5:
				#there is a bb fault happened during compute phase
				done = (3 + self.wakeupArgs[1]) * (-1)
				self.reqService(self.engine.minDelay, "pfsRequest", [-1, jobID, compNodes, pickedBBNodes], "pfs", 0)
		retval = done
		print "stage in done with retval %d" % retval
		return retval

	def stageOut(self, this, compNodes, pickedBBNodes, bbArch):
		retval = 0
		args = []
		reqType = None
		reqSize = None
		reqSize = self.compNodeMem * self.chkpRatio

		#shared partitioned
		reqType = 7
		reqSize = reqSize / len(pickedBBNodes)

		args.append(reqType)
		args.append(self.jobID)
		args.append(compNodes)
		args.append(pickedBBNodes)
		args.append(reqSize)
		args.append(len(pickedBBNodes) * self.jobCompNodeCnt)
		self.reqService(self.engine.minDelay, "pfsRequest", args, "pfs", 0)
		
		#now wait for wakeup
		done = -1
		while done == -1:
			this.hibernate(*(self.wakeupArgs))
			if self.wakeupArgs[0] == -4:
				#got done signal
				done = 1
			elif self.wakeupArgs[0] == -2 or self.wakeupArgs[0] == -3:
				#there is a bb fault, at this point ignore compute node fault
				bbVictim = self.wakeupArgs[1]
				if self.parity == 1:
					#keep going due to parity, but cancel bb req on the faulty one
					print "in stage out, job %d sending cancel bb request to bb %d" % (self.jobID, bbVictim)
					self.reqService(self.engine.minDelay, "pfsRequest", [-2, self.jobID, compNodes, pickedBBNodes, bbVictim], "pfs", 0)
				elif self.parity == 0:
					done = 2
					print "job %d cancel stage out and return" % self.jobID
					self.reqService(self.engine.minDelay, "pfsRequest", [-1, self.jobID, compNodes, pickedBBNodes], "pfs", 0)
				if self.wakeupArgs[0] == -5:
					done = (3 + self.wakeupArgs[1]) * (-1)
		retval = done
		print "stage out done with retval %d at %f" % (retval, self.engine.now)

		return retval

	def switchBBSigns(self, pickedBBNodes):
		length = len(pickedBBNodes)
		for i in range(0, length):
			if pickedBBNodes[i] < 0:
				pickedBBNodes[i] = pickedBBNodes[i] * (-1)

	def setupComputeNodes(self, compNodes, pickedBBNodes, startMode, bbArch, bbPartSize = -1):
		for i in range(self.jobCompNodeCnt):
			bbs = None
			args = []
			nodeName = "cn"+str(compNodes[i])
			bbs = pickedBBNodes
			#set local node ID
			args.append(i)
			#set job ID
			args.append(self.jobID)
			#set job node count
			args.append(self.jobCompNodeCnt)
			#set output ratio
			args.append(self.chkpRatio)
			#set chkp frequency
			args.append(self.chkpFreq)
			#set bb nodes for this comput node
			args.append(bbs)
			#append compute node with localID 0
			args.append(compNodes)
			#append start mode
			args.append(startMode)
			#append job start cycle, used to inform compute node on restarts
			args.append(self.cycles)
			#append bbarch
			args.append(bbArch)
			#append bbPartSize
			args.append(bbPartSize)
			self.reqService(self.engine.minDelay, "startComputeProcess", args, nodeName, compNodes[i])
		print "job %d started comput nodes" % self.jobID

	def cancelCompNodes(self, compNodes):
		#first reset wakeupArgs
		print "job %d sending cancel process to compute nodes" % self.jobID
		for i in compNodes:
			nodeName = "cn"+str(i)
			self.reqService(self.engine.minDelay, "cancelComputeProcess", None, nodeName, i)

	def stopCompNodes(self, compNodes):
		#send stop event to compute nodes
		print "job %d sending stop signal to compute nodes at %f" % (self.jobID, self.engine.now)
		for i in range(self.jobCompNodeCnt):
			nodeName = "cn"+str(compNodes[i])
			self.reqService(self.engine.minDelay, "stopComputeProcess", None, nodeName, compNodes[i])
	
	def markNodesBusy(self, compNodes):
		print "###job %d marking nodes busy" % self.jobID
		for node in compNodes:
			self.reqService(self.engine.minDelay, "markNodeBusy", None, "cn"+str(node), node)

	def markNodesFree(self, compNodes):
		print "###job %d markign nodes free" % self.jobID
		#print compNodes
		if self.badNode == -1:
			for node in compNodes:
				self.reqService(self.engine.minDelay, "markNodeFree", None, "cn"+str(node), node)
		else:
			for node in compNodes:
				if node != self.badNode:
					self.reqService(self.engine.minDelay, "markNodeFree", None, "cn"+str(node), node)
