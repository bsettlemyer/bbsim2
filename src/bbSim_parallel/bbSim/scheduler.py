from computeNode import *
from bbNode import *
from job import *
from jobInfo import *
from hybridJob import *
from hybridNode import *
from SimianPie.simian import Simian
from SimianPie.entity import Entity
import random
import math
import copy
import os
import sys
import datetime

#first fit scheduler
class Scheduler(Entity):
	def __init__(self, baseInfo, *args):
		super(Scheduler, self).__init__(baseInfo)
		#self.engine = args[4]
		self.bbArch = args[0] #burst buffer architecture
		self.recoveryTime = args[1]
		self.compNodes = args[2]
		self.faultRate = args[5]
		self.compNodeStats = args[7]
		self.jobs = args[3] #job queue
		self.parity = args[6]
		self.runningJobs = dict()
		self.eap_pipes = dict()
		self.lap_pipes = dict()
		self.ston_pipes = dict()
		self.vpic_pipes = dict()
		self.bbNodes = None
		self.bbNodeStats = None
		self.bbSelect = None
		self.bbNumber = None
		self.bbPartSize = None
		self.bbPartStats = []
		self.availableCompNodes = len(self.compNodes)
		self.wakeupArgs = [-99, -99, None]
		self.wakeupEventCnt = 0
		self.wakeupMsgs = []
		self.restartMsgs = []
		self.output_prefix = None
		if self.bbArch == 0 or self.bbArch == 3:
			#shared architecture
			self.bbNodes = args[8]
			self.bbNodeStats = args[9]
			self.bbSelect = args[10]
			self.bbNumber = args[11]
			self.bbPartSize = args[12]
			self.output_prefix = args[13]
			self.bbUsage = []
		else:
			self.output_prefix = args[8]
		if self.bbArch == 3:
			numParts = len(self.compNodes) / self.bbPartSize
			for i in range(0, numParts):
				self.bbPartStats.append(0)
		self.procName = "runScheduler"
		self.createProcess(self.procName, self.runScheduler)
		self.startProcess(self.procName)
		self.outFd = open("./results/output"+"_"+self.output_prefix, "w")
		self.faultFD = open("./results/faults"+"_"+self.output_prefix, "w")
		self.progressFD = open("./results/progress"+"_"+self.output_prefix, "w")
		self.cpBandwidths = open("./results/cpBandwidth"+"_"+self.output_prefix, "w")
		print "#####scheduler rank %d" % self.engine.rank
		self.faultWakeupMsg = [-1]
		self.faultName = "faultGenerator"
		self.createProcess(self.faultName, self.faultGenerator)
		self.startProcess(self.faultName)
		self.currentHandlingFaultJob = -1
		self.faultJobs = []
		self.inRestart = 0
		self.eventMsgID = -99999999999
		print "!@@@!@!@ SCHEDULER PARITY %d" % self.parity
		self.restartingJob = dict()
	def runScheduler1(self, this):
		print "starting SCHEDULER"
		length = len(self.compNodes)
		print self.compNodes
		for i in range(0, length):
			self.reqService(self.engine.minDelay, "testNode", None, "cn"+str(i), i)
			print "sent to node %d" % i
		return

	def runScheduler(self, this):
		time1 = datetime.datetime.now()
		print "scheduler "
		entity = this.entity
		jobQueue = []
		#copy job queue to save the original copy for book keeping purposes
		self.copyJobQueue(jobQueue)
	
		done = 0
		head = 0
		#main loop
		while done == 0:
			print "HEREWERWER"
			job = jobQueue[head]
			check = self.checkPipelineJob(job)
			print "check value %d" % check
			retval = 1
			#only try to allocate current job at the head if there is no job running in the same pipeline
			if check == 0:
				#try to allocate the job
				print "trying to allcoated job %d" % job.jobID
				retval = self.scheduleToRun(job, 0)
				#print "retval %d" % retval
			
			#cannot schedule job at the head
			if retval != 0:
				#look at other jobs in the job queue
				cursor = head + 1
				ret = 1
				while cursor < len(jobQueue):
					#print "cursor %d job queue len %d" % (cursor, (len(jobQueue)))
					#try to schedule other jobs in queue
					nextJob = jobQueue[cursor]
					nextJobID = nextJob.jobID
					check = self.checkPipelineJob(nextJob)
					if check == 0:
						ret = self.scheduleToRun(nextJob, 0)
					#print "ret val %d" % ret
					if ret == 0:
						#next job in queue successfully scheduled, remove it from q
						del jobQueue[cursor]
					else:
						#next job cannot be scheduled
						cursor = cursor + 1
						#print "cursor %d" % cursor
						#print "queue len %d" % (len(jobQueue)) 

				#finished this pass, cannot schedule more job, wait for some job to finish
				#before trying to re-schedule the job at the head
				print "SCHEDULER FINISEHD THIS PASS, CANNOT SCHEDULE MORE JOBS at %f, WAIT FOR SOME JOB TO FINISH" % self.engine.now
				wait = 0
				while wait == 0:
					this.hibernate(*(self.wakeupArgs))
					ret = self.wakeupHandler(this)
					wait = ret ^ 1
					#if ret == 0:
					#	wait = 1
				print "SCHEDULER WAS NOTIFIED SOME JOB FINISHED AT %f" % self.engine.now
			#job at the head scheduled successfully
			else:
				del jobQueue[head]
				print "after deleting job head, queue len %d" % (len(jobQueue))
				if len(jobQueue) == 0:
					done = 1
		#Now wait for all job to finish
		
		print "SCHEDULER SCHEDULED ALL JOBS, WAITING FOR REMAINING JOBS TO FINISH AT %f" % self.engine.now
		while len(self.runningJobs) > 0:
			wait = 0
			while wait == 0:
				this.hibernate(*(self.wakeupArgs))
				ret = self.wakeupHandler(this)
				print "SCHEDULER WAKEUP RET VAL %d" % ret 
				wait = ret ^ 1
				#if ret == 0:
				#	wait = 1
			#####test codes#####
			if len(self.runningJobs) == 0:
				done = 1
				print "ALL JOB FINISHED AT %f" % self.engine.now
				#must manually set endTime due to simian lacking this feature...

		simEndTime = self.engine.now
		self.outFd.write("Simulated total run time:%f\n" % self.engine.now)
		self.reqService(self.engine.minDelay, "faultWakeup", 1, "scheduler", 0)
		self.endAllNodes(this)
		self.collectNodesStats(this)
		self.printJobStats()
		#for bcast in self.bcastEntities:
		#	self.reqService(self.engine.minDelay, "endSimulation", simEndTime, bcast[0], bcast[1])
		#self.engine.endTime = simEndTime
		time2 = datetime.datetime.now()
		self.outFd.write("Simulation started: %s\n" % time1)
		self.outFd.write("Simulation ended: %s\n" % time2)
		print time1
		print time2
		self.outFd.close()
		self.faultFD.close()
		self.cpBandwidths.close()
		print "scheduler exiting"
		#exit()
	#######################
	### Other Processes ###
	#######################
	def faultGenerator(self, this):
		#nextFault = 31
		#self.reqService(nextFault, "faultWakeup", 0, "scheduler", 0)
		#this.hibernate(*(self.faultWakeupMsg))
		#if self.faultWakeupMsg[0] == 1:
		#	done = 1
		#	return
		#victim = (0, 1)
		#print "bb node %d fault" % victim[1]
		#self.reqService(self.engine.minDelay, "notifyBBNodeFault", victim[1], "scheduler", 0)
		#return
		#self.reqService(self.engine.minDelay, "notifyCompNodeFault", victim[1], "scheduler", 0)

		#nextFault = 60
		#self.reqService(nextFault, "faultWakeup", 0, "scheduler", 0)
		#this.hibernate(*(self.faultWakeupMsg))
		#victim = (0,2)
		##print "comp node %d fault" % victim[1]
		#self.reqService(self.engine.minDelay, "notifyCompNodeFault", victim[1], "scheduler", 0)
		#return
		#nextFault = 16852.9
		#self.reqService(nextFault, "faultWakeup", 0, "scheduler", 0)
		#this.hibernate(*(self.faultWakeupMsg))
		#victim = (0,2)
		#print "comp node %d fault" % victim[1]
		#self.reqService(self.engine.minDelay, "notifyCompNodeFault", victim[1], "scheduler", 0)
		#return
	
		done = 0
		while done == 0:
			nextFault = random.expovariate(1/self.faultRate)
			print "***NEXT FAULT %f @#$@#$" % nextFault

			self.reqService(nextFault, "faultWakeup", 0, "scheduler", 0)
			this.hibernate(*(self.faultWakeupMsg))			
			if self.faultWakeupMsg[0] == 1:
				print "fault generator terminating"
				done = 1
				return
			#this.sleep(nextFault)
			#waking up
			victim = self.selectVictim()
			print "victim is "
			print victim
			if victim[0] == 0:
				print "comp node %d fault" % victim[1]
				#notify scheduler about compute node fault
				self.reqService(self.engine.minDelay, "notifyCompNodeFault", victim[1], "scheduler", 0)
			elif victim[0] == 1:
				print "BB node %d fault" % victim[1]
				self.reqService(self.engine.minDelay, "notifyBBNodeFault", victim[1], "scheduler", 0)
				
			#test code

	def faultWakeup(self, data, jobID, jobLen):
		if data == 0:
			print"fault wake from faultgenerator"
			self.faultWakeupMsg[0] = 0
		elif data == 1:
			print "fault wake from scheduler to terminate"
			self.faultWakeupMsg[0] = 1

		procStat = self.statusProcess("faultGenerator")
		if procStat == "suspended":
			self.wakeProcess("faultGenerator", *(self.faultWakeupMsg))

	#########################

	#############################
	### Async. Event Handlers ###
	#############################
	def jobFinishTimer(self, data, jobID, jobLen):
		print "something called job finish timer  at %f " % (self.engine.now)
		jobID = data[0]
		timestamp1 = data[1]
		timestampNow = -9999
		print "jobID %d" % jobID
		if jobID in self.runningJobs:
			timestampNow = self.runningJobs[jobID]
			print "job %d timestamp now %f" % (jobID, timestampNow)
		else:
			self.out.write( "job no longer running at %f\n" % jobID)
			return
		print "old time %d new time %d" % (timestamp1, timestampNow)
		if timestamp1 == timestampNow:
			self.out.write( "###SCHEDUELR JOB %d FINISH TIMER WAKESUP AT %f\n" % (jobID, self.engine.now))
			self.out.flush()
			#send kill job event
			target = "job"+str(jobID)
			self.reqService(self.engine.minDelay, "killJob", None, target, jobID)
		else:
			print "###SCHEDULER TRYING TO kill JOB %d WITH NEWER TIMESTAMP at %f, VOID THIS KILL" % (jobID, self.engine.now)

	def jobDoneEventHandler(self, data, tx, txID):
		self.out.write( "##SCHEDULER GOT JOB DONE EVENT at %f\n" % self.engine.now)
		self.out.flush()
		jobID = data[0]
		jobCompNodes = data[1]
		exitStat = data[2]
		#only free if exited normally free compute nodes from job
		#if exited with error, just dont free nodes for now because it will be reused immediately for the restart
		if exitStat == 1:
			print "job %d exited normally, freeing nodes at %f" % (jobID, self.engine.now)
			count = 0
			for i in jobCompNodes:
				if self.compNodeStats[i] == jobID:
					#only free compute node marked by job, if node is down, do not add it back
					self.compNodeStats[i] = -1
					count = count + 1
			print "ava nodes %d count %d" % (self.availableCompNodes, count)
			self.availableCompNodes = self.availableCompNodes + count
			if self.bbArch == 3:
				#also need to free burst buffers
				job = self.getJob(jobID)
				for bb in job.bbNodes:
					bb = abs(bb) - 1
					#print "@#$@#$ changing bb %d stats" % bb
					self.bbNodeStats[bb] = -1
			print "after job %d released, current aviable nodes are %d" % (jobID, self.availableCompNodes)
		if exitStat < 0:
			bbVictim = (exitStat * (-1)) - 3
			procStatus = self.statusProcess(self.procName)
			print "### SCHEDULER GOT JOB  %d DONE WITH DELAYED BB %d FAULT" % (jobID, bbVictim)
			if procStatus == "suspended":
				self.wakeupArgs[0] = -6
				self.wakeupArgs[1] = [jobID, bbVictim]
				self.wakeupArgs[2] = self.eventMsgID
				self.wakeupEventCnt = self.wakeupEventCnt + 1
				self.wakeupMsgs.append(self.wakeupArgs)
				self.wakeProcess(self.procName, *(self.wakeupArgs))
		else:
			if exitStat == 1:
				#ONLY REMOVE JOB FROM RUNNING LIST WHEN IT EXITED NORMALLY
				print "###JOB %d exit normally at %f, removing from running list" % (jobID, self.engine.now)
				self.runningJobs.pop(jobID, None)
				job = self.getJob(jobID)
				self.removePipeID(job)
				self.out.write("job %d finished normally at %f with chkpFreq %f\n" % (job.jobID, self.engine.now, job.chkpFreq))
				self.progressFD.write("job %d finished at %f\n" % (job.jobID, self.engine.now))
				self.progressFD.flush()
			print "job exit stat %d, now try to wak up sscheduer" % exitStat
			procStatus = self.statusProcess(self.procName)
			if procStatus == "suspended":
				self.wakeupArgs[0] = -99
				self.wakeupArgs[1] = jobID
				self.wakeupArgs[2] = self.eventMsgID
				self.wakeupEventCnt = self.wakeupEventCnt + 1
				self.wakeupMsgs.append(self.wakeupArgs)
				self.wakeProcess(self.procName, *(self.wakeupArgs))
		self.eventMsgID = self.eventMsgID + 1

	def notifyCompNodeFault(self, data, tx, txID):
		compNodeID = data
		#start recovery timer
		self.reqService(self.recoveryTime, "compNodeRecovery", compNodeID, "scheduler", 0)
		self.out.write("####SCHEDULER handling compute node %d fault at %f\n" % (compNodeID, self.engine.now))
		jobID = self.compNodeStats[compNodeID]
		self.compNodeStats[compNodeID] = -2
		if self.bbArch == 3:
			self.changeBBPartitionStats(compNodeID, 0, -2)
		if jobID >= 0:
			job = self.getJob(jobID)
			self.faultFD.write("%f %d %d %d\n" % (self.engine.now, compNodeID, job.jobID, job.compNodeCnt))
			print "#### SCHEDULER SENDING COMP NODE %d FAULT NOTIFICATION TO JOB %d" % (compNodeID, job.jobID)
			#this comp node has a job on it, notify job
			if self.bbArch == 0 or self.bbArch == 3:
				self.reqService(self.engine.minDelay, "notifyJobCompNodeFault", compNodeID, "job"+str(jobID), jobID)
			elif self.bbArch == 1:
				self.reqService(self.engine.minDelay, "notifyLocalNodeFault", compNodeID, "job"+str(jobID), jobID)
			elif self.bbArch == 2:
				self.reqService(self.engine.minDelay, "notifyHybridNodeFault", compNodeID, "job"+str(jobID), jobID)
			procStat = self.statusProcess(self.procName)
			if procStat == "suspended":
				print "### TELLING SCHEDULER TO WAKEUP TO RESTART JOB %d" % job.jobID
				self.wakeupArgs[0] = -1
				self.wakeupArgs[1] = [jobID, compNodeID]
				self.wakeupArgs[2] = self.eventMsgID
				self.eventMsgID = self.eventMsgID + 1
				self.wakeupEventCnt = self.wakeupEventCnt + 1
				self.wakeupMsgs.append(self.wakeupArgs)
				self.wakeProcess(self.procName, *(self.wakeupArgs))
			print "available nodes %d" % self.availableCompNodes
		elif jobID == -1:
			#this 
			self.faultFD.write("%d %d\n" % (-1, 0))
			if self.bbArch == 0:
				self.availableCompNodes = self.availableCompNodes - 1
			elif self.bbArch == 3:
				#entire bb partition must become unavailable
				self.availableCompNodes = self.availableCompNodes - self.bbPartSize
			print "available comp nodes %d" % self.availableCompNodes
		self.faultFD.flush()
		self.out.flush()

	def changeBBPartitionStats(self, badID, mode, stat):
		bbID = None
		retval = -1
		if mode == 0:
			#compNode is the bad node
			bbID = badID / self.bbPartSize
		elif mode == 1:
			#badID is bb
			bbID = badID
		self.out.write( "affected bb partition %d\n" % bbID)
		jobID = self.bbNodeStats[bbID]
		if (self.bbPartStats[bbID] == 0 and stat == -2) or (self.bbPartStats[bbID] == 1 and stat == -1):
			#there is no fault on this bb partition yet, we need to change the stats of all the nodes on this partition
			#from good to bad, including the bb node
			self.out.write("bb partition stat %d, bb node stat %d, stats %d\n" % (self.bbPartStats[bbID], self.bbNodeStats[bbID], stat))
			for i in range(0, self.bbPartSize):
				self.compNodeStats[bbID * self.bbPartSize + i] = stat
			if bbID == 61:
				self.out.write("changing bb %d status to %d" % (bbID, stat))
			self.bbNodeStats[bbID] = stat
		if stat == -2:
			#now we need to increment the fault count for this bb partition
			self.bbPartStats[bbID] = self.bbPartStats[bbID] + 1
		elif stat == -1:
			self.bbPartStats[bbID] = self.bbPartStats[bbID] - 1

		if self.bbPartStats[bbID] == 0:
			self.out.write("bb partition %d becomes available at %f\n" % (bbID, self.engine.now))
			retval = 0
		elif self.bbPartStats[bbID] == 1:
			#this is the first time this partition is down
			self.out.write("bb partition %d becomes unavailable at %f\n" % (bbID, self.engine.now))
			retval = 1
		self.out.flush()
		return retval

	def compNodeRecovery(self, data, tx, txID):
		compNodeID = data
		self.out.write( "compNode %d recovered at %f\n" % (compNodeID, self.engine.now))
		if self.bbArch == 0 or self.bbArch == 1 or self.bbArch == 2:
			self.compNodeStats[compNodeID] = -1
			#now change stats back for this partition
			self.availableCompNodes = self.availableCompNodes + 1
		elif self.bbArch == 3:
			retval = self.changeBBPartitionStats(compNodeID, 0, -1)
			if retval == 0:
				self.availableCompNodes = self.availableCompNodes + self.bbPartSize
		print "available nodes %d" % self.availableCompNodes

		procStat = self.statusProcess(self.procName)
		if procStat == "suspended":
			self.wakeupArgs[0] = -3
			self.wakeupArgs[1] = compNodeID
			self.wakeupArgs[2] = self.eventMsgID
			self.eventMsgID = self.eventMsgID + 1
			self.wakeupEventCnt = self.wakeupEventCnt + 1
			self.wakeupMsgs.append(self.wakeupArgs)
			self.wakeProcess(self.procName, *(self.wakeupArgs))
		self.out.flush()

	def notifyBBNodeFault(self, data, tx, txID):
		self.out.write("###scheduler got bb %d fault at %f\n" % (data+1, self.engine.now))
		victim = data
		if self.bbArch == 3:
			retval = self.changeBBPartitionStats(victim, 1, -2)
		elif self.bbArch == 0:
			self.bbNodeStats[victim] = -2
		#if retval == 1:
			#first time this partition fails
		#	self.availableCompNodes = self.availableCompNodes - self.bbPartSize
		print "available nodes after bb fault %d" % self.availableCompNodes
		self.faultFD.write("%f bb %d" % (self.engine.now, victim+1))
		self.faultFD.flush()
		self.reqService(self.engine.minDelay, "bbGoBad", victim, "bb"+str(victim+1), victim+1)
		self.reqService(self.recoveryTime, "bbNodeRecovery", victim, "scheduler", 0)
		procStat = self.statusProcess(self.procName)
		if procStat == "suspended":
			self.wakeupArgs[0] = -2
			self.wakeupArgs[1] = victim+1
			self.wakeupArgs[2] = self.eventMsgID
			self.eventMsgID = self.eventMsgID + 1
			self.wakeupEventCnt = self.wakeupEventCnt + 1
			self.wakeupMsgs.append(self.wakeupArgs)
			print "notifyings scheduler for burst buffer fault"
			self.wakeProcess(self.procName, *(self.wakeupArgs))
		self.out.flush()

	def bbNodeRecovery(self, data, tx, txID):
		bbID = data
		self.out.write("bb %d recovered at %f\n" % (bbID, self.engine.now))
		self.out.flush()
		if self.bbArch == 0:
			self.bbNodeStats[bbID] = -1
		elif self.bbArch == 3:
			retval = self.changeBBPartitionStats(data, 1, -1)
			if retval == 0:
				self.availableCompNodes = self.availableCompNodes + self.bbPartSize
		print "available nodes after BB recovery %d" % self.availableCompNodes
		print "bb %d status changed to good. sending change status event to bb node at %f" % (bbID+1, self.engine.now)
		self.reqService(self.engine.minDelay, "bbGoGood", None, "bb"+str(bbID+1), bbID+1)
		procStat = self.statusProcess(self.procName)
		if procStat == "suspended":
			self.wakeupArgs[0] = -10
			self.wakeupArgs[1] = bbID
			self.wakeupArgs[2] = self.eventMsgID
			self.eventMsgID = self.eventMsgID + 1
			self.wakeupEventCnt = self.wakeupEventCnt + 1
			self.wakeupMsgs.append(self.wakeupArgs)
			self.wakeProcess(self.procName, *(self.wakeupArgs))

	#def reportJobStage(self, data, tx, txID):
	#	jobID = data[0]
	#	jobStage = data[1]
	#	procStat = self.statusProcess(self.procName)
	#	if procStat == "suspended":
	#		self.wakeupArgs[0] = -5
	#		self.wakeupArgs[1] = [jobID, jobStage]
 	#		self.wakeupEventCnt = self.wakeupEventCnt + 1
	#		self.wakeupMsgs.append(self.wakeupArgs)
	#		self.wakeProcess(self.procName, *(self.wakeupArgs))

	def receiveAffectedJobs(self, data, tx, txID):
		bbID = data[0]
		affectedJobs = data[1]
		bundle = (bbID, affectedJobs)
		procStat = self.statusProcess(self.procName)
		if procStat == "suspended":
			self.wakeupArgs[0] = -4
			self.wakeupArgs[1] = bundle
			self.wakeupArgs[2] = self.eventMsgID
			self.eventMsgID = self.eventMsgID + 1
			print "### SCHEDULER BUNDLE"
			print bundle
			self.wakeupMsgs.append(self.wakeupArgs)
			self.wakeupEventCnt = self.wakeupEventCnt + 1
			print "notify scheduler to receive affected jobs for bb fault"
			self.wakeProcess(self.procName, *(self.wakeupArgs))


	def updateJobProgress(self, data, tx, txID):
		print "###SCHEDULER UPDATING JOB %d COMPUTE PROGRESS###" % data[0]
		jobID = data[0]
		newComputeTime = data[1]
		job = self.getJob(jobID)
		job.totalComputeTime = newComputeTime

	def updateJobCycle(self, data, tx, txID):
		jobID = data[0]
		cycles = data[1]
		print "####SCHEDULER UDPATING JOB %d COMMITTED CYCLE %d" % (jobID, cycles)
		job = self.getJob(jobID)
		job.cycles = cycles

	def updateJobWastedComputeTime(self, data, tx, txID):
		jobID = data[0]
		wastedComputeTime = data[1]
		print "####SCHEDULER UDPATING JOB %d WASTED COMPUTE TIME %f" % (jobID, wastedComputeTime)
		job = self.getJob(jobID)
		job.totalWastedComputeTime = wastedComputeTime

	def updateJobIOTime(self, data, tx, txID):
		jobID = data[0]
		totalIOTime = data[1]
		job = self.getJob(jobID)
		job.totalIOTime = totalIOTime

	def reportJobStage(self, data, tx, txID):
		#this event is only used for fault during a stage out with parity on
		#so scheduler knows not to restart this job again because of parity protection
		self.wakeupArgs[0] = -7
		self.wakeupArgs[1] = data
		self.wakeupArgs[2] = self.eventMsgID
		self.eventMsgID = self.eventMsgID + 1
		self.wakeupMsgs.append(self.wakeupArgs)
		self.wakeupEventCnt = self.wakeupEventCnt + 1
		procStat = self.statusProcess(self.procName)
		if procStat == "suspended":
			self.wakeProcess(self.procName, *(self.wakeupArgs))

	def updateJobStageInTime(self, data, tx, txID):
		jobID = data[0]
		stageInTime = data[1]
		print "###SCHEDULER updating job %d stagein time %f" % (jobID, stageInTime)
		job = self.getJob(jobID)
		job.stageInTime = job.stageInTime + stageInTime

	def updateJobStageOutTime(self, data, tx, txID):
		jobID = data[0]
		stageOutTime = data[1]
		print "###SCHEDULER updating job %d stageout time %f" % (jobID, stageOutTime)
		job = self.getJob(jobID)
		job.stageOutTime = job.stageOutTime + stageOutTime

	def reportNodeStats(self, data, tx, txID):
		nodeID = data[0]
		stats = data[1]
		print "SCHEDULER GOT NODE STAT FROM NODE %d" % nodeID
		self.wakeupArgs[0] = -8
		self.wakeupArgs[1] = [nodeID, stats]
		procStat = self.statusProcess(self.procName)
		if procStat == "suspended":
			self.wakeProcess(self.procName, *(self.wakeupArgs))
	def recordCPBandwidth(self, data, tx, txID):
		bandwidth = data
		self.cpBandwidths.write("%s\n"% bandwidth)
		self.cpBandwidths.flush()

	##########################################################################

	########################
	### Helper Functions ###
	########################
	def wakeupHandler(self, this):
		#go through all wakeup messages
		print "###SCHEDULER IN WAKEUP HANDLER"
		retval = -1
		while len(self.wakeupMsgs) != 0:
			print "########SCHEDULER current number of wakeupmsgs %d" % len(self.wakeupMsgs)
			currentMsg = self.wakeupMsgs[0]
			print "####current msg"
			print currentMsg
			del self.wakeupMsgs[0]
			self.wakeupEventCnt = self.wakeupEventCnt - 1
			#first reset wakeupArgs
			if currentMsg[0] == -99:
				#this is a normal job done signal
				print "#### SCHEDULER JOB %d FINISHED MSG" % currentMsg[1]
				retval = 0

			elif currentMsg[0] == -1:
				#wait for job to report back to which state it is in
				#this.hibernate(*(self.wakeupArgs))
				jobID = currentMsg[1][0]
				badNodeID = currentMsg[1][1]
				#self.currentHandlingFaultJob = jobID
				#self.faultJobs.append(jobID)
				#modify job time stamp
				timestamp = self.runningJobs[jobID]
				timestamp = timestamp * (-1)
				self.runningJobs[jobID] = timestamp
				print "###SCHEDULER HIBERNATE BEFORE RESTART TO WAIT FOR FAULTY JOB DONE EVENT OR STAGE OUT STATUS MESSAGE"
				self.reqService(self.engine.minDelay, "getJobStage", None, "job"+str(jobID), jobID)
				done = 0
				while done == 0:
					this.hibernate(*(self.wakeupArgs))
					#search for report job stage
					length = len(self.wakeupMsgs)
					jobStage = -1
					for i in range(0, length):
						msg = self.wakeupMsgs[i]
						if msg[0] == -7:
							#got job stage
							reportedJobID = msg[1][0]
							if reportedJobID == jobID:
								jobStage = msg[1][1]
								done = 1
								del self.wakeupMsgs[i]
								self.wakeupEventCnt = self.wakeupEventCnt - 1
								break
				print"### SCHEDULER GOT JOB %d STAGE %d" % (jobID, jobStage)
				#if jobStage != 3 or self.bbArch != 2 or self.parity != 1:
				#	#need to wait for job done event for job that do
				#	done = 0
				#	while done == 0:
				#		this.hibernate(*(self.wakeupArgs))
				#		length = len(self.wakeupMsgs)
				#		for i in range(0, length):
				#			msg = self.wakeupMsgs[i]
				#			if msg[0] == -99 and msg[1] == -jobID:
				#				#got the job done msg
				#				done = 1
				#				del self.wakeupMsgs[i]
				#				self.wakeupEventCnt = self.wakeupEventCnt - 1
				#				break

				print "###SCHEDULER HIBERNATE WAKEUP FROM EITHER A JOB DONE EVENT FOR FAULT JOB"
				#look for this job complete msg
				#go through messages queus to find 
				print "### SCHEDULER RESTART JOB %d MSG" % jobID
				if jobStage != 3:
					self.restartJob(this, jobID, 1, badNodeID)
				else:
					#jobs with fault in stage out must be handled carefully
					self.restartJob(this, jobID, 3, badNodeID)
				#self.currentHandlingFaultJob = -1

			elif currentMsg[0] == -2:
				#bb node fault
				bbNodeID = currentMsg[1]
				print "#### SCHEDULER handling bb node %d fault at %f" % (bbNodeID, self.engine.now)
				affectedJobs = None
				self.reqService(self.engine.minDelay, "getAffectedJobs", None, "bb"+str(bbNodeID), bbNodeID)
				done = 0
				while done == 0:
					this.hibernate(*(self.wakeupArgs))
					#go through messges to find if there is type -4
					self.wakeupEventCnt = self.wakeupEventCnt - 1
					length = len(self.wakeupMsgs)
					for i in range(0, length):
						msg = self.wakeupMsgs[i]
						if msg[0] == -4:
							bundle = msg[1]
							if bundle[0] == bbNodeID:
								affectedJobs = bundle[1]
								del self.wakeupMsgs[i]
								done = 1
								break
				#now notify all affected jobs
				stages = []
				for i in affectedJobs:
					#self.faultJobs.append(int(i))
					print "job affected %s" % i
					self.reqService(self.engine.minDelay, "notifyJobBBFault", bbNodeID, "job"+i, int(i))
					#now wait for reply
					this.hibernate(*(self.wakeupArgs))
					#find the msg
					length = len(self.wakeupMsgs)
					for i in range(0, length):
						msg = self.wakeupMsgs[i]
						print "msg is "
						print msg
						if msg[0] == -7:
							print "job %d stage %d" % (msg[1][0], msg[1][1])
							stages.append(msg[1][1])
							del self.wakeupMsgs[i]
							self.wakeupEventCnt = self.wakeupEventCnt - 1

				#now try to restart all affected jobs
				length = len(affectedJobs)
				#for i in aff
				for i in range(0, length):
					affectedJobs[i] = int(affectedJobs[i])
				done = 0

				while len(affectedJobs) > 0:
					jobID = affectedJobs[0]
					#self.currentHandlingFaultJob = jobID
					print "afftected job %d" % jobID
					print stages
					if stages[i] == 1 and self.parity == 1:
						affectedJobs.pop(0)
						print "job %d in stage out and have parity enabled, do not need to restart" % jobID
					else:	
						print "job %d not in stage or parity disabled, need to restart"
						timestamp = self.runningJobs[jobID]
						timestamp = timestamp * (-1)
						self.runningJobs[jobID] = timestamp
						print "restarting affected job %d" % jobID
						self.restartJob(this, jobID, 2, bbNodeID, affectedJobs)
						affectedJobs.pop(0)
					#self.currentHanldingFaultJob = -1

			elif currentMsg[0] == -3:
				#a compute node has recovered
				retval = 0
			elif currentMsg[0] == -10:
				#a bb node has recovered
				retval = 0

			elif currentMsg[0] == -6:
				#a job encountered a bb fault during compute, but discovered it during checkpoint/stageout
				jobID = currentMsg[1][0]
				bbVictim = currentMsg[1][1]	
				timestamp = self.runningJobs[jobID]
				timestamp = timestamp * (-1)
				self.runningJobs[jobID] = timestamp
				print "restarting job %d for delayed bb %d fault" % (jobID, bbVictim)
				self.restartJob(this, jobID, 2, bbVictim)
			#remove current message from queue after processing
			#del self.wakeupMsgs[0]
			#self.wakeupEventCnt = self.wakeupEventCnt - 1

		return retval

	def restartJob(self, this, jobID, restartMode, badNode=-1, affectedJobs=None):
		if jobID in self.restartingJob:
			self.restartingJob[jobID] = self.restartingJob[jobID] + 1
		else:
			self.restartingJob[jobID] = 1
		self.inRestart = 1
		#find job information first..
		target = None
		for job in self.jobs:
			if job.jobID == jobID:
				#got it
				target = job
				break
		print "###SCHEDULER trying to restart job %d, bad node %d" % (jobID, badNode)
		ret = self.scheduleToRun(target, restartMode, badNode)
		print "ret val from schedule run %d" % ret
		while ret != 0:
			print "###SCHEDULER restart failed, not enough nodes now wait"
			wait = 0
			while wait == 0:
				this.hibernate(*(self.wakeupArgs))
				print "###SCHEDULER woke up from waiting for available nodes, now calling wakupe handler"
				retval = self.wakeupHandler(this)
				#if restartMode == 1:
				#	retval = self.restartCompHandler(this, job, jobID, badNode)
				#elif restartMode == 2:
					#bb node fault
				#	retval = self.restartBBHandler(this, job, jobID, badNode, affectedJobs)
				#retval = self.restartHanlder(this)
				wait = retval ^ 1
			ret = self.scheduleToRun(target, restartMode, badNode)
		print "###SCHEDULER done job %d restarting" % jobID
		self.inRestart = 0

	def restartCompNodeHandler(self, this, job, jobID, badNode):
		retval = 1
		delete = 0
		wakeupArgs = copy.deepcopy(self.wakeupArgs)
		if wakeupArgs[0] == -99 and wakeupArgs[1] >= 0:
			#this is a job that has ended,
			#which means there are new resources being released
			retval = 0
			delete = 1
		elif wakeupArgs[0] == -3:
			retval = 0
			delete = 1
		elif wakeupArgs[0] == -10:
			retval = 0
			delete = 1
		#now handle in coming faults
		elif wakeupArgs[0] == -1:
			badJodID = wakeupArgs[1][0]
			badCompNode = wakeupArgs[1][1]
			if badJobID == jobID:
				badNode.append(badCompNode)
				delete = 1
			else:
				#this is a new bad node, we do not have to tell the job to stop because the event handler already did
				#now we just leave this message alone
				delete = 0
		elif wakeupArgs[0] == -2:
			badBB = wakeupArgs[1]
			#first check if our job uses this bad BB
			if badBB in job.bbNodes:
				#it does, so we add this to bad node with a negative sign
				#to indicate that this is a bb
				badNode.append(-badBB)
				#move this job out of the bbusage list for this bb
				self.bbUsage[badBB-1].remove(jobID)
			#now we get the affected job for this bad BB
			affectedJobs = []
			for i in self.bbUsage[badBB-1]:
				pair = (i, -1)
				affectedJobs.append(pair)
			for affected in affectedJobs:
				print "jobs afected %d" % affected
				self.reqService(self.engine.minDelay, "notifyJobBBFault", bbNodeID, "job"+str(affected), affected)
				#now wait for reply
				this.hibernate(*(self.wakeupArgs))
				#find msg
				length = len(self.wakeupMsgs)
				for i in range(0, length):
					msg = self.wakeupMsgs[i]
					if msg[0] == -7:
						print "job %d stage %d" % (msg[1][0], msg[1][1])
						for pair in affectedJobs:
							if pair[0] == msg[1][0]:
								pair[1] = msg[1][1]
								break
						del self.wakeupMsgs[i]
						self.wakeupEventCnt = self.wakeupEventCnt - 1
				#by the end of this loop, we have a list of affected jobs, not including the current
				#job that we are trying to restart, in the affecteJobs list and their job stages
			#now we make a new wakeup msg
			newArgs = []
			newArgs.append(-100, badBB, affectedJobs, self.eventMsgID)
			self.eventMsgID = self.eventMsgID + 1
			self.wakeupMsgs.append(newArgs)
			delete = 1

		if delete == 1:
			done = 0
			cursor = len(self.wakeupMsgs) - 1
			while done == 0:
				if self.wakeupMsgs[2] == wakeupArgs[2]:
					#remove this from 
					self.wakeupMsgs.pop(cursor)
					self.wakeupEventCnt = self.wakeupEventCnt - 1
		

		return retval
			

		

	def selectVictim(self):
		done = 0
		victim = None
		compNodeCnt = len(self.compNodes)
		if self.bbArch == 0 or self.bbArch == 3:
			#shared
			while done == 0:
				total = compNodeCnt + self.bbNumber
				pick = random.randrange(0, total, 1)
				if pick < compNodeCnt and self.compNodeStats[pick] != -2:
					#successfully picked a compute node victim
					done = 1
					victim = (0, pick)
				elif pick >= compNodeCnt:
					bbID = pick - compNodeCnt
					print "pick is %d %d" % (bbID, compNodeCnt)
					if self.bbNodeStats[bbID] == -1:
						done = 1
						victim = (1, bbID)
					
		elif self.bbArch == 2 or self.bbArch == 1:
			#hybrid
			while done == 0:
				pick = random.randrange(0, len(self.compNodes), 1)
				if self.compNodeStats[pick] != -2:
					done = 1
					victim = (0, pick)
					print "hybrid victim is @@@"
					print victim
		return victim

	def scheduleToRun(self, job, startMode, badNode = -1):
		retval = 0
		#print "###scheduler job %d start mode %d" % (job.jobID, startMode)
		if startMode == 0:
			#fresh start
			print "#####scheduler fresh starting job %d" % job.jobID
			retval = self.scheduleFreshStartJob(job, startMode, badNode)
		else:
			#this handles restarts
			print "###SCHEDULER RESTARTING JOB %d" % job.jobID
			retval = self.scheduleRestartJob(job, startMode, badNode)
		
		

		return retval

	def scheduleFreshStartJob(self, job, startMode, badNode=-1):
		retVal = 0
		#print "SCHEDULER TRY TO ALLOCATE NODES FOR JOB %d at %f. CURRENT AVAL NODES %d" % (job.jobID, self.engine.now, self.availableCompNodes)
		#new start job
		#if self.availableCompNodes < job.compNodeCnt:
		#	print "NOT ENOUGH NODES FOR JOB %d" % job.jobID
		#	retVal = -1
		#else:
		if True:
			#first allocate a set of nodes
			if self.bbArch != 3:
				ret = self.allocateNodes(job)
			else:
				ret = self.allocateSharedPartitionNodes(job)
			retVal = ret[0]
			allocatedNodes = ret[1]
			timestamp = 1
			jobStartMode = startMode
			jobName = "job"+str(job.jobID)
		
			if retVal == 0 and self.bbArch != 3:
				#a set of nodes are allocated, proceed
				#self.recordPipeID(job)
				job.allocatedNodes = allocatedNodes
				self.runningJobs[job.jobID] = timestamp
				if self.bbArch == 0:
					#still need to allocate BBs for shared
					allocatedBBNodes = self.allocateBurstBuffers(job.compNodeCnt)
					print "allocated BB Nodes"
					print allocatedBBNodes
					if allocatedBBNodes != None:
						job.bbNodes = allocatedBBNodes
						print "###SCHEDULER ALLCOATED COMP NODES AND BBs for job %d" % job.jobID
						self.recordPipeID(job)
						self.reqService(self.engine.minDelay, "startJob", [allocatedNodes, allocatedBBNodes, jobStartMode, self.bbArch], jobName, job.jobID)
						self.out.write("###SCHEDULER making job %d finish timer at %f\n" % (job.jobID, job.jobLen))
						self.reqService(job.jobLen, "jobFinishTimer", (job.jobID, timestamp), "scheduler", 0)

					else:
						self.deallocateNodes(job)
						retval = -1
						return retval
				#elif self.bbArch == 3:
				#	bbPartition = self.allocateBBPartition(job)
				#	job.bbNodes = bbPartition
				#	print "#### SCHEDULER ALLOCATED COMP NODES AND BB for job %d" % job.jobID
				#	self.reqService(self.engine.minDelay, "startJob", [allocatedNodes, bbPartition, jobStartMode, self.bbArch], jobName, job.jobID)
				elif self.bbArch == 1 or self.bbArch == 2:
					self.recordPipeID(job)
					print "### SCHEDULER ALLOCATED COMP NODES FOR JOB %d" % job.jobID
					self.reqService(self.engine.minDelay, "startJob", [allocatedNodes, jobStartMode], jobName, job.jobID)
					self.reqService(job.jobLen, "jobFinishTimer", (job.jobID, timestamp), "scheduler", 0)
			elif retVal == 0 and self.bbArch == 3:
				bbPartition = ret[2]
				print "BB partition for job %d" % job.jobID
				print bbPartition
				if len(bbPartition) != 0:
					self.recordPipeID(job)
					self.runningJobs[job.jobID] = timestamp
					job.allocatedNodes = allocatedNodes
					job.bbNodes = bbPartition
					print "###SCHEDULER ALLOCATED COMP NODES AND BBs for job %d" % job.jobID
					self.reqService(self.engine.minDelay, "startJob", [allocatedNodes, bbPartition, jobStartMode, self.bbArch, self.bbPartSize], jobName, job.jobID)
					self.reqService(job.jobLen, "jobFinishTimer", (job.jobID, timestamp), "scheduler", 0)
				else:
					self.deallocateNodes(job)
					print "###NOT ENOUGH BB NODES FOR JOB %d" % job.jobID
					retVal = -1
			
		return retVal

	def deallocateNodes(self, job):
		for i in job.allocatedNodes:
			self.compNodeStats[i] = -1

		job.allocatedNodes = None

	def scheduleRestartJob(self, job, startMode, badNode=-1):
		retval = 0
		timestamp = None
		if startMode == 1:
			#normal node fail,replace bad node
			badNodeIndex = None
			newNodeID = None
			if self.bbArch != 3:
				badNodeIndex = job.allocatedNodes.index(badNode)
				newNodeID = self.replaceNewNode(job.jobID)
			else:
				#Need to replace entire partition
				retval = self.replaceBBPartition(job, badNode, 0)
				newNodeID = retval
			print "####REPLACED NODE is %d" % newNodeID
			if newNodeID != -1:
				if self.bbArch != 3:
					print "bad node is %d badNodeindex %d, newNode %d" % (badNode, badNodeIndex, newNodeID)
					job.allocatedNodes[badNodeIndex] = newNodeID
				if self.restartingJob[job.jobID] > 1:
					print "### SCHEDULER restarting redundant job %d, DO NOT RESTART, redundancy %d" % (job.jobID, self.restartingJob[job.jobID])
					self.restartingJob[job.jobID] = self.restartingJob[job.jobID] - 1
					return 0
				#a new node is selected
				self.recordPipeID(job)
				jobName = "job"+str(job.jobID)
				print "###SCHEDULER RESTARTING JOB %d" % job.jobID
				procName = jobName + "timer"
				if self.bbArch == 0 or self.bbArch == 3:

					timestamp = abs(self.runningJobs[job.jobID])
					timestamp = timestamp + 1
					self.runningJobs[job.jobID] = timestamp
					if self.bbArch == 0:		
						self.reqService(self.engine.minDelay, "startJob", [job.allocatedNodes, job.bbNodes, startMode, self.bbArch], jobName, job.jobID)
					elif self.bbArch == 3:
						self.reqService(self.engine.minDelay, "startJob", [job.allocatedNodes, job.bbNodes, startMode, self.bbArch, self.bbPartSize], jobName, job.jobID)
					#shared architecture, when a compute node faults, no need to restart from beginning
					if job.cycles >= 0:
						self.out.write("#####job %d restart new duration after fault %f with total compute time %f, total wasted compute time %f\n" % (job.jobID, job.jobLen-job.totalComputeTime+job.totalWastedComputeTime, job.totalComputeTime, job.totalWastedComputeTime))
						self.reqService((job.jobLen-job.totalComputeTime+job.totalWastedComputeTime), "jobFinishTimer", (job.jobID, timestamp), "scheduler", 0)
					else:
						self.out.write( "### job %d does not have a commited cycle, restart from beginning %f\n" % (job.jobID, job.jobLen))
						self.reqService((job.jobLen), "jobFinishTimer", (job.jobID, timestamp), "scheduler", 0)

				elif self.bbArch == 2 or self.bbArch == 1:
					print "#### JOB %d cycles %d" % (job.jobID, job.cycles)	
					print "#### JOB %d total compute time %f" % (job.jobID, job.totalComputeTime)
					print "##@  JOB %d total wasted compute time %f" % (job.jobID, job.totalWastedComputeTime)
					timestamp = abs(self.runningJobs[job.jobID])
					timestamp = timestamp + 1
					self.runningJobs[job.jobID] = timestamp

					self.reqService(self.engine.minDelay, "startJob", [job.allocatedNodes, startMode], jobName, job.jobID)
					if self.parity == 0:
						#for hybrid local, if no parity, then entire progress is lost, must restart from beginning
						job.totalWastedComputeTime = job.totalComputeTime
						print "### JOB %d restarting from beginning without parity" % job.jobID
						self.reqService(self.engine.minDelay, "rewriteJobWastedComputeTime", job.totalComputeTime, "job"+str(job.jobID), job.jobID)
						self.reqService(job.jobLen, "jobFinishTimer", (job.jobID, timestamp), "scheduler", 0)
					elif self.parity == 1:
						#if there is parity, then we can restart from most recent one
						if job.cycles == 0:
							#no commited cycles, still need to restart from beginning
							self.reqService(job.jobLen, "jobFinishTimer", (job.jobID, timestamp), "scheduler", 0)
						elif job.cycles > 0:
							#there are possible committed cycles
							print "###@@#@$ JOB %d restart new duration %f" % (job.jobID, (job.jobLen-job.totalComputeTime+job.totalWastedComputeTime))
							self.reqService((job.jobLen - job.totalComputeTime+job.totalWastedComputeTime), "jobFinishTimer", (job.jobID, timestamp), "scheduler", 0)
				self.restartingJob[job.jobID] = self.restartingJob[job.jobID] - 1
				if self.restartingJob[job.jobID] == 0:
					print "### SCHEDULER successfuly restarted job %d, removing it from restartin list" % job.jobID
					self.restartingJob.pop(job.jobID, None)
			else:
				print "##SCHEDULER CANNOT FIND A GOOD REPLACED NODE FOR JOB %d" % job.jobID
				retval = -1

		elif startMode == 2:
			#replace bad bb
			ret = None
			if self.bbArch == 0:
				ret = self.replaceBadBB(job, badNode)
			elif self.bbArch == 3:
				ret = self.replaceBBPartition(job, badNode, 1)
			if ret == 0:
				if self.restartingJob[job.jobID] > 1:
					#this is a redundant one that replaced 1 group. DO NO RESTART the job!
					print "#### SCHEDULER IN redundant restart for job %d, do not restart, redundatn %d" % (job.jobID, self.restartingJob[job.jobID])
					self.restartingJob[job.jobID] = self.restartingJob[job.jobID] - 1
					return 0
				timestamp = abs(self.runningJobs[job.jobID])
				timestamp = timestamp + 1
				self.runningJobs[job.jobID] = timestamp
				self.recordPipeID(job)
				jobName = "job"+str(job.jobID)
				print "###SCHEDULER REPLACED BAD BB for JOB %d and restarting" % job.jobID
				#print "nodes"
				#print job.allocatedNodes
				if self.bbArch == 0:
					self.reqService(self.engine.minDelay, "startJob", [job.allocatedNodes, job.bbNodes, startMode, self.bbArch], jobName, job.jobID)
				elif self.bbArch == 3:
					self.reqService(self.engine.minDelay, "startJob", [job.allocatedNodes, job.bbNodes, startMode, self.bbArch, self.bbPartSize], jobName, job.jobID)
				#start timer 
				procName = jobName + "timer"
				if (self.parity == 1 and job.cycles == 0) or (self.parity == 0):
					#parity on but no committed cycles
					job.totalWastedComputeTime = job.totalComputeTime
					print "####scheuler restarting job %d from beginning" % job.jobID
					self.reqService((job.jobLen), "jobFinishTimer", (job.jobID, timestamp), "scheduler", 0)
				elif self.parity == 1 and job.cycles > 0:
					print "### scheduler restarting job %d with new remain time %f" % (job.jobID, job.jobLen - job.totalComputeTime+job.totalWastedComputeTime)
					self.reqService((job.jobLen - job.totalComputeTime+job.totalWastedComputeTime), "jobFinishTimer", (job.jobID, timestamp), "scheduler", 0)
				self.restartingJob[job.jobID] = self.restartingJob[job.jobID] - 1
				if self.restartingJob[job.jobID] == 0:
					print "### SCHEDULER sucessfully restart job %d, removing it from restarting list" % job.jobID
					self.restartingJob.pop(job.jobID, None)
			else:
				print "##SCHEDULER CANNOT FIND A GOOD BB FOR JOB %d" % job.jobID
				retval = -1

		elif startMode == 3:
			#a node failed during stage out
			if self.bbArch == 0 or self.bbArch == 3:
				#no need to  restart because, compute node is not in use, just bb
				self.restartingJob[job.jobID] = self.restartingJob[job.jobID] - 1
				if self.restartingJob[job.jobID] == 0:
					self.restartingJob.pop(job.jobID, None)
				retval = 0

			elif self.bbArch == 1 or self.bbArch == 2:
				if self.parity == 0:
					#first replace bad node
					badNodeIndex = job.allocatedNodes.index(badNode)
					newNodeID = self.replaceNewNode(job.jobID)
					if newNodeID != -1:
						if self.restartingJob[job.jobID] > 1:
							print "###Scheduler in redundant restart job %d no need to restart " % job.jobID
							self.restartingJob[job.jobID] = self.restartingJob[job.jobID] - 1
							return 0
						print "###REPLACED NODE  to %d" % newNodeID
						job.allocatedNodes[badNodeIndex] = newNodeID
						timestamp = abs(self.runningJobs[job.jobID])
						timestamp = timestamp + 1
						self.runningJobs[job.jobID]= timestamp
						
						#Need to restart from the beginning..
						job.totalWastedComputeTime = job.totalComputeTime
						self.reqService(self.engine.minDelay, "startJob", [job.allocatedNodes, startMode], "job"+str(job.jobID), job.jobID)
						print "### JOB %d faulted in stage out restarting from beginning due to no parity" % job.jobID
						self.reqService(self.engine.minDelay, "rewriteJobWastedComputeTime", job.totalComputeTime, "job"+str(job.jobID), job.jobID)
						self.reqService(job.jobLen, "jobFinishTimer", (job.jobID, timestamp), "scheduler", 0)
						self.restartingJob[job.jobID] = self.restartingJob[job.jobID] - 1
						if self.restartingJob[job.jobID] == 0:
							self.restartingJob.pop(job.jobID, None)
					else:
						print "###SCHEDULER CANOT FIND A GOOD REPLACED NODE FOR JOB %d" % job.jobID
						retval =  -1

				elif self.parity == 1:
					#can continue due to parity
					print "job %d can continue due to parity" % job.jobID
					self.restartingJob[job.jobID] = self.restartingJob[job.jobID] - 1
					retval = 0

		print "restart ret val %d" % retval
		return retval


	def replaceBBPartition(self, job, badNode, mode):
		badBB = None
		retval = 0
		if mode == 0:
			#compute node failure, find bb partition based on compute node
			badBB = badNode / self.bbPartSize
			#$self.bbNodeStats[badBB] = -2
		elif mode == 1:
			#bb fail.
			badBB = badNode - 1
		##mark all compute nodes unavailable
		#for i in range(0, self.bbPartSize):
		#	self.compNodeStats[badBB * self.bbPartSize + i] = -2
		
		#find a new bbPartition
		cursor = 0
		done = 0
		print "bb node stats"
		print self.bbNodeStats
		while done == 0 and cursor < len(self.bbNodeStats):
			if self.bbNodeStats[cursor] == -1:
				#now check if all compute nodes are available
				nodeCount = 0
				for j in range(0, self.bbPartSize):
					if self.compNodeStats[self.bbPartSize * cursor + j] == -1:
						nodeCount = nodeCount + 1
				if nodeCount == self.bbPartSize:
					done = 1
					break
			cursor = cursor + 1
		
		if done == 1:
			print "bad bbPartition %d, good bb Partition %d" % (badBB, cursor)
			#we have a replacement
			badIndex = job.bbNodes.index(badBB+1)
			job.bbNodes[badIndex] = cursor + 1
			print "bad bb partition %d, good bb Partition %d, bad bb index %d" % (badBB, cursor, badIndex) 
			#now replace the compute nodes
			for i in range(0, self.bbPartSize):
				badComp = badBB * self.bbPartSize + i
				replaceComp = cursor * self.bbPartSize + i
				badCompIndx = job.allocatedNodes.index(badComp)
				job.allocatedNodes[badCompIndx] = replaceComp
				self.compNodeStats[replaceComp] = job.jobID
				#self.compNodeStats[badComp] = -2
			#now validate
			
		else:
			print "no more available bb partition"
			retval = -1
		return retval
		
	def replaceBadBB(self, job, badBB):
		print "in replace BB for job %d bad bb is %d" % (job.jobID, badBB)
		print job.bbNodes
		retval = 0
		badIndex = job.bbNodes.index(badBB)
		#replace bb
		cursor = 0
		done = 0
		while done == 0 and cursor < len(self.bbNodeStats):
			if self.bbArch == 3:
				if self.bbNodeStats[cursor] == -1:
					self.bbNodeStats[cursor] = job.jobID
					job.bbNodes[badIndex] = cursor+1
					done = 1
					print "### SHAREDPARTITION replaced bb %d" % (cursor+1)
			elif self.bbArch == 0:
				if self.bbNodeStats[cursor] == -1 and (not((cursor+1) in job.bbNodes)):
					job.bbNodes[badIndex] = cursor + 1
					done = 1
				elif self.bbNodeStats[cursor] == -1 and ((cursor+1) in job.bbNodes) and (cursor + 1 == badBB):
					job.bbNodes[badIndex] = cursor + 1
					print "replaced bb node is %d" % (cursor + 1)
					done = 1
			cursor = cursor + 1
		if done == 1:
			retval = 0
		else:
			retval = -1
		return retval
		
	def replaceNewNode(self, jobID):
		cursor = 0
		done = 0
		length = len(self.compNodeStats)
		cursor = 0
		found = 0
		retVal = -1
		while done == 0 and cursor < length:
			if self.compNodeStats[cursor] == -1:
				done = 1
				found = 1
				break
			cursor = cursor + 1

		if found == 1:
			self.compNodeStats[cursor] = jobID
			self.availableCompNodes = self.availableCompNodes - 1
			retVal = cursor

		return retVal


	def checkBBNodes(self, job):
		retval = 0
		for perNodeBBs in job.bbNodes:
			for bb in perNodeBBs:
				if self.bbNodeStats[bb] != 0:
					retval = -1
					break
			if retval == -1:
				break
		return retval
					

	def allocateSharedPartitionNodes(self, job):
		#must gurantee to find good bb partitions before starting the job
		#this is a coupled failure domain, if ANY of the compute nodes or the bb node
		#for this partition has a fault, this entire partition, compute nodes AND bb
		#becomes unavailable
		retval = -1
		allocated = 0
		cursor = 0
		bbPartIndices = []
		length = len(self.compNodes)
		bbLen = len(self.bbNodeStats)
		compNodeIndices = []
		#instead of finding compute nodes, we find first available bb Nodes
		numPartitions = job.compNodeCnt / self.bbPartSize
		#print "bb node stats"
		#print self.bbNodeStats
		while allocated != numPartitions and cursor < bbLen:
			#print "bb Node %d Stats %d" % (cursor, self.bbNodeStats[cursor])
			if self.bbNodeStats[cursor] == -1:
				#also check all the compute nodes for this bb partition
				nodeCnt = 0
				for j in range(0, self.bbPartSize):
					#print "comp node %d stat %d" % (cursor * self.bbPartSize + j, self.compNodeStats[cursor * self.bbPartSize + j]) 
					if self.compNodeStats[cursor * self.bbPartSize + j] == -1:
						nodeCnt = nodeCnt + 1
						#print "nodeCnt %d" % nodeCnt
				if nodeCnt == self.bbPartSize:
					bbPartIndices.append(cursor+1)
					allocated = allocated + 1
				#print "allocated %d" % allocated
			cursor = cursor + 1
		#print "allocated %d numPartition %d" % (allocated, numPartitions)
		if allocated == numPartitions:
			#since we have available BBs, AND available BBs imply that
			#ALL compute nodes for this partition is available, we can just go
			#and give the compute nodes to this job
			retval = 0
			for i in bbPartIndices:
				#print "allocated bb %d" % (i-1)
				self.bbNodeStats[i-1] = job.jobID
				for j in range(0, self.bbPartSize):
					#generate corresponding compute nodes
					compNode = (i - 1) * self.bbPartSize + j
					compNodeIndices.append(compNode)
					#print "compNode %d allocated to job %d with bb %d" % (compNode, job.jobID, i-1)
					self.compNodeStats[compNode] = job.jobID
			self.availableCompNodes = self.availableCompNodes - job.compNodeCnt
			#print "available comp node cnt after allocation %d" % self.availableCompNodes
		#return both the compute nodes and the bb Partitions
		return [retval, compNodeIndices, bbPartIndices]

	def allocateNodes(self, job):
		retval = 1
		allocated = 0
		cursor = 0
		indices = []
		length = len(self.compNodes)
		#find potential available nodes
		while allocated != job.compNodeCnt and cursor < length:
			if self.compNodeStats[cursor]  == -1:
				indices.append(cursor)
				allocated = allocated + 1
			cursor = cursor + 1

		#enough available nodes?
		if allocated == job.compNodeCnt:
			#enough nodes
			retval = 0
			for i in range(allocated):
				self.compNodeStats[indices[i]] = job.jobID#assign job to compute node
			self.availableCompNodes = self.availableCompNodes - allocated
		return [retval, indices]

				
	def allocateBurstBuffers(self, jobNodeCnt):
		jobBBNodes = None
		jobBBNodes = self.randomBBSelect(jobNodeCnt)
		return jobBBNodes

	def allocateBBPartition(self, job):
		#first select partition
		totalNodeNumber = len(self.compNodes)
		totalBBNumber = len(self.bbNodes)
		print "job comp %d  total num %d totalbbNumber %d" % (job.compNodeCnt, totalNodeNumber, totalBBNumber)
		bbPartitionSize = math.floor(((job.compNodeCnt / (totalNodeNumber*1.0)) * totalBBNumber))
		print "bbPartition size %d" % bbPartitionSize
		if bbPartitionSize <= 1:
			bbPartitionSize = 2
		#now pick available BBNodes
		done = 0
		cursor = 0
		bbPartition = []
		done = bbPartitionSize
		cursor = 0
		while done != 0 and cursor < len(self.bbNodeStats):
			if self.bbNodeStats[cursor] == -1:
				self.bbNodeStats[cursor] = job.jobID
				bbPartition.append(cursor+1)
				done = done - 1
			cursor = cursor + 1
		if done != 0:
			#did not find enough nodes
			bbPartition = []
		print "### SCHEDULER ALLOCATED BB PARITION FOR JOB %d" % job.jobID
		print bbPartition
		return bbPartition

	def randomBBSelect(self, jobNodeCnt):
		#just pick as many as possible
		bbs = []
		done = len(self.bbNodeStats)
		cursor = 0
		while done != 0 and cursor < len(self.bbNodeStats):
			if self.bbNodeStats[cursor] == -1:
				bbs.append(cursor+1)
				done = done - 1
			cursor = cursor + 1
		return bbs


	def copyJobQueue(self, jobQueue):
		for i in self.jobs:
			jobQueue.append(i)
			

	def checkPipelineJob(self, job):
		retval = 0
		#jobType = job[0]
		#pipeID = job[1]
		if job.jobType == "EAP":
			if job.pipeID in self.eap_pipes.keys():
				retval = 1
		elif job.jobType == "LAP":
			if job.pipeID in self.lap_pipes.keys():
				retval = 1
		elif job.jobType == "STON":
			if job.pipeID in self.ston_pipes.keys():
				retval = 1
		elif job.jobType == "VPIC":
			if job.pipeID in self.vpic_pipes.keys():
				retval = 1

		return retval


	def recordPipeID(self, job):
		#jobType = job[0]
		#pipeID = job[1]
		if job.jobType == "EAP":
			self.eap_pipes[job.pipeID] = 1
		elif job.jobType == "LAP":
			self.lap_pipes[job.pipeID] = 1
		elif job.jobType == "STON":
			self.ston_pipes[job.pipeID] = 1
		elif job.jobType == "VPIC":
			self.vpic_pipes[job.pipeID] = 1


	def removePipeID(self, job):
		#jobType = job[0]
		#pipeID = job[1]
		if job.jobType == "EAP":
			del self.eap_pipes[job.pipeID]
		elif job.jobType == "LAP":
			del self.lap_pipes[job.pipeID]
		elif job.jobType == "STON":
			del self.ston_pipes[job.pipeID]
		elif job.jobType == "VPIC":
			del self.vpic_pipes[job.pipeID]

	def getJob(self, jobID):
		job = None
		for i in self.jobs:
			if i.jobID == jobID:
				job = i
				break

		return job

	def endAllNodes(self, this):
		length = len(self.compNodes)
		for i in range(0, length):
			if self.bbArch == 0 or self.bbArch == 3:
				self.reqService(self.engine.minDelay, "endNode", None, "cn"+str(i), i)
			elif self.bbArch == 1:
				self.reqService(self.engine.minDelay, "endNode", None, "local"+str(i), i)
			elif self.bbArch == 2:
				self.reqService(self.engine.minDelay, "endNode", None, "hybrid"+str(i), i)


	def collectNodesStats(self, this):
		#send inquery message to each node
		totalIdle = 0
		totalNonIdle = 0
		length = len(self.compNodes)
		print self.compNodes
		for i in range(0, length):
			if self.bbArch == 0 or self.bbArch == 3:
				self.reqService(self.engine.minDelay, "getNodeStats", None, "cn"+str(i), i)
				print "sending getnodeStats to %s" % ("cn"+str(i))
			elif self.bbArch == 1:
				self.reqService(self.engine.minDelay, "getNodeStats", None, "local"+str(i), i)
			elif self.bbArch == 2:
				self.reqService(self.engine.minDelay, "getNodeStats", None, "hybrid"+str(i), i)


		done = len(self.compNodes)
		print "### COLLECT NODE STATS done %d" % done
		while done != 0:
			this.hibernate(*(self.wakeupArgs))
			#print "COLLECT WAKE UP"
			if self.wakeupArgs[0] == -8:
				nodeID = self.wakeupArgs[1][0]
				stats = self.wakeupArgs[1][1]
				#print "nodeID %d" % nodeID
				#print "total NON Idle %f" % stats[0]
				#print "total Idle %f" % stats[1] 
				totalNonIdle = totalNonIdle + stats[0]
				totalIdle = totalIdle + stats[1]
				done = done - 1
				#print "done val %d" % done
		print "done val %d after" % done
		self.outFd.write("total non-idle node time: %f\n" % totalNonIdle)
		self.outFd.write("total idle node time: %f\n" % totalIdle)
		self.outFd.write("###########\n")
		self.outFd.flush()

	def printJobStats(self):
		print "here"
		totalComputeTime = 0
		totalIOTime = 0
		totalWastedComputeTime = 0
		totalStageInTime = 0
		totalStageOutTime = 0
		for job in self.jobs:
			#print "job ID: %d" % job.jobID
			#print "job total compute time: %f" % job.totalComputeTime
			#print "job total wasted compute time: %f" % job.totalWastedComputeTime
			#print "job total IO time: %f" % job.totalIOTime
			#print "job total stage in Time: %f" % job.stageInTime
			#print "job total stage out time: %f" % job.stageOutTime
			totalComputeTime = totalComputeTime + job.compNodeCnt * job.totalComputeTime
			totalWastedComputeTime = totalWastedComputeTime + job.totalWastedComputeTime * job.compNodeCnt
			totalIOTime = totalIOTime + job.compNodeCnt * job.totalIOTime
			totalStageInTime = totalStageInTime + job.compNodeCnt * job.stageInTime
			totalStageOutTime = totalStageOutTime + job.compNodeCnt * job.stageOutTime
			
		efficiency = totalComputeTime / (totalComputeTime + totalIOTime + totalStageInTime + totalStageOutTime)
		effectiveEfficiency = (totalComputeTime - totalWastedComputeTime) / (totalComputeTime + totalIOTime + totalStageInTime + totalStageOutTime)

		self.outFd.write("totalComputeTime: %f\n" % totalComputeTime)
		self.outFd.write("totalWastedComputeTime: %f\n" % totalWastedComputeTime)
		self.outFd.write("totalIOTime: %f\n" % totalIOTime)
		self.outFd.write("totalStageInTime: %f\n" % totalStageInTime)
		self.outFd.write("totalStageOutTime: %f\n" % totalStageOutTime)
		self.outFd.write("efficiency: %f\n" % efficiency)
		self.outFd.write("effectiveEfficiency: %f\n" % effectiveEfficiency)
		self.outFd.flush()
